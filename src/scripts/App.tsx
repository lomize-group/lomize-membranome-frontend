import * as appendQuery from "append-query";
import { History } from "history";
import * as qs from "query-string";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { match } from "react-router-dom";
import "./App.scss";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import MainContent from "./components/main_content/MainContent";
import Search from "./components/search/Search";
import Sidebar from "./components/sidebar/Sidebar";
import { Classification } from "./config/Classification";
import { RootState } from "./store";
import { SearchBy, updateSearchBy } from "./store/search";
import { updateCurrentUrl } from "./store/url";

function mapStateToProps(state: RootState) {
    return {
        searchBy: state.search.searchBy,
    };
}

const connector = connect(mapStateToProps, { updateCurrentUrl, updateSearchBy });

type AppProps = ConnectedProps<typeof connector> & {
    match: match;
    location: {
        search: string;
    };
    history: History;
};

class App extends React.Component<AppProps> {
    constructor(props: AppProps) {
        super(props);
        props.updateCurrentUrl({
            ...props.match,
            query: qs.parse(props.location.search),
            urlPrefix: "/" + props.match.url.split("/")[1],
            history: this.props.history,
        });
    }

    handleSearch = (value: string) => {
        this.props.history.push(appendQuery("/" + this.props.searchBy, { search: value }));
    };

    changeSearchFor = (e: React.SyntheticEvent<HTMLSelectElement>): void => {
        const searchBy = (e.target as HTMLSelectElement).value as SearchBy;
        this.props.updateSearchBy(searchBy);
    };

    componentWillReceiveProps(nextProps: AppProps): void {
        this.props.updateCurrentUrl({
            ...nextProps.match,
            query: qs.parse(nextProps.location.search),
            urlPrefix: "/" + nextProps.match.url.split("/")[1],
            history: this.props.history,
        });
    }

    render(): JSX.Element {
        return (
            <div className="app">
                <div className="search-header">
                    <div className="header-links">
                        <a target="_blank" href="http://www.umich.edu">
                            UNIVERSITY OF MICHIGAN
                        </a>{" "}
                        |{" "}
                        <a target="_blank" href="http://www.umich.edu/~pharmacy/">
                            COLLEGE OF PHARMACY
                        </a>{" "}
                        |{" "}
                        <a target="_blank" href="https://pharmacy.umich.edu/lomize-group">
                            LOMIZE GROUP
                        </a>
                    </div>
                    <div className="search-membranome">
                        <span className="search-input">
                            <Search
                                placeholder={
                                    "Search " +
                                    Classification.definitions[this.props.searchBy].name +
                                    " by "+(Classification.definitions[this.props.searchBy].name!="pathways" ? "Uniprot ID or ":"")+"name"
                                }
                                onSearch={this.handleSearch}
                            />
                        </span>
                        <span className="search-options">
                            Search
                            <select
                                value={this.props.searchBy}
                                name="searchBy"
                                id="search-by"
                                onChange={this.changeSearchFor}
                            >
                                <option value="proteins">proteins</option>
                                <option value="complex_structures">complexes</option>
                                <option value="mutations">mutations</option>
                                <option value="pathways">pathways</option>
                            </select>
                        </span>
                    </div>
                </div>
                <Header />
                <div className="flex-container">
                    <Sidebar />
                    <MainContent>{this.props.children}</MainContent>
                    <div className="sidebar-right"></div>
                </div>
                <Footer />
            </div>
        );
    }
}

export default connector(App);
