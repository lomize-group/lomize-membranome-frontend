import * as React from "react";

const Error404: React.FC = () => {
    return <div>Page not found</div>;
};

export default Error404;
