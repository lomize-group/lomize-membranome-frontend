import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { RootState } from "../../store";
import "./About.scss";
import Acknowledgements from "./acknowledgements/Acknowledgements";
import Analysis from "./analysis/Analysis";
import Classification from "./classification/Classification";
import Introduction from "./introduction/Introduction";
import MethodsDefinition from "./methods_definition/MethodsDefinition";
import MethodsVerification from "./methods_verification/MethodsVerification";
import ProteinSet from "./protein_set/ProteinSet";
import Sources from "./sources/Sources";
import Verification from "./verification/Verification";

function renderComponent(componentName: string): JSX.Element {
    switch (componentName) {
        case "introduction":
            return <Introduction />;
        case "sources":
            return <Sources />;
        case "methods":
            return <MethodsDefinition />;
        case "mv":
            return <MethodsVerification />;
        case "verification":
            return <Verification />;
        case "classification":
            return <Classification />;
        case "analysis":
            return <Analysis />;
        case "pset":
            return <ProteinSet />;
        case "acknowledgements":
            return <Acknowledgements />;
        default:
            return <Introduction />;
    }
}

function mapStateToProps(state: RootState) {
    return { currentUrl: state.currentUrl };
}

const connector = connect(mapStateToProps);

type AboutProps = ConnectedProps<typeof connector>;

const About: React.FC<AboutProps> = (props: AboutProps) => {
    return (
        <div className="about">
            <div className="nav-panel">
                <Link to="#introduction">Introduction</Link>
                <hr />
                <Link to="#sources">Sources</Link>
                <hr />
                <Link to="#methods">Methods and Definitions</Link>
                <hr />
                <Link to="#mv">Method Verification</Link>
                <hr />
                <Link to="#verification">Verification of TM dimers</Link>
                <hr />
                <Link to="#classification">Classification</Link>
                <hr />
                <Link to="#analysis">Analysis of Bitopic Proteins</Link>
                <hr />
                <Link to="#pset">Protein Set</Link>
                <hr />
                <Link to="#acknowledgements">Acknowledgements</Link>
            </div>
            {renderComponent(props.currentUrl.history.location.hash.replace("#", ""))}
        </div>
    );
};

export default connector(About);
