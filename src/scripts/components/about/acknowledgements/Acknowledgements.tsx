import * as React from "react";
import "./Acknowledgements.scss";

const Acknowledgements: React.FC = () => (
    <div className="acknowledgements normal-info-page">
        <div className="page-header">Acknowledgements</div>
        {/* <a href="https://www.nsf.gov/awardsearch/showAward?AWD_ID=1855425" target="_blank">
            Membranome is funded by the National Science Foundation (NSF)
        </a> */}
        <br />
        <br />
        <p>
            FMAP and TMDOCK have been developed using NACCESS, a program for calculating solvent
            accessible surface areas of proteins, Hubbard S.J., Thornton J.M. (1993), Department of
            Biochemistry and Molecular Biology , University College London, UK.
        </p>
    </div>
);

export default Acknowledgements;
