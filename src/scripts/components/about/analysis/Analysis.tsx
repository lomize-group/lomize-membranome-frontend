import * as React from "react";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./Analysis.scss";

const Analysis: React.FC = () => (
    <div className="analysis normal-info-page">
        <div className="page-header">Analysis of Bitopic Proteins</div>
        <p>
            <b>Table. </b>Bitopic proteins from different biological membranes can be classified
            into three types based on characteristics of their TM domains: length of TM segments,
            hydrophobic thickness (<i>D</i>), transfer energy of an α-helix from water to the lipid
            environment α-transf), and folding energy of an α-helix in membrane relative to coil in
            water (ΔG - fold).
        </p>
        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("analysis-table.png")} className="" />
        </div>
        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("ab1.jpg")} className="" />
        </div>
        <p>
            <b>Figure 9. </b>Distributions of hydrophobic thicknesses of TM α-helices of bitopic
            proteins from different membrane types: prokaryotic plasma membranes (A), eukaryotic
            plasma membrane (B), membranes of endoplasmic reticulum (C), Goldgi apparatus (D),
            mitochondrial inner (E) and outer (F) membranes. Number of proteins assigned to each
            membrane type are indicated in parentheses. Distributions demonstate presence of
            multiple maxima with a major maximum at ~35 Å for eukaryotic plasma membane proteins,
            ~31 Å for proteins from prokaryotic cell membrane, endoplasmic reticulum and Golgi
            apparatus membranes, and ~29 Å for proteins from mitochondrial membranes.
        </p>
        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("All_energy6_confer.jpg")} className="" />
        </div>
        <p>
            <b>Figure 10. </b>Distributions of folding energies (dashed line) and transfer energies
            (solid line) of TM α-helices of bitopic proteins from different membrane types:
            prokaryotic plasma membranes (A), eukaryotic plasma membrane (B), membranes of
            endoplasmic reticulum (C), Goldgi apparatus (D), mitochondrial inner (E) and outer (F)
            membranes. Numbers of proteins assigned to each membrane type are indicated in
            parentheses. Distributions demonstrate that TM helices of plasma proteins are more
            stable and hydrophobic than helices from other membranes, especially from mitochondria.
        </p>
    </div>
);

export default Analysis;
