import * as React from "react";
import "./Classification.scss";

const Classification: React.FC = () => (
    <div className="classification-page normal-info-page">
        <div className="page-header">Classification</div>

        <p>
            {" "}
            The database provides an original classification of whole proteins rather than their
            individual domains. It includes three levels of hierarchy:
        </p>
        <ol>
            <li>
                <p>
                    <b>Functional class:</b> receptors, ligands/regulators, structural/adhesion,
                    proteins involved in biogenesis, membrane remodeling and vesicular trafficking,
                    molecular transport, gene regulation, 6 classes of enzymes, and proteins with
                    undefined function
                </p>
            </li>
            <li>
                <p>
                    <b>Superfamily:</b> related proteins with similar domain architecture
                </p>
            </li>
            <li>
                <p>
                    <b>Family:</b> proteins with significant sequence homology
                </p>
            </li>
        </ol>
        <p>
            {" "}
            Assignment to functional class is based on annotations from{" "}
            <a href="http://www.uniprot.org/">UniProt</a>, <a href="https://www.ebi.ac.uk/interpro/set/pfam/">Pfam</a>,{" "}
            <a href="http://www.ebi.ac.uk/interpro/">InterPro</a> and publications.{" "}
        </p>
        <p>
            Protein superfamilies generally correspond to clans or families in{" "}
            <a href="https://www.ebi.ac.uk/interpro/set/pfam/">Pfam</a>,{" "}
            <a href="https://merops.sanger.ac.uk/">MEROPS</a>, and{" "}
            <a href="http://www.cazy.org/">CAZy</a>.
        </p>
        <p>
            {" "}
            Allocation to protein families follows{" "}
            <a href="http://www.ebi.ac.uk/interpro/">InterPro</a> and{" "}
            <a href="http://www.uniprot.org/">UniProt</a> classifications. Some proteins were
            assigned to families using <a href="http://www.pantherdb.org/">Panther</a>,{" "}
            <a href="http://inparanoid.sbc.su.se/cgi-bin/index.cgi">InParanoid</a>, and{" "}
            <a href="http://www.genome.jp/kegg/">KEGG</a> paralogues and orthologues. Different
            subunits of stable multiprotein complexes were included as families of the same
            superfamily.
        </p>
        <p>
            Membranome also provides complexes formed by TM and/or water-soluble domains of bitopic
            proteins from the PDB. The complexes are classified into functional classes and
            families, similar to that for individual proteins.
        </p>
    </div>
);

export default Classification;
