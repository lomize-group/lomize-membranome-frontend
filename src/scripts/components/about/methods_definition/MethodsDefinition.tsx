import * as React from "react";
import { Link } from "react-router-dom";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import PubMedLink from "../../pubmed_link/PubMedLink";
import "./MethodsDefinition.scss";

const MethodsDefinition: React.FC = () => (
    <div className="methods-definition">
        <div className="page-header">Methods and Definitions</div>
        <p>
            The computational procedure for modeling TM α-helices was based on the following
            thermodynamic cycle which describes folding of peptides and proteins in membranes: (1)
            helix-coil transition in water; (2) transfer of folded peptide to membrane; (3) binding
            of coil to membrane surface; (4) insertion and folding of a peptide in membrane; and (5)
            helix association in membranes (<b>Figure 3</b>).
        </p>

        <div className="method1-div">
            <ImgWrapper src={Assets.images.about("method1.jpg")} className="method1" />
        </div>
        <p>
            <b>Figure 3</b>. Thermodynamic cycle of α-helix folding and association in membrane. The
            two-state model includes: (I) α-helix folding and insertion into membrane; and (II)
            helix-helix association in membrane.
        </p>

        <div className="page-header">FMAP</div>
        <p>
            The FMAP (Folding of Membrane-Associated Peptides) method for prediction of
            membrane-bound α-helices and their positions in membranes (transitions 1-4) was
            developed and implemented into the <Link to="/fmap">FMAP web server</Link>. FMAP was
            used to predict TM domains of bitopic proteins from Membranome and to generate their 3D
            models in the lipid bilayer.
        </p>
        <p>
            The coordinate files of 3D models of TM α-helical domains of bitopic proteins are
            available for downloading individually and for the entire protein set.
        </p>
        <p>
            <Link to="/fmap">
                <b>FMAP server</b>
            </Link>{" "}
            predicts formation of individually stable α-helices by peptides and proteins depending
            on the experimental conditions
            <i>in vitro</i>. It utilizes different physical models for the following cases:
        </p>
        <ol type="a">
            <li> peptides in aqueous solution </li>
            <li> water-soluble protein in the molten globule state </li>
            <li> peptides in micelles </li>
            <li> peptides in the lipid bilayers </li>
            <li> TM proteins </li>
        </ol>
        <p>
            <b>The computational procedure includes the following steps:</b>
        </p>
        <ol>
            <li>
                <p>
                    Calculation of free energy differences for membrane-bound α-helical and coil
                    segments relative to coil in water for every segment of the polypeptide chain,
                    similar to that in FRAMEWORK program (
                    <PubMedLink id="9235002" />)
                </p>
            </li>
            <li>
                <p>
                    Calculation of the lowest energy partition of polypeptide chain into α-helical
                    and coil segments by the dynamic programming algorithm or averaging of different
                    helix-coil partitions
                </p>
            </li>
            <li>
                <p>Generation of all-atom 3D models for predicted α-helical segments</p>
            </li>

            <li>
                <p>
                    Positioning of 3D model of a predicted TM α-helices in membranes by optimizing
                    their transfer free energy by the PPM2.0 program using an anisotropic solvent
                    model of the lipid bilayer (
                    <PubMedLink id="21438606" />)
                </p>
            </li>
        </ol>

        <p>
            <b>STEP 1</b>
        </p>
        <p>
            <b>Folding energy of every α-helical segment</b> of <i>m</i> residues, starting from
            residue <i>k</i> is calculated as the sum of intrinsic helix stability in water and its
            transfer energy from water to the lipid bilayer:
        </p>

        <div className="formula-div">
            <ImgWrapper src={Assets.images.about("Formula_DGfold.jpg")} className="formula" />
        </div>

        <p>
            <b>Energy of helix folding in water, </b>&Delta;<i>G</i>
            <sub>
                <i>int,wat</i>
            </sub>
            <sup>
                <i>α</i>
            </sup>
            <i>(k,m)</i> is calculated as sum of main chain enthalpic (H-bonding) and conformational
            entropy contributions for the “host” poly-Ala chain during coil-to-helix transition,
            free energy differences associated with replacement of Ala by other side chains
            (experimental α-helix “propensities”), H-bonding, electrostatic, and hydrophobic
            interactions between side-chains. Empirical energies for N-cap, C-cap and the
            hydrophobic staple motifs are tabulated based on experimental studies of peptide
            stabilities in the aqueous solution.
        </p>
        <p>
            <b>Transfer energy of an α-helix</b> from water to a nonpolar environment &Delta;
            <i>G</i>
            <sub>
                <i>int,wat</i>
            </sub>
            <sup>
                <i>α</i>
            </sup>
            <i>(k,m)</i> depends on the chosen physical model. The approach for micelles and protein
            molten globule state is essentially the same as in our previous work (
            <PubMedLink id="9235002" />) although we have changed transfer energies to micelles to
            reflect recent studies.
        </p>

        <p>
            <b>Transfer energies for TM α-helices</b> can be estimated using an all-atom or
            whole-residue approximations. For bitopic proteins we used the whole-residue transfer
            energy profiles along the bilayer normal for different types of residues, &Delta;
            <i>G</i>
            <sub>
                <i>i</i>
            </sub>
            <sup>
                <i>α</i>
            </sup>
            <i>
                (z <sub>i</sub> )
            </i>
            w which were pre-calculated ausing our implicit solvation model (
            <PubMedLink id="21438606" />
            ):
        </p>

        <div className="method3-div">
            <ImgWrapper src={Assets.images.about("method3.jpg")} className="method3" />
        </div>

        <p>
            Two additional terms describe the energetic penalty for the hydrophobic mismatch and the
            helix tilting in membrane, where <i>D</i>
            <sub>
                <i>0</i>
            </sub>{" "}
            and <i>D</i>
            are the hydrophobic thicknesses of the lipid bilayer and helix, respectively,
            <i>L</i> is length of TM segment of the helix, <i>ϕ</i> is tilt angle of the helix with
            respect to bilayer normal, and <i>f</i>
            <sub>
                <i>mism</i>
            </sub>{" "}
            and <i>f</i>
            <sub>
                <i>cross</i>
            </sub>{" "}
            are adjustable parameters which define the corresponding contributions. Transfer energy
            &Delta;<i>G</i>
            <sub>
                <i>transfer</i>
            </sub>
            <sup>
                <i>α</i>
            </sup>
            <i>(k,m)</i> was optimized by grid scan with respect to three variables:
            <i>z</i>
            <sub>
                <i>o</i>
            </sub>{" "}
            (shift of 1<sup>st</sup> residue in the helix along membrane normal), <i>D</i>{" "}
            (hydrophobic thickness) and <i>ϕ</i> (tilt angle of the helix with respect to membrane.
        </p>

        <p>
            <b>Membrane binding energy of unfolded polypeptide chain </b>&Delta;<i>G</i>
            <sup>
                <i>bind</i>
            </sup>
            <i>(k,m)</i> is calculated based on Wimley-White whole-residue interfacial scale.
        </p>

        <p>
            <b>STEP 2.</b> was implemented as described in our previous work (
            <PubMedLink id="9235002" />
            ).
        </p>

        <p>
            <b>STEP 3.</b>
        </p>
        <p>
            <b>To generate all-atom 3D models</b> of predicted α-helical segments, FMAP uses
            structural templates that represent a straight or a Pro-kinked helix. The allowed
            side-chain rotamers are automatically selected from a backbone-dependent library to
            minimize transfer energy in the lipid bilayer.
        </p>

        <p>
            <b>STEP 4.</b>
        </p>
        <p>
            <b>The spatial positions of 3D models</b> of predicted TM α-helices are defined by
            optimizing transfer energy from water to the lipid bilayer using an updated version of
            PPM program incorporated into FMAP. In addition to solvation energy, it includes a few
            mechanical energy terms to account for membrane deformations during penetration of
            α-helices into the lipid bilayer. These contributions are related to the hydrophobic
            mismatch and tilting of TM helices.
        </p>
        <div className="page-header">TMDOCK</div>
        <p>
            TMDOCK web server predicts formation of parallel homodimers by transmembrane (TM)
            alpha-helices. The computational procedure includes the following steps:
        </p>
        <div className="indented">
            <p>Step 1: A single TM helix is predicted and generated by FMAP</p>
            <p>
                Step 2: A set of different structures of the dimer is generated by “threading”
                target amino acid sequence through four dimeric templates (5eh4, 2zta, 2k1k and 2hac
                PDB entries). The target and template helices are superimosed to produce all
                possible helix packing arrangements. Each structure undergoes local energy
                minimization in space of torsion angles and rigid body variables of the helices
                using a modified version of ConforNMR (
                <PubMedLink id="1376600">Lomize et al. 1992</PubMedLink>) with new force field.
            </p>
            <p>
                Step 3: Selecting set of possible structures of the homodimer based on the
                calculated free energy of helix association and a few supplementary parameters. The
                methodology and parameters for calculating this energy were previously developed and
                tested (<PubMedLink id="12142453">Lomize et al. 2002</PubMedLink>,
                <PubMedLink id="15340167">Lomize et al. 2004</PubMedLink>
                ). Main components of this energy are: (a) stabilizing van der Waals interactions
                and H-bonds between TM helices; (b) solvation energy that describes transfer of
                protein atoms from lipid bilayer to the protein interior; (c) changes in
                conformational entropy of side chains during helix association; and (d)
                electrostatic interactions between helix dipoles in membrane. The contributions of
                interactions are averaged over different side chain conformers and therefore
                effectively reduced for flexible side chains.
            </p>
        </div>
    </div>
);

export default MethodsDefinition;
