import * as React from "react";
import { Link } from "react-router-dom";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./MethodsVerification.scss";

const MethodsVerification: React.FC = () => (
    <div className="methods-verification normal-info-page">
        <div className="page-header">Verification of FMAP for bitopic proteins</div>
        <p>
            The FMAP method was developed as a physics-based approach to predict stability and
            lengths of membrane-bound alpha-helices in peptides and single-spanning TM proteins.
            FMAP was tested for a large set of peptides studied by solution NMR and other methods in
            micelles, bicelles and lipid bilayers. It was also tested for a set of 141 different
            bitopic proteins with known structures (83 PDB entries), as described below.
        </p>
        <p>
            All TM single-helical segments were identified with average errors in prediction of N-
            and C-termini of 2.2 and 3.0 residue/per helix end, respectively (<b>Figure 4 A, B</b>).
            The average accuracy in prediction of hydrophobic thicknesses for single-pass TM-helices
            was 2.4±2.0 Å (<b>Figure 4 C</b>). For ~80% of bitopic proteins with known 3D structure
            the difference between experimental and predicted lengths of TM helices did not exceed 7
            residues per helix (<b>Figures 5 A-C</b>). The largest differences were observed for
            bitopic proteins from multiprotein complexes, likely due to the contributions of
            specific tertiary inter-helical interactions not included in FMAP.
        </p>

        <div className="reg-img accuracy_color_img">
            <ImgWrapper src={Assets.images.about("Accuracy_color_copy.jpg")} className="" />
        </div>
        <p>
            <b>Figure 4. </b>Accuracy of FMAP in predicting ends and hydrophobic thicknesses of TM
            helices. Distributions of differences in lengths (<b>A:</b> N-termini; <b>B:</b>
            C-termini) and hydrophobic thickness (<b>C</b>) between FMAP-predicted TM helices and
            experimental 3D structures of 139 bitopic proteins from 83 PDB entries. Hydrophobic
            thicknesses of experimental structures were taken from the{" "}
            <a href="http://opm.phar.umich.edu/">OPM</a>
            database.
        </p>

        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("Fig5A_monomer.png")} className="" />
        </div>
        <p>
            <b>Figure 5A</b>. Comparison of FMAP-predicted TM helices with experimental structures
            of bitopic proteins in monomeric state. Experimental structures of bitopic proteins are
            shown as cartoons colored gray. FMAP-predicted intra-helical residues are highlighted by
            purple color. PDB IDs are indicated for each structure. Horizontal lines specify
            hydrophobic membrane boundaries calculated by PPM2.0, which are colored blue for the
            inner leaflet and colored red for the outer leaflet.
        </p>
        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("Picture_oligomer111.jpg")} className="" />
        </div>
        <p>
            <b>Figure 5B</b>. Comparison of FMAP-predicted TM helices with experimental structures
            of bitopic proteins in in homo- and hetero-oligomeric states. Experimental structures of
            bitopic proteins are shown as cartoons colored gray. FMAP-predicted intra-helical
            residues e highlighted by purple color. PDB IDs are indicated for each structure. Most
            structures represent homodimers, except two structures for heterodimers (2k9j, 2ks1) and
            one structure for heterotetramer (2rlf). Horizontal lines specify hydrophobic membrane
            boundaries calculated by PPM2.0, which are colored blue for the inner leaflet and
            colored red for a outer leaflet.
        </p>

        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("Fig5C_complex11.png")} className="" />
        </div>
        <p>
            <b>Figure 5C</b>. Comparison of FMAP-predicted TM helices with experimental structures
            of bitopic proteins in multiprotein complexes. Experimental structures of bitopic
            proteins are shown as cartoons colored gray. FMAP-predicted intra-helical residues are
            highlighted y purple color. PDB IDs and subunit names are indicated for each structure.
            Horizontal lines specify hydrophobic membrane boundaries calculated by PPM2.0, which are
            colored blue for the inner leaflet and colored red for the outer leaflet.
        </p>

        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("Fig5C_complex2.png")} className="" />
        </div>

        <p>
            <b>Figure 5C (continued).</b> Comparison of FMAP-predicted TM helices with experimental
            structures of bitopic proteins in multiprotein complexes. Experimental structures of
            bitopic proteins are shown as cartoons colored gray. FMAP-predicted intra-helical
            residues are highlighted by purple color. PDB IDs and subunit names are indicated for
            each structure. Horizontal lines specify hydrophobic membrane boundaries calculated by
            PPM2.0, which are colored blue for the inner leaflet and colored red for the outer
            leaflet.
        </p>
        <p>
            Comparison of TMH included in Membranome database (FMAP prediction, with subsequent
            removal of false-positives, see <Link to="#pset">Protein set</Link> with prediction of
            TMH by top servers. For a set of 4831 bitopic proteins in Membranome, almost 97%
            proteins were also identified as bitopic by Phobius. Importantly, &gt;86% of the helices
            showed a nearly perfect sequence overlap: &gt;80% of residues in the overlapped helices
            coincide. The corresponding fractions for TMHMM, and TopPred were 75% and 70%,
            respectively (<b>Figure 6, Table 1</b>).
        </p>

        <div className="reg-img mv7">
            <ImgWrapper src={Assets.images.about("mv7.jpg")} className="" />
        </div>
        <p>
            <b>Figure 6. </b>Performance of FMAP <i>vs</i>
            top prediction servers in identification of TM α-helices.
        </p>
        <p>
            <b>Table 1</b>.
        </p>
        <p>
            Comparison of TM α-helices included in Membranome database with predictions of TMH by
            top bioinformatics tools. Numbers designate protein fractions (from 4831 bitopic
            proteins) that demonstrate 80-100%, 1-79% and 0% sequence overlap in TM α-helices
            predicted by FMAP and other servers.
        </p>

        <div className="reg-img mv7">
            <ImgWrapper src={Assets.images.about("helix-overlap-table.png")} className="" />
        </div>

        <div className="page-header">Verification of TMDOCK</div>
        <p>
            {" "}
            TMDOCK web server predicts formation of parallel homodimers by transmembrane (TM)
            alpha-helices. Antiparallel dimers and heterodimers are not included in this version.
        </p>
        <p>
            {" "}
            The underlying theoretical method was able to reproduce 26 experimental dimeric
            structures formed by TM α-helices of 21 single-pass membrane proteins (including 4
            mutants) with Cα-atom r.m.s.d. 1.0 to 3.3 Å and native-like models ranked #1 in 70% of
            cases (Figure below).
        </p>
        <div className="reg-img mv7">
            <ImgWrapper src={Assets.images.about("TMDOCK_rmsdnew_JMB.png")} className="" />
        </div>
        <p>
            <b>Figure 7. </b>Superposition of experimental structures (colored blue) and
            TMDOCK-generated models (colored yellow). Theoretical model was ranked #1 in all cases,
            except NMR structures stabilized by surface helices in APP (2loh), ErbB2 (2n2a) and few
            other proteins: Toll-like receptor, PGFRB, EphA2, FGFR3, and ErbB3. Helix crossing
            angles and r.m.s.d. between experimental and modeled structures are indicated. Positions
            of calculated hydrocarbon membrane boundaries (dotted lines) are indicated by red for
            extracellular membrane side and by blue for cytoplasmic side.
        </p>
        <p>
            <b>Note:</b> This web server implements a faster version of the method with threading
            through only four structural templates. Therefore, it misses several low-stability NMR
            models that could only be detected using a larger set of templates (2k9y, 2l6w, 2l9u and
            2mjo).
        </p>
        <div className="reg-img mv7">
            <ImgWrapper src={Assets.images.about("TMDOCK_energy_Correlation.png")} className="" />
        </div>
        <p>
            <b>Figure 8. </b> Correlation of experimental and calculated helix association energies
        </p>
    </div>
);

export default MethodsVerification;
