import * as React from "react";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./ProteinSet.scss";

const ProteinSet: React.FC = () => (
    <div className="protein-set normal-info-page">
        <div className="page-header">Protein Set</div>
        <p>
            A set of bitopic proteins from six species was prepared using protein entries (main
            isoforms) from
            <a href="http://www.uniprot.org/">UniProt</a> Swiss-Prot and TrEMBL databases (release
            2015-07) (<b>Figure 1</b>).
        </p>
        <p>
            {" "}
            FMAP method was used to identify TM α-helices in amino acid sequences and generate their
            3D models in membranes (see METHODS). Predicted bitopic proteins were filtered to remove
            polytopic proteins and proteins with signal sequences or cleavable C-terminal helices
            based on <a href="http://www.uniprot.org/">UniProt</a> and{" "}
            <a href="https://www.ebi.ac.uk/interpro/set/pfam/">Pfam</a> annotations. We also excluded proteins with
            hydrophobic helices that overlaped with globular domains indicated in Pfam,{" "}
            <a href="http://www.ebi.ac.uk/interpro/">InterPro</a> or{" "}
            <a href="http://www.pdb.org/pdb/home/home.do">PDB</a>. Human intervention and analysis
            of related publications was required to resolve some ambiguous cases.
        </p>
        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("pset1.jpg")} className="" />
        </div>
        <p>
            <b>Figure 1</b>. Numbers of bitopic proteins from different organisms in the Membranome
            database (2016-02).
        </p>
        <p>
            The sets of bitopic proteins included in the Membranome database (Figure 2) are similar
            to those in <a href="http://www.uniprot.org/">UniProt</a> for the fully annotated
            proteomes (human, yeast and prokaryotic), however they are almost 2 fold expanded
            relative to <a href="http://www.uniprot.org/">UniProt</a> for <i> A. thaliana</i> and{" "}
            <i>D. discoideum</i>
            proteomes due to inclusion of numerous TrEMBL entries (<b>Figure 2</b>).
        </p>
        <div className="reg-img">
            <ImgWrapper src={Assets.images.about("UniProt_vs_Membranome2.png")} className="" />
        </div>
        <p>
            <b>Figure 2. </b>Comparison of bitopic protein sets in Membranome and UniProt databases
        </p>
    </div>
);

export default ProteinSet;
