import * as React from "react";
import ExternalLink from "../../external_link/ExternalLink";
import PubMedLink from "../../pubmed_link/PubMedLink";
import "./Sources.scss";

const Sources: React.FC = () => (
    <div className="sources">
        <div className="page-header">Sources</div>
        <div className="main-text">
            <p>
                Nomenclature, general annotation and sequence features of proteins are taken from{" "}
                <a href="http://www.uniprot.org/" target="_blank" className="external">
                    Uniprot
                </a>
                .
            </p>

            <p>
                {" "}
                The classification was built using{" "}
                <a href="http://www.uniprot.org/" target="_blank" className="external">
                    Uniprot
                </a>{" "}
                families,{" "}
                <a href="https://www.ebi.ac.uk/interpro/set/pfam/" target="_blank" className="external">
                    Pfam
                </a>
                ,{" "}
                <a href="http://www.ebi.ac.uk/interpro/" target="_blank" className="external">
                    Interpro
                </a>
                ,{" "}
                <a href="http://www.pantherdb.org/" target="_blank" className="external">
                    Panther
                </a>
                ,{" "}
                <a href="https://merops.sanger.ac.uk/" target="_blank" className="external">
                    MEROPS
                </a>
                ,{" "}
                <a href="http://www.cazy.org/" target="_blank" className="external">
                    CAZy
                </a>
                ,{" "}
                <a
                    href="http://inparanoid.sbc.su.se/cgi-bin/index.cgi"
                    target="_blank"
                    className="external"
                >
                    InParanoid
                </a>
                , and{" "}
                <a href="http://www.genome.jp/kegg/" target="_blank" className="external">
                    KEGG
                </a>
                .
            </p>

            <p>
                Intracellular localization of eukaryotic proteins in cellular and organelle
                membranes is based on information from{" "}
                <a href="http://www.uniprot.org/" target="_blank" className="external">
                    Uniprot
                </a>{" "}
                and{" "}
                <a
                    href="http://compartments.jensenlab.org/Search"
                    target="_blank"
                    className="external"
                >
                    COMPARTMENTS
                </a>{" "}
                databases. The current version provides only the most biologically relevant of
                several possible localizations.
            </p>

            <p>
                Protein topology (association of protein N-terminus with a specific membrane
                leaflet: “In” or “Out”) was based on information from{" "}
                <a href="http://www.uniprot.org/" target="_blank" className="external">
                    Uniprot
                </a>
                ,{" "}
                <a href="http://topdb.enzim.hu/" target="_blank" className="external">
                    TOPDB
                </a>
                ,{" "}
                <a href="http://www.rcsb.org/pdb/home/home.do" target="_blank" className="external">
                    PDB
                </a>{" "}
                and publicationsi, including direct experimental studies of TM protein topology,
                locations of water-soluble domains in specific intracellular compartments and
                post-translational modifications.
            </p>

            <p>
                Information on complexes of bitopoc proteins was compiled using{" "}
                <ExternalLink href="https://reactome.org/">Reactome</ExternalLink> (
                <PubMedLink id="31691815" />
                ),{" "}
                <ExternalLink href="https://www.ebi.ac.uk/complexportal/home">
                    Complex Portal
                </ExternalLink>{" "}
                (<PubMedLink id="30357405" />
                ),{" "}
                <ExternalLink href="https://mips.helmholtz-muenchen.de/corum/">
                    CORUM
                </ExternalLink>{" "}
                (<PubMedLink id="30357367" />
                ), and <ExternalLink href="https://www.rcsb.org/">PDB</ExternalLink>, based on the
                following criteria: (a) at least one bitopic protein from Membranome participated in
                the complex, (b) the existence of the complex was experimentally proven and
                supported by Pubmed references, (c) only complexes with 8 or less uniquely defined
                proteins were taken from Reactome.
            </p>

            <p>
                Direct protein-protein interactions between bitopic proteins were taken from{" "}
                <ExternalLink href="http://hint.yulab.org/">HINT</ExternalLink> (
                <PubMedLink id="22846459" />) and{" "}
                <ExternalLink href="http://cicblade.dep.usal.es:8080/APID/init.action">
                    APID
                </ExternalLink>{" "}
                (<PubMedLink id="30715274" />) databases. Only “high-quality” and “level 2”
                interactions were taken from HINT and APID, respectively. Additional interactions
                can be found by following links to{" "}
                <ExternalLink href="https://www.ebi.ac.uk/intact/">IntAct</ExternalLink>,{" "}
                <ExternalLink href="https://thebiogrid.org/">BioGrid</ExternalLink>, and{" "}
                <ExternalLink href="https://string-db.org/cgi/input.pl">STRING</ExternalLink> on
                protein pages.
            </p>

            <p>
                Database provides links to{" "}
                <ExternalLink href="http://www.uniprot.org/">Uniprot</ExternalLink>,{" "}
                <ExternalLink href="https://www.ebi.ac.uk/interpro/set/pfam/">Pfam</ExternalLink>,{" "}
                <ExternalLink href="http://www.ebi.ac.uk/interpro/">Interpro</ExternalLink>,{" "}
                <ExternalLink href="http://www.pdb.org/pdb/home/home.do">PDB</ExternalLink>,{" "}
                <ExternalLink href="http://opm.phar.umich.edu/">OPM</ExternalLink>,{" "}
                <ExternalLink href="http://www.hmdb.ca/">HMDB</ExternalLink>,{" "}
                <ExternalLink href="http://www.genenames.org/">HGNC</ExternalLink>,{" "}
                <ExternalLink href="http://www.reactome.org/">Reactome</ExternalLink>,{" "}
                <ExternalLink href="https://www.ebi.ac.uk/intact/">IntAct</ExternalLink>,{" "}
                <ExternalLink href="https://thebiogrid.org/">BioGrid</ExternalLink>,{" "}
                <ExternalLink href="http://www.hmdb.ca/">HMDB</ExternalLink>,{" "}
                <ExternalLink href="https://www.wikipedia.org/">Wikipedia</ExternalLink>,{" "}
                <ExternalLink href="https://www.wikipathways.org/index.php/WikiPathways">
                    WikiPathways
                </ExternalLink>
                ,{" "}
                <ExternalLink href="https://www.ebi.ac.uk/complexportal/home">
                    Complex Portal
                </ExternalLink>
                , and PubMed citations provided by related databases.
            </p>

            <p>
                Mutations in transmembrane helices are taken from{" "}
                <ExternalLink href="https://www.iitm.ac.in/bioinfo/MutHTP/">MutHTP</ExternalLink>{" "}
                database.
            </p>

            <p>
                Biological pathways were compiled from{" "}
                <ExternalLink href="https://www.genome.jp/kegg/pathway.html">KEGG</ExternalLink>,{" "}
                <ExternalLink href="https://reactome.org/">Reactome</ExternalLink>,{" "}
                <ExternalLink href="https://biocyc.org/">BioCyc</ExternalLink>,{" "}
                <ExternalLink href="https://www.wikipathways.org/index.php/WikiPathways">
                    WikiPathways
                </ExternalLink>
                , and <ExternalLink href="http://www.hmdb.ca/">HMDB</ExternalLink> databases.
            </p>

            <p>
                Membranome allows searching based on UniProt and PDB IDs and text fields. Browsing
                and sorting of proteins can be done by species, individual proteins or complexes,
                classes, superfamilies, families, localization, hydrophobic thicknesses and tilt
                angles.
            </p>
        </div>
    </div>
);

export default Sources;
