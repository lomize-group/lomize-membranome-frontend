import * as React from "react";
import VerificationLong from "./Verification-long";
import "./Verification.scss";

const Verification: React.FC = () => {
    return <VerificationLong />;
    // return(
    // 	<div className="verification normal-info-page">
    // 		<div className="page-header">
    // 			Verification of TMDOCK generated dimers for agreement with experimental data
    // 		</div>
    // 		<p>This is a placeholder page</p>
    // 		<span> the actual verification is ~6000 lines long and takes a while to build, thus slowing development.
    // 		 React insists on rebuilding it every time. You can find it in Verification-long and enable it that way before prod deploy.</span>
    // 	</div>
    // )
};

export default Verification;
