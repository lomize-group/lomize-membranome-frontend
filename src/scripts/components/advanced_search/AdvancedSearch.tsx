import * as React from "react";
import {Urls} from "../../config/Urls";
import {Classification, CType} from "../../config/Classification";
import {Columns} from "../../config/Columns";
import {capitalize} from "../../config/Util";
import Table from "../table/Table";
import "./AdvancedSearch.scss"

//The structure of this Component is derived from AdvancedSearch.js located inside opm-frontend. Changes were made where necessary to comply with TypeScript's additional type rules.

//props and state parameters stated here to comply with TypeScript's type static rules
export type AdvancedProps={};
export type AdvancedState={
    search_for:string,
    submitted:boolean,
    submit_data:Record<string,string>
}; //for some reason, if results is added everything breaks
//   as a result, results will stay removed until it's needed


class AdvancedSearch extends React.Component<AdvancedProps,AdvancedState>{

    //state of form when first loaded
    get_initial_state = () =>({
            search_for:"proteins",
            submitted:false,
            submit_data:{},
            results:[]
    })

    reset = () => {
        this.setState(this.get_initial_state());
    }

    search = (event:any) =>{
        event.preventDefault();

        //catches all the possible ways an entry can be not filled
        const not_blank = (val:any) => val !== "" && val !== null && val !== undefined;

        type FormPair={
            name:string,
            value:string
        };

        //extract all the fields which have filled values
        const form_vals = Object.values(event.target.elements).filter((elem:any) => not_blank(elem.name)) as FormPair[];

        
        //construct JS object to be used in table construction
        var submit_data: Record<string,string>={};
			for (let i = 0; i < form_vals.length; ++i) {
                var name = form_vals[i].name;
                var value = form_vals[i].value;
				submit_data[name] = value;
            }

        //trigger table load
        this.setState(
            {submitted:false},
            () => this.setState({
                submitted:true,
                submit_data
            })
        );
    }

    renderResults(){
        //on initial load of search form
        if(!this.state.submitted){
            return null;
        }

        //on form submit
        //as CType is included to comply with typescript
        const ctype:string = this.state.search_for;

        //NOTE: this should be the full extension
        //Classification.definitions[ctype as CType].api.route 
        return(
            <Table
                title={"Search results for " + capitalize(ctype)}
                url={Urls.api_url("advanced_search",this.state.submit_data)}
                columns={Columns[ctype].columns}
                rowHeight={Columns[ctype].rowHeight}
                redirect={false}
                selectedSpecies={[this.state.submit_data.species]}
            />
        );

    }

    constructor(props:any){
        super(props);

        this.state = this.get_initial_state();


		
        //NOTE: handle_change not needed unless professor wishes to generalize to beyond proteins

    }

    render_search_entry(screen_name:string, hidden_name: string, placeholder:string){
        //optional: form-inline classname
        return (
        <div className="form-group row">
            <div className="col-xs-3">
                <label htmlFor={hidden_name}>{screen_name}</label>{" "}
            </div>
            <div className = "col-xs-9">
                <input
                    type="text"
                    className="form-control input-sm"
                    name={hidden_name}
                    id={hidden_name}
                    placeholder={placeholder}
                />
            </div>
        </div>);
    }

    render(){
		const render_comparisons = (id_name:string) => {
            {/** NOTE: fit this parameter into select*/}
            {/*value={this.state[id_name as keyof AdvancedState]}*/}
			return(
				<select
					className="form-control input-sm"
					name={id_name}
					id={id_name}
				>
					<option value=">=">&ge;</option>
					<option value="<=">&le;</option>
				</select>
			);
		}

        return(
            <div className = "advanced-search">
				<div className="page-header">Search</div>
                {/* TODO: add onSubmit={} parameter to form*/}
                <form className="inputs-area" onSubmit={this.search}>
                    {/* OPTIONAL: add search for optiom -- see OPM for example*/}
                    <div className="search-options">
                        <h2>Search For Proteins</h2>
						<p>
							Search results will satisfy the conditions of all inputs. Leaving an input
							empty will exclude it from the search.
						</p>

                    </div>
                    {/* OPTIONAL: move to separate function */}
                    <div className="row">
                        <div className="col-xs-6">
                            {this.render_search_entry("Protein Name", "name","")}
                            {this.render_search_entry("Uniprot ID","uniprot_id","for example, MGAT2_ARATH  or Q9FT88")}
                            {this.render_search_entry("PDB ID","pdbid","for example, 6ntp")} 
                            {this.render_search_entry("Gene Name","genename","")}
                            {this.render_search_entry("Uniprot Features","uniprot_features","")}
                            {this.render_search_entry("Uniprot Annotations","uniprot_annotations","")}
                            {this.render_search_entry("Species","species","")}
                            {this.render_search_entry("PFAM Domains","pfam_search_domains","")}
                        </div>
                        <div className="col-xs-6">
                            {this.render_search_entry("Superfamily","superfamily","")}
                            {this.render_search_entry("Family","family","")}
                            {this.render_search_entry("Localization","localization","")}

                            <div className="form-group form-inline row">
                                <div className="col-xs-3">
                                    <label htmlFor="thickness">Hydrophobic Thickness</label>
                                </div>
                                <div className="col-xs-9">
                                    {render_comparisons("thickness_compare")}{" "}
                                    <input
                                        type="text"
                                        className="form-control input-sm"
                                        id="thickness"
                                        name="thickness"
                                        placeholder="Example: 30.0"
                                    />
                                </div>
                            </div>
                            <div className="form-group form-inline row">
                                <div className="col-xs-3">
                                    <label htmlFor="tilt_angle">Tilt Angle</label>
                                </div>
                                <div className="col-xs-9">
                                    {render_comparisons("tilt_angle_compare")}{" "}
                                    <input
                                        type="text"
                                        className="form-control input-sm"
                                        id="tilt_angle"
                                        name="tilt_angle"
                                        placeholder="Example: 10"
                                    />
                                </div>
                            </div>
                            <div className="form-group form-inline row">
                                <div className="col-xs-3">
                                    <label htmlFor="delta_transfer">&Delta;G<sub>transfer</sub></label>
                                </div>
                                <div className="col-xs-9">
                                    {render_comparisons("delta_transfer_compare")}{" "}
                                    <input
                                        type="text"
                                        className="form-control input-sm"
                                        id="delta_transfer"
                                        name="delta_transfer"
                                        placeholder="Example: -10.0"
                                    />
                                </div>
                            </div>
                            <div className="form-group form-inline row">
                                <div className="col-xs-3">
                                    <label htmlFor="delta_fold">&Delta;G<sub>fold</sub></label>
                                </div>
                                <div className="col-xs-9">
                                    {render_comparisons("delta_fold_compare")}{" "}
                                    <input
                                        type="text"
                                        className="form-control input-sm"
                                        id="delta_fold"
                                        name="delta_fold"
                                        placeholder="Example: -10.0"
                                    />
                                </div>
                            </div>

                        </div>
                    </div>
                    <br/>
					<div className="input-group search-buttons">
						<input
							type="submit"
							value="Search"
							className="submit-button"
						/>
						{" "}
                        {/* TODO: add onClick option here*/}
						<input
							type="reset"
							value="Reset"
                            className="submit-button"
                            onClick={this.reset}
						/>
					</div>

                </form>
                <br/>
                {this.renderResults()}
            </div>
        );
    }
}

export default AdvancedSearch;