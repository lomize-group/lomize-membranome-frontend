import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Assets } from "../../config/Assets";
import {
    Classification as ClassificationDef,
    CType,
    CTypeCategory,
} from "../../config/Classification";
import { Columns } from "../../config/Columns";
import { Urls } from "../../config/Urls";
import { capitalize, createInfo } from "../../config/Util";
import { RootState } from "../../store";
import Info, { InfoProps } from "../info/Info";
import List, { ListProps } from "../list/List";
import Table, { TableProps } from "../table/Table";
import "./Classification.scss";

function mapStateToProps(state: RootState) {
    return {
        currentUrl: state.currentUrl,
    };
}

const connector = connect(mapStateToProps);

type ClassificationProps = ConnectedProps<typeof connector> & {
    ctype: CType;
    category: CTypeCategory;
    hasTable: boolean;
    hasList: boolean;
};

type ClassificationState = {
    resize: boolean;
    isSingular: boolean;
    loading: boolean;
    header?: string;
    info?: InfoProps;
    tableData?: TableProps;
    listData?: ListProps;
    listHeader?: string;
};

function fetchForTable(curProps: ClassificationProps, isSingular: boolean) {
    let returnState: Partial<ClassificationState> = {};
    const { ctype, currentUrl, category } = curProps;
    const ctype_def = ClassificationDef.definitions[ctype];

    const pageSize = category === "complex_structures" ? 200 : 500;

    if (isSingular) {
        const category_def = ClassificationDef.definitions[category];
        const cols = ctype_def.hasOwnProperty("parents")
            ? Columns[category].columnsWithout(ctype_def.column!)
            : Columns[category].columns;

        returnState.tableData = {
            title: capitalize(category_def.name),
            url:
                Urls.baseUrl +
                ClassificationDef.definitions[ctype].api.route +
                "/" +
                currentUrl.params.id +
                category_def.api.route,
            columns: cols,
            rowHeight: Columns[category].rowHeight,
            ctype: category,
            showSpeciesFilter: category === "proteins",
            redirect: false,
            pageSize,
        };
    } else {
        returnState.tableData = {
            title: "All " + ctype_def.name + " in Membranome",
            url: Urls.baseUrl + ClassificationDef.definitions[ctype].api.route,
            columns: Columns[ctype].columns,
            rowHeight: Columns[ctype].rowHeight,
            search: currentUrl.query.search,
            ctype: ClassificationDef.category_of(ctype),
            showSpeciesFilter: ctype === "proteins",
            pageSize,
            redirect: !(curProps.category === "mutations") //ensures redirections do not occur for mutations
        };
    }

    return returnState;
}

function fetchForList(curProps: ClassificationProps, isSingular: boolean) {
    let returnState: Partial<ClassificationState> = {};
    const { ctype, currentUrl, category } = curProps;
    const ctype_def = ClassificationDef.definitions[ctype];
    const child_ctype_def = ClassificationDef.definitions[ctype_def.child!];

    if (isSingular) {
        returnState.listHeader = capitalize(child_ctype_def.name);
        returnState.listData = {
            url:
                Urls.baseUrl +
                ctype_def.api.route +
                "/" +
                currentUrl.params.id +
                child_ctype_def.api.route,
            ctype: ctype_def.child!,
            expandableItems: category !== "pathways",
        };
    } else {
        returnState.listHeader = capitalize(ctype_def.name);
        returnState.listData = {
            url: Urls.baseUrl + ctype_def.api.route,
            ctype: ctype,
        };
    }

    return returnState;
}

class Classification extends React.Component<ClassificationProps, ClassificationState> {
    static defaultProps: Partial<ClassificationProps> = {
        hasTable: false,
        hasList: false,
    };

    private table: any;

    constructor(props: ClassificationProps) {
        super(props);

        this.state = {
            resize: false,
            isSingular: props.currentUrl.params.hasOwnProperty("id"),
            loading: false,
            header: "",
        };
        this.state = {
            ...this.state,
            ...this.fetch(props),
        };
    }

    private createParentInfo = (curProps: ClassificationProps) => {
        const { ctype, currentUrl } = curProps;
        const ctype_def = ClassificationDef.definitions[ctype];

        axios.get(Urls.baseUrl + ctype_def.api.route + "/" + currentUrl.params.id).then((res) => {
            this.setState({
                loading: false,
                header: res.data.name,
                info: createInfo(ctype, res.data),
            });
        });

        const returnState = {
            loading: true,
        };
        return returnState;
    };

    fetch = (curProps: ClassificationProps): Partial<ClassificationState> => {
        let returnState: Partial<ClassificationState> = {};

        // a parent-type classification type has been selected
        if (this.state.isSingular) {
            returnState = this.createParentInfo(curProps);
        } else {
            returnState.header = capitalize(ClassificationDef.definitions[curProps.category].name);
        }

        // only a table
        if (curProps.hasTable) {
            returnState = {
                ...returnState,
                ...fetchForTable(curProps, this.state.isSingular),
            };
        }

        // only a list
        if (curProps.hasList) {
            returnState = {
                ...returnState,
                ...fetchForList(curProps, this.state.isSingular),
            };
        }

        return returnState;
    };

    onResize = () => {
        if (this.hasOwnProperty("table") && this.table !== undefined && this.table !== null) {
            this.table.updateSize();
        }
    };

    componentWillReceiveProps(nextProps: ClassificationProps) {
        this.setState({ ...this.state, ...this.fetch(nextProps) });
    }

    render() {
        let items = [];
        items.push(
            <div key={items.length} className={"page-header"}>
                {this.state.header}
            </div>
        );

        if (this.state.info) {
            items.push(
                <div key={items.length}>
                    <Info {...this.state.info} />
                </div>
            );
        } else if (this.state.loading) {
            items.push(
                <div key={items.length} className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        if (this.state.listData) {
            items.push(
                <div key={items.length} className="list-section">
                    {this.state.listHeader && (
                        <div className={"list-header"}>{this.state.listHeader}:</div>
                    )}
                    <List
                        {...this.state.listData}
                        expandByDefault={this.props.hasTable}
                        onResize={this.onResize}
                    />
                </div>
            );
        }

        if (this.state.tableData) {
            items.push(
                <div key={items.length} className="table-section">
                    <Table {...this.state.tableData} ref={(ref: any) => (this.table = ref)} />
                </div>
            );
        }

        return <div className="classification">{items}</div>;
    }
}

export default connector(Classification);
