import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { Assets } from "../../config/Assets";
import { Classification } from "../../config/Classification";
import { Urls } from "../../config/Urls";
import { sortStrings } from "../../config/Util";
import { RootState } from "../../store";
import ExternalLink from "../external_link/ExternalLink";
import Info, { ClassificationInfo, InfoProps } from "../info/Info";
import { RelatedPathways } from "../protein/related_pathways/RelatedPathways";
import PubMedLink from "../pubmed_link/PubMedLink";
import ProteinList from "../list/protein/ProteinList";
import "./Complex.scss";
import PDB from "./pdb/PDB";

function mapStateToProps(state: RootState) {
    return {
        currentUrl: state.currentUrl,
    };
}

const connector = connect(mapStateToProps);

export type ComplexProps = ConnectedProps<typeof connector>;

type ComplexState = {
    header: string;
    hasData: boolean;
    complex: any;
    info: InfoProps;
};

function createInfo(obj: any): InfoProps {
    let complex_classes: Array<ClassificationInfo> = [];
    for (let i = 0; i < obj.complex_classes.length; ++i) {
        const c = obj.complex_classes[i];
        complex_classes.push({
            id: c.id,
            name: c.name,
            subclasses: c.protein_superfamilies_count,
        });
    }

    let complex_superfamilies: Array<ClassificationInfo> = [];
    for (let i = 0; i < obj.complex_superfamilies.length; ++i) {
        const sf = obj.complex_superfamilies[i];
        complex_superfamilies.push({
            id: sf.id,
            name: sf.name,
            subclasses: sf.protein_families_count,
            pfam: sf.pfam,
        });
    }

    let complex_families: Array<ClassificationInfo> = [];
    for (let i = 0; i < obj.complex_families.length; ++i) {
        const fam = obj.complex_families[i];
        complex_families.push({
            id: fam.id,
            name: fam.name,
            subclasses: fam.complex_structures_count,
            interpro: fam.interpro,
        });
    }

    let complex_localizations: Array<ClassificationInfo> = [];
    for (let i = 0; i < obj.membranes.length; ++i) {
        const mem = obj.membranes[i];
        complex_localizations.push({
            id: mem.membrane_id,
            name: mem.membrane_name_cache,
            subclasses: mem.complex_structures_count_cache,
        });
    }
    return { complex_classes, complex_families, complex_superfamilies, complex_localizations };
}

function renderUniprotLink(key: number, name: string, uniprotId: string | number): JSX.Element {
    return (
        <div key={key}>
            {name} (
            <ExternalLink href={Classification.definitions.proteins.uniprotcode.uniprot(uniprotId)}>
                {uniprotId}
            </ExternalLink>
            )
        </div>
    );
}

function rowColor(rowNum: number): "dark" | "light" {
    return rowNum & 1 ? "light" : "dark";
}

class Complex extends React.Component<ComplexProps, ComplexState> {
    constructor(props: ComplexProps) {
        super(props);
        this.state = {
            header: "",
            hasData: false,
            info: {},
            complex: null,
        };
        this.getData(props.currentUrl.params.id);
    }

    private async getData(id: number | string): Promise<void> {
        const res = await axios.get(
            Urls.baseUrl + Classification.definitions.complex_structures.api.route + "/" + id
        );
        let complex = res.data;

        // sort fields that need to be sorted
        complex.related_proteins = sortStrings(complex.related_proteins, "name");
        complex.topologies = sortStrings(complex.related_proteins, "pdbid");
        complex.complex_classes = sortStrings(complex.complex_classes, "name");
        complex.complex_superfamilies = sortStrings(complex.complex_superfamilies, "name");
        complex.complex_families = sortStrings(complex.complex_families, "name");
        complex.membranes = sortStrings(complex.membranes, "membrane_name_cache");
        complex.pubmed = sortStrings(complex.pubmed.split(",").filter((p: string) => p !== ""));
        complex.pathways = sortStrings(complex.pathways, "pathway_name_cache");

        // split uniprot links by *topic
        const polytopic_uniprot_links: Array<any> = complex.uniprot_links.filter(
            (u: any) => u.type_of_protein === "polytopic"
        );
        const other_uniprot_links: Array<any> = complex.uniprot_links.filter(
            (u: any) => u.type_of_protein !== "polytopic"
        );
        complex.polytopic_uniprot_links = sortStrings(polytopic_uniprot_links, "uniprot_name");
        complex.other_uniprot_links = sortStrings(other_uniprot_links, "uniprot_name");

        // pdb links
        complex.pdb_links = sortStrings(complex.pdb_links, "pdb");
        for (let i = 0; i < complex.pdb_links.length; ++i) {
            complex.pdb_links[i].uniprots = sortStrings(complex.pdb_links[i].uniprots, "uniprotid");
        }

        // subcomplexes
        complex.subcomplexes = sortStrings(complex.subcomplexes, "name");
        for (let i = 0; i < complex.subcomplexes.length; ++i) {
            complex.subcomplexes[i].uniprotids = sortStrings(
                complex.subcomplexes[i].uniprotids.split(",")
            );
            complex.subcomplexes[i].pdbcodes = sortStrings(
                complex.subcomplexes[i].pdbcodes.split(",")
            );
            complex.subcomplexes[i].pubmed = sortStrings(complex.subcomplexes[i].pubmed.split(","));
        }

        this.setState({
            header: complex.name,
            hasData: true,
            info: createInfo(complex),
            complex,
        });
    }

    renderInfo(): React.ReactNode {
        return (
            <div>
                <Info {...this.state.info} />
            </div>
        );
    }

    renderProteinsCell(): React.ReactNode {
        if (this.state.complex.related_proteins.length === 0) {
            return <span>none</span>;
        }
        let items: React.ReactNodeArray = [];
        for (let i = 0; i < this.state.complex.related_proteins.length; i++) {
            const protein = this.state.complex.related_proteins[i];
            items.push(
                <div key={i} className="spaced">
                    <Link to={"/proteins/" + protein.id}>{protein.name}</Link> (
                    <ExternalLink
                        href={Classification.definitions.proteins.uniprotcode.uniprot(
                            protein.pdbid
                        )}
                    >
                        {protein.pdbid}
                    </ExternalLink>
                    )
                </div>
            );
        }
        return items;
    }

    renderProteinsList(): React.ReactNode {
        return (<>
            <h2>Bitopic Proteins</h2>
            <ProteinList data={{ objects: this.state.complex.related_proteins }} />
        </>);
    }

    renderOtherUniprotLinks(): React.ReactNode {
        if (this.state.complex.other_uniprot_links.length === 0) {
            return <span>none</span>;
        }
        let items: React.ReactNodeArray = [];
        for (let i = 0; i < this.state.complex.other_uniprot_links.length; i++) {
            const unipr = this.state.complex.other_uniprot_links[i];
            items.push(renderUniprotLink(i, unipr.uniprot_name, unipr.uniprotid));
        }
        return items;
    }

    renderPolytopicUniprotLinks(): React.ReactNode {
        if (this.state.complex.polytopic_uniprot_links.length === 0) {
            return <span>none</span>;
        }
        let items: React.ReactNodeArray = [];
        for (let i = 0; i < this.state.complex.polytopic_uniprot_links.length; i++) {
            const unipr = this.state.complex.polytopic_uniprot_links[i];
            items.push(renderUniprotLink(i, unipr.uniprot_name, unipr.uniprotid));
        }
        return items;
    }

    renderTableRow(right: React.ReactNode, left: React.ReactNode, attr: string): JSX.Element {
        return (
            <tr className={attr}>
                <td className="right">
                    <b>{right}</b>
                </td>
                <td className="left">{left}</td>
            </tr>
        );
    }

    componentWillReceiveProps(nextProps: ComplexProps): void {
        if (nextProps.currentUrl.params.id !== this.props.currentUrl.params.id) {
            this.getData(nextProps.currentUrl.params.id);
            this.setState({ hasData: false });
        }
    }

    renderTopology(): React.ReactNode {
        if (this.state.complex.topologies.length === 0) {
            return <span>none</span>;
        }

        let items: React.ReactNodeArray = [];
        for (let i = 0; i < this.state.complex.topologies.length; ++i) {
            const protein = this.state.complex.topologies[i];
            let inOrOut = "und";
            if (protein.topology_show_in !== null) {
                inOrOut = protein.topology_show_in ? "in" : "out";
            }
            items.push(
                <div key={i} className="spaced">
                    {protein.pdbid}: <b>{inOrOut}</b>
                </div>
            );
        }
        return items;
    }

    renderSubcomplexes(): React.ReactNode {
        if (this.state.complex.subcomplexes.length === 0) {
            return <span>none</span>;
        }

        let subcomplexes: React.ReactNodeArray = [];
        for (let i = 0; i < this.state.complex.subcomplexes.length; ++i) {
            const subcomplex = this.state.complex.subcomplexes[i];
            const pdb_links = [];
            for (let j = 0; j < subcomplex.pdbcodes.length; ++j) {
                if (subcomplex.pdbcodes[j] !== "") {
                    pdb_links.push(
                        <span key={j}>
                            {j > 0 && ", "}
                            <ExternalLink
                                href={Classification.definitions.proteins.pdb(
                                    subcomplex.pdbcodes[j]
                                )}
                            >
                                {subcomplex.pdbcodes[j]}
                            </ExternalLink>
                        </span>
                    );
                }
            }
            const uniprot_links = [];
            for (let j = 0; j < subcomplex.uniprotids.length; ++j) {
                if (subcomplex.uniprotids[j] !== "") {
                    uniprot_links.push(
                        <span key={j}>
                            {j > 0 && ", "}
                            <ExternalLink
                                href={Classification.definitions.proteins.uniprotcode.uniprot(
                                    subcomplex.uniprotids[j]
                                )}
                            >
                                {subcomplex.uniprotids[j]}
                            </ExternalLink>
                        </span>
                    );
                }
            }
            const pubmed_links = [];
            for (let j = 0; j < subcomplex.pubmed.length; ++j) {
                const pmid = subcomplex.pubmed[j];
                pubmed_links.push(
                    <span key={j}>
                        {j > 0 && ", "}
                        <PubMedLink id={pmid}>{pmid}</PubMedLink>
                    </span>
                );
            }
            subcomplexes.push(
                <div key={i} className="spaced">
                    {subcomplex.name}
                    {"; "}
                    {uniprot_links.length > 0 && <span>{uniprot_links}; </span>}
                    {pdb_links.length > 0 && <span>{pdb_links}; </span>}
                    {pubmed_links.length > 0 && <span>PubMed: {pubmed_links}; </span>}
                    Source: {this.renderSourceDB(subcomplex.source_db, subcomplex.source_db_link)}
                    <br />
                </div>
            );
        }
        return subcomplexes;
    }

    renderPdbLinks(): React.ReactNode {
        if (this.state.complex.pdb_links.length === 0) {
            return <span>none</span>;
        }

        let pdbLinks: React.ReactNodeArray = [];
        for (let i = 0; i < this.state.complex.pdb_links.length; ++i) {
            const pdb_link = this.state.complex.pdb_links[i];
            let uniprotids: React.ReactNodeArray = [];
            for (let j = 0; j < pdb_link.uniprots.length; ++j) {
                const id: string = pdb_link.uniprots[j].uniprotid;
                uniprotids.push(
                    <span key={j}>
                        {j > 0 && ", "}
                        <ExternalLink
                            href={Classification.definitions.proteins.uniprotcode.uniprot(id)}
                        >
                            {id}
                        </ExternalLink>
                    </span>
                );
            }
            pdbLinks.push(
                <div key={i} className="spaced">
                    <ExternalLink href={Classification.definitions.proteins.pdb(pdb_link.pdb)}>
                        {pdb_link.pdb}
                    </ExternalLink>{" "}
                    {uniprotids.length > 0 && <span>({uniprotids}) </span>}
                    resolution:{" "}
                    {pdb_link.resolution === null ? "N/A" : pdb_link.resolution.toFixed(2)}
                    <br />
                </div>
            );
        }
        return pdbLinks;
    }

    renderPubMed(): React.ReactNode {
        if (this.state.complex.pubmed.length === 0) {
            return <span>none</span>;
        }

        let items = [];
        for (let i = 0; i < this.state.complex.pubmed.length; ++i) {
            const pmid = this.state.complex.pubmed[i];
            items.push(
                <span key={i}>
                    {i > 0 && ", "}
                    <PubMedLink id={pmid}>{pmid}</PubMedLink>
                </span>
            );
        }
        return <span>{items}</span>;
    }

    renderSourceDB(sourceDb: string, sourceDbLink: string): React.ReactNode {
        if (!sourceDb) {
            return <span>none;</span>;
        }

        if (sourceDb.toUpperCase() === "CORUM" && !sourceDbLink) {
            sourceDbLink = Urls.corum;
        } else if (sourceDb.toUpperCase() === "PDB" && this.state.complex.refpdbcode) {
            sourceDb += ` (${this.state.complex.refpdbcode})`;
        }

        return <ExternalLink href={sourceDbLink}>{sourceDb}</ExternalLink>;
    }

    render(): JSX.Element {
        if (!this.state.hasData) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        let rowNum = 0;

        return (
            <div className="protein-complex">
                <div className="page-header">{this.state.complex.name}</div>
                <div className="protein-content">
                    <div className="protein-info">{this.renderInfo()}</div>
                    <br />
                    <div className="pdb-container">
                        {this.state.complex.refpdbcode && (
                            <PDB
                                id={this.state.complex.id}
                                pdbid={this.state.complex.refpdbcode}
                                name={this.state.complex.name}
                            />
                        )}
                    </div>
                    <div className="info-table">
                        <table>
                            <tbody>
                                <tr className={rowColor(rowNum++)}>
                                    <th colSpan={2}>{this.state.complex.name}</th>
                                </tr>
                                {this.state.complex.gibbs != 0 &&
                                    this.renderTableRow(
                                        "Hydrophobic Thickness",
                                        this.state.complex.thickness +
                                            " ± " +
                                            this.state.complex.thicknesserror +
                                            " Å",
                                        rowColor(rowNum++)
                                    )}
                                {this.state.complex.gibbs != 0 &&
                                    this.renderTableRow(
                                        "Tilt Angle",
                                        this.state.complex.tilt +
                                            " ± " +
                                            this.state.complex.tilterror +
                                            "°",
                                            rowColor(rowNum++)
                                    )}
                                {this.renderTableRow("Species", this.state.complex.related_proteins[0].species_name_cache, rowColor(rowNum++))}
                                {this.renderTableRow(
                                    "Bitopic proteins",
                                    this.renderProteinsCell(),
                                    rowColor(rowNum++)
                                )}
                                {this.renderTableRow(
                                    "Polytopic proteins",
                                    this.renderPolytopicUniprotLinks(),
                                    rowColor(rowNum++)
                                )}
                                {this.renderTableRow(
                                    "Other proteins",
                                    this.renderOtherUniprotLinks(),
                                    rowColor(rowNum++)
                                )}
                                {this.renderTableRow(
                                    "Subcomplexes",
                                    this.renderSubcomplexes(),
                                    rowColor(rowNum++)
                                )}
                                {this.renderTableRow("PDB", this.renderPdbLinks(), rowColor(rowNum++))}
                                {this.renderTableRow(
                                    "Reference structure",
                                    this.state.complex.refpdbcode || "none",
                                    rowColor(rowNum++)
                                )}
                                {/* TEMPORARY: Pathways are incorrect. Do not show until fixed. */}
                                {/* {this.renderTableRow(
                                    "Pathways",
                                    <RelatedPathways pathways={this.state.complex.pathways} />,
                                    rowColor(rowNum++)
                                )} */}
                                {this.renderTableRow("Topology", this.renderTopology(), rowColor(rowNum++))}
                                {this.renderTableRow("Comments",this.state.complex.comments || "none", rowColor(rowNum++))}
                                {this.renderTableRow("PubMed", this.renderPubMed(), rowColor(rowNum++))}
                                {this.renderTableRow(
                                    "Source",
                                    this.renderSourceDB(
                                        this.state.complex.source_db,
                                        this.state.complex.source_db_link
                                    ),
                                    rowColor(rowNum++)
                                )}
                            </tbody>
                        </table>
                    </div>
                    {this.renderProteinsList()}
                </div>
            </div>
        );
    }
}

export default connector(Complex);
