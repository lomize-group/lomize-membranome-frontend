import * as React from "react";
import { Classification } from "../../../config/Classification";
import ExternalLink from "../../external_link/ExternalLink";
import GLmolViewer from "../../glmol/GLmolViewer";
import "./PDB.scss";

export type PDBProps = {
    id: number;
    pdbid: string;
    name: string;
};

export default function PDB(props: PDBProps): JSX.Element {
    return (
        <div className="complex-pdb">
            <div>
                <GLmolViewer
                    pdbid={props.pdbid}
                    ctype="complex_structures"
                    style={{
                        backgroundColor: "white",
                        width: "100%",
                        height: "300px",
                    }}
                    components={{
                        colorBy: {
                            show: false,
                        },
                        sideChains: {
                            show: true,
                        },
                        table: false,
                        footer: false,
                    }}
                    autoScaleInitialRender
                />
            </div>
            <hr className="divider" />
            <div className="centered">
                <div className="download-link">
                    <a
                        href={Classification.definitions.complex_structures.pdb_file(props.pdbid)}
                        download
                    >
                        Download File: {props.pdbid}.pdb
                    </a>
                </div>
                <div>
                    3D view in{" "}
                    <a target="_blank" href={"/complex_structures/" + props.id + "/glmol_viewer"}>
                        GLMol
                    </a>
                    {" or "}
                    <ExternalLink
                        href={Classification.definitions.complex_structures.jmol(props.pdbid)}
                    >
                        Jmol
                    </ExternalLink>
                </div>
            </div>
        </div>
    );
}
