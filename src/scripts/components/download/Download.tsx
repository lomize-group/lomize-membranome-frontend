import * as appendQuery from "append-query";
import * as React from "react";
import SwaggerUI from "swagger-ui-react";
import "swagger-ui-react/swagger-ui.css";
import { Assets } from "../../config/Assets";
import { Classification, CType } from "../../config/Classification";
import { Urls } from "../../config/Urls";
import "./Download.scss";

const SWAGGER_JSON = "/swagger-membranome-api.json" as const;

export default class Download extends React.Component {
    renderDownloadCSV(): JSX.Element {
        let items: Array<React.ReactNode> = [];
        let csv_param: any = {
            fileFormat: "csv",
        };

        for (let i: number = 0; i < Classification.tables.length; ++i) {
            // get specific classification for this type
            const tablename: CType = Classification.tables[i];
            const url: string = appendQuery(
                Urls.baseUrl + Classification.definitions[tablename].api.route,
                csv_param
            );
            items.push(
                <li key={i}>
                    <a href={url} download>
                        Download {tablename}
                    </a>
                </li>
            );
        }

        return (
            <div className="download-csv">
                <hr />
                <h3>CSV Files</h3>
                <ul className="csv-list">{items}</ul>
                <hr />
            </div>
        );
    }

    render(): JSX.Element {
        return (
            <div className="download">
                <div className="tar-file">
                    <a href={Assets.protein_pdb_tar} download>
                        Download all modified AlphaFold models of proteins
                    </a>
                    <br />
                    <a href={Assets.complex_pdb_tar} download>
                        Download tar file of all complex pdb files
                    </a>
                    <br />
                    <a href={Assets.dimer_xlsx} download>
                        Table of homodimer (model #1) parameters (.xlsx), v.2.0
                    </a>
                    <br />
                    <a href={Assets.dimers_pdbs} download>
                        Download pdb files of all TM homodimers calculated by TMDOCK
                    </a>
                    <br />
                    <a href={Assets.TM_helices} download>
                        Download pdb files of all TM alpha-helices calculated by FMAP
                    </a>
                    <br />
                    {this.renderDownloadCSV()}
                    <br />
                </div>
                <SwaggerUI docExpansion="none" url={SWAGGER_JSON} />
            </div>
        );
    }
}
