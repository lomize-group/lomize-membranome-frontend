import * as React from "react";
import { prependHttps } from "../../config/Util";
import "./ExternalLink.scss";

type ExternalLinkProps = {
    href: string;
    prepend_https?: boolean;
};

const ExternalLink: React.FC<ExternalLinkProps> = ({ href, children, prepend_https = false }) => (
    <a className="external-link" target="_blank" href={prepend_https ? prependHttps(href) : href}>
        {children}
    </a>
);

export default ExternalLink;
