import axios from "axios";
import "bootstrap";
import * as React from "react";
import { Link } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import * as appendQuery from "append-query";
import { Assets } from "../../config/Assets";
import { ComputationServer, Urls } from "../../config/Urls";
import { validEmail, validFilename } from "../../config/Util";
import { RootState } from "../../store";
import Submitted from "../submitted/Submitted";
import "./FMAP.scss";

const mapStateToProps = (state: RootState) => ({
    currentUrl: state.currentUrl,
});

const connector = connect(mapStateToProps);

type FMAPProps = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer,
    submitted?: boolean;
};

type MicelleType = "sds" | "dpc" | "lds" | "dhpc" | "diameter";
type MembraneType = "dopc" | "dmpc" | "dlpc" | "depc" | "bact" | "plas" | "reti" | "mito"
                    | "thyl" | "arch" | "gpos";

type FMAPFormData = {
    model: string;
    gen3dModels: boolean;
    ph: number;
    temperature: number;
    userEmail?: string;
    outputFilename?: string;
    micelleType: MicelleType;
    micelleDiameter: string | undefined;
    membraneType: MembraneType;
    predefinedSegments: string;
    sequence: string;
};

type FMAPState = FMAPFormData & {
    loading: boolean;
    instructions: boolean;
    errorText: string;
};

function getInitialState(): FMAPState {
    return {
        loading: false,
        instructions: false,
        model: "Peptide in water",
        gen3dModels: true,
        ph: 7.0,
        temperature: 300,
        errorText: "",
        sequence: "",
        outputFilename: "",
        micelleType: "sds",
        micelleDiameter: undefined,
        membraneType: "dopc",
        predefinedSegments: "",
        userEmail: undefined,
    };
}

class FMAP extends React.Component<FMAPProps, FMAPState> {
    state: FMAPState = getInitialState();

    handleInputChange = (event: any) => {
        this.setState({ [event.target.name]: event.target.value } as FMAPState);
    };

    handleReset = (): void => {
        this.setState(getInitialState());
    };

    submit = (): void => {
        const { computationServer } = this.props;
        let errors: Array<string> = [];

        if (this.state.userEmail && !validEmail(this.state.userEmail)) {
            errors.push("invalid email");
        }
        if (!this.state.model) {
            errors.push("you must select a model");
        }
        if (!this.state.ph || this.state.ph < 0 || this.state.ph > 14) {
            errors.push("you must enter a valid pH level");
        }
        if (!this.state.temperature || this.state.temperature < 0) {
            errors.push("you must enter a valid temperature in Kelvin");
        }
        if (!this.state.sequence || this.state.sequence == "") {
            errors.push("you must enter a valid amino acid sequence");
        }
        if (this.state.outputFilename && !validFilename(this.state.outputFilename)) {
            errors.push("filename must be alphanumeric");
        }

        if (this.state.model === "pep_micelles" && this.state.micelleType === "diameter") {
            if (this.state.micelleDiameter === undefined) {
                errors.push("you must enter a micelle diameter");
            } else {
                // https://stackoverflow.com/a/10834843
                const isValidInteger = (str: string) => {
                    const n = Math.floor(Number(str));
                    return n !== Infinity && String(n) === str && n > 0;
                }

                if (!isValidInteger(this.state.micelleDiameter)) {
                    errors.push("you must enter a positive integer for micelle diameter");
                }
            }
        }

        this.setState({ errorText: errors.join("; ") });

        if (errors.length > 0) {
            return;
        }

        let data: FMAPFormData = {
            model: this.state.model,
            gen3dModels: this.state.gen3dModels,
            ph: this.state.ph,
            temperature: this.state.temperature,
            sequence: this.state.sequence,
            micelleType: this.state.micelleType,
            micelleDiameter: this.state.micelleDiameter,
            membraneType: this.state.membraneType,
            predefinedSegments: this.state.predefinedSegments,
        };

        if (this.state.userEmail) {
            data.userEmail = this.state.userEmail;
        }
        if (this.state.outputFilename) {
            data.outputFilename = this.state.outputFilename;
        }

        /*
        console.log(JSON.stringify(data));
        return;
        */

        axios
            .post(Urls.computation_servers[computationServer] + "fmap", data)
            .then((res) => {
                // push url for submitted page
                const urlParams = {
                    resultsUrl: res.data.resultsUrl || "",
                    waitingUrl: res.data.waitingUrl || "",
                    message: res.data.message,
                };
                let s = this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === '/' ? "submitted" : "/submitted";
                const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
                this.props.currentUrl.history.push(submitUrl);
                this.setState({ loading: false });
            })
            .catch((err) => {
                this.setState({
                    errorText: "An error occurred when submitting: " + JSON.stringify(err),
                    loading: false,
                });
            });

        this.setState({ loading: true });
    };

    renderInputSection(): React.ReactNode {
        return (
            <tr>
                <td colSpan={2} className="table-section dark">
                    <div className="input-type-choices centered">
                        <button name="paste" className="selected">
                            paste a single amino acid sequence
                        </button>
                        <br />
                        <span className="subtext centered">
                            <b>FASTA format</b>
                        </span>
                    </div>
                    <div className="user-input-section">
                        <label className="amino-sequence-label" htmlFor="amino_acid_sequence">
                            <b>
                                Enter your sequence below
                            </b>
                            {' '}
                            (single-letter code; acetylated and amidated termini can be shown as -)
                        </label>
                        <br />
                        <textarea
                            id="sequence"
                            name="sequence"
                            className="text-input"
                            placeholder="Enter your text here"
                            value={this.state.sequence}
                            onChange={this.handleInputChange}
                        />
                    </div>
                </td>
            </tr>
        );
    }

    renderOutputSection(): React.ReactNode {
        return (
            <tr className="output-section">
                <td colSpan={2} className="table-section">
                    <div>
                        {/* <b className="sub-title">Generate 3D models of alpha-helices:</b>
                        <div className="input-subsection">
                            <span className="inline-radio">
                                <input
                                    id="gen3dModelsYes"
                                    name="gen3dModels"
                                    type="radio"
                                    checked={this.state.gen3dModels}
                                    onChange={() => {
                                        this.setState({ gen3dModels: true });
                                    }}
                                />{" "}
                                <label htmlFor="gen3dModelsYes">Yes</label>
                            </span>
                            <span className="inline-radio">
                                <input
                                    id="gen3dModelsNo"
                                    name="gen3dModels"
                                    type="radio"
                                    checked={!this.state.gen3dModels}
                                    onChange={() => {
                                        this.setState({ gen3dModels: false });
                                    }}
                                />{" "}
                                <label htmlFor="gen3dModelsNo">No</label>
                            </span>
                        </div> */}
                        <div className="input-subsection">
                            <b className="sub-title">Name of output PDB file:</b>
                            <br />
                            <input
                                id="outputFilename"
                                name="outputFilename"
                                type="text"
                                value={this.state.outputFilename}
                                onChange={this.handleInputChange}
                            />{" "}
                            <b className="subtext">
                                ex: abcd (.pdb extension will be added automatically)
                            </b>
                        </div>
                        <div className="input-subsection">
                            <b className="sub-title">User Email (optional):</b>
                            <br />
                            <input
                                id="userEmail"
                                name="userEmail"
                                type="text"
                                value={this.state.userEmail}
                                onChange={this.handleInputChange}
                            />{" "}
                            <b className="subtext">
                                (this will send you an email with your results)
                            </b>
                        </div>
                    </div>
                    <div className="submit-buttons">
                        <button onClick={this.submit}>
                            <u>S</u>ubmit
                        </button>
                        <button onClick={this.handleReset}>
                            <u>R</u>eset
                        </button>{" "}
                        <span className="error-msg">{this.state.errorText}</span>
                    </div>
                </td>
            </tr>
        );
    }

    renderHeader(): React.ReactNode {
        const { computationServer } = this.props;
        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;
        return (
            <div>
                <div className="server-header">FMAP 2.0 Server ({serverType})</div>
                <div>
                    <p>
                        The FMAP (<b>F</b>olding of <b>M</b>embrane-<b>A</b>ssociated{" "}
                        <b>P</b>eptides) server predicts the existence of individually 
                        stable alpha-helices formed by peptides in aqueous solution, 
                        micelles, and lipid bilayers and provides their all-atom models 
                        positioned with respect to the flat lipid bilayer or spherical micelles.
                        The source code to our computational server can be found 
                        on <a href="https://cggit.cc.lehigh.edu/biomembhub/fmap_server_code">GitLab</a>.
                        
                        <br />
                        <br />

                        FMAP 2.0 uses our previously developed methods, including the
                        thermodynamic model of helix-coil transition, Framework 
                        (Lomize and Mosberg, <i>Biopolymers</i>. 1997, <b>42</b>: 239-269{" "}
                        <a
                            href="https://pubmed.ncbi.nlm.nih.gov/9235002/"
                            className="external"
                            target="_blank"
                        >
                            PubMed
                        </a>
                        ), Positioning of Proteins in Membranes, PPM 2.0 method 
                        (Lomize et al., <i>J. Chem. Inf. Model.</i>, 2011, <b>51</b>: 930-946{" "}
                        <a
                            href="https://pubmed.ncbi.nlm.nih.gov/21438606/"
                            className="external"
                            target="_blank"
                        >
                            PubMed
                        </a>
                        ), and optimization with potential energy functions 
                        derived from protein engineering data (
                        <a
                        href="https://pubmed.ncbi.nlm.nih.gov/27622289/"
                        className="external"
                        target="_blank"
                        >
                            PubMed
                        </a>
                        ). 

                        <br />
                        <br />

                        FMAP 2.0 was tested for 723 peptides (926 data points) experimentally
                        studied in different environments and for 170 single-pass transmembrane (TM)
                        proteins with available crystal structures. More than 95% of experimentally
                        observed &#120572;-helices were identified with an average error in helix
                        end determination of around 2, 3, 4, and 5 residues per helix for peptides
                        in water, micelles, bilayers, and TM proteins, respectively. Experimental
                        micelle- and membrane-binding energies and tilt angles of peptides were
                        reproduced with rmse of around 2 kcal/mol and 7&deg;, respectively. The
                        testing datasets are available
                        {' '}
                        <a
                            href="https://storage.googleapis.com/membranome-assets/fmap_code/fmap_datasets/FMAP_testing_datasets.xlsx"
                            className="external"
                        >
                            here
                        </a>
                        .
                    </p>
                </div>
                <div className="fmap-buttons">
                    <button
                        onClick={() => this.setState({ instructions: !this.state.instructions })}
                    >
                        Instructions {this.state.instructions ? "-" : "+"}
                    </button>
                    <button onClick={this.submit}>Run FMAP</button>
                    <button onClick={this.handleReset}>Reset</button>{" "}
                    <span className="error-msg">{this.state.errorText}</span>
                </div>
            </div>
        );
    }

    renderMicelleDiameter() {
        if (this.state.model !== "pep_micelles" || this.state.micelleType !== "diameter") return null;
        return (
            <>
                <br />
                <label htmlFor="micelleDiameter">
                    <b className="sub-title">
                        Micelle diameter (Å)
                    </b>
                </label>
                <input
                    id="micelleDiameter"
                    type="number"
                    name="micelleDiameter"
                    value={this.state.micelleDiameter}
                    min="1"
                    onChange={this.handleInputChange}
                />
            </>
        );
    }

    renderOptions(): React.ReactNode {
        return (
            <tr>
                <td className="table-section subtext">
                    <b className="sub-title">Model</b>
                    <br />
                    <input
                        id="peptide_in_water"
                        name="model"
                        type="radio"
                        value="Peptide in water"
                        checked={this.state.model === "Peptide in water"}
                        onChange={this.handleInputChange}
                    />{" "}
                    <label htmlFor="peptide_in_water">Peptide in water</label>
                    <br />
                    <input
                        id="peptide_in_micelle"
                        name="model"
                        type="radio"
                        value="pep_micelles"
                        checked={this.state.model === "pep_micelles"}
                        onChange={this.handleInputChange}
                    />{" "}
                    <label htmlFor="peptide_in_micelle">Peptide in micelle</label>
                    {this.state.model === "pep_micelles" ? <br /> : null}
                    <span
                        className={
                            this.state.model === "pep_micelles" ? "model-selection" : "hidden"
                        }
                    >
                        <label htmlFor="micelleType">
                            <b className="sub-title">Micelle type</b>
                        </label>{" "}
                        <select
                            id="micelleType"
                            name="micelleType"
                            value={this.state.micelleType}
                            onChange={this.handleInputChange}
                        >
                            <option value="sds">SDS</option>
                            <option value="dpc">DPC</option>
                            <option value="lps">LPS</option>
                            <option value="dhpc">DHPC</option>
                            <option value="diameter">Diameter</option>
                        </select>
                        {this.renderMicelleDiameter()}
                    </span>
                    <br />
                    <input
                        id="peptide_in_membrane"
                        name="model"
                        type="radio"
                        value="pep_membrane"
                        checked={this.state.model === "pep_membrane"}
                        onChange={this.handleInputChange}
                    />{" "}
                    <label htmlFor="peptide_in_membrane">Peptide in membrane</label>
                    <span
                        className={
                            this.state.model === "pep_membrane" ? "model-selection" : "hidden"
                        }
                    >
                        <label htmlFor="membraneType">
                            <b className="sub-title">Membrane type</b>
                        </label>{" "}
                        <select
                            id="membraneType"
                            name="membraneType"
                            value={this.state.membraneType}
                            onChange={this.handleInputChange}
                        >
                            <option value={"dopc"}>DOPC</option>
                            <option value={"dmpc"}>DMPC</option>
                            <option value={"dlpc"}>DLPC</option>
                            <option value={"depc"}>DEuPC</option>
                            <option value={"bact"}>Gram-negative IM</option>
                            <option value={"plas"}>Eukaryotic PM</option>
                            <option value={"reti"}>ER</option>
                            <option value={"mito"}>Mitochondrial</option>
                            <option value={"thyl"}>Thylakoid</option>
                            <option value={"arch"}>Archaeal PM</option>
                            <option value={"gpos"}>Gram-positive PM</option>
                        </select>
                    </span>
                    <br />
                    <input
                        id="transmembrane_protein"
                        name="model"
                        type="radio"
                        value="tran_protein"
                        checked={this.state.model === "tran_protein"}
                        onChange={this.handleInputChange}
                    />{" "}
                    <label htmlFor="transmembrane_protein">Transmembrane protein</label>
                    <br />
                    <input
                        id="water_soluble_protein"
                        name="model"
                        type="radio"
                        value="water_protein"
                        checked={this.state.model === "water_protein"}
                        onChange={this.handleInputChange}
                    />{" "}
                    <label htmlFor="water_soluble_protein">
                        Water-soluble protein (molten globule)
                    </label>
                </td>
                <td className="table-section">
                    <b className="sub-title">Experimental conditions for peptides:</b>
                    <table className="inline-table input-subsection">
                        <tbody>
                            <tr>
                                <td>
                                    <label htmlFor="temperature" className="subtext">
                                        T&deg; (K)
                                    </label>
                                    <br />
                                    <input
                                        id="temperature"
                                        name="temperature"
                                        type="number"
                                        min={0}
                                        placeholder="Temperature K"
                                        value={this.state.temperature}
                                        onChange={this.handleInputChange}
                                    />
                                </td>
                                <td>
                                    <label htmlFor="ph" className="subtext">
                                        pH
                                    </label>
                                    <br />
                                    <input
                                        id="ph"
                                        name="ph"
                                        type="number"
                                        placeholder="pH level"
                                        min={0}
                                        max={14}
                                        step={0.1}
                                        value={this.state.ph}
                                        onChange={this.handleInputChange}
                                    />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <b className="sub-title">Predefined alpha-helical segments (optional, for 
                        peptides in micelles or membranes):</b>
                    <br />
                    <input
                        id="predefinedSegments"
                        name="predefinedSegments"
                        type="text"
                        value={this.state.predefinedSegments}
                        onChange={this.handleInputChange}
                    />{" "}
                    <b className="subtext">ex. 1-20, 25-45</b>
                </td>
            </tr>
        );
    }

    renderInstructions(): React.ReactNode {
        return (
            <div className="popup">
                
                <h1>Models implemented in FMAP:</h1>
                <ol>
                    <li>
                        <b>Peptide in water.</b> This model predicts formation of stable 
                        alpha-helices in aqueous solution that can be detected by NMR spectroscopy 
                        depending on the temperature and pH.
                    </li>
                    <li>
                        <b>Peptide in micelle (SDS, DPC, LPS, DHPC).</b> This model predicts 
                        formation of alpha-helices by peptides, calculates their stability at the 
                        specified temperature and pH, and generates 3D models of alpha-helices in 
                        the chosen type of a spherical micelle. The hydrophobic diameters of 
                        micelles are fixed at 37 Å for SDS, 39 Å for DPC, 50 Å for LPS, and 26 Å 
                        for DHPC. As an alternative, the hydrophobic diameter of a micelle (&lt;50
                        Å) can be also defined by user.
                    </li>
                    <li>
                        <b>Peptide in membrane (DOPC).</b> This model identifies alpha-helical 
                        segments in the amino acid sequence, calculates their stability at specified
                        temperature and pH, and generates 3D models of alpha-helices positioned in 
                        the planar DOPC bilayer. The hydrophobic thickness of the DOPC bilayer is 
                        fixed at 28.8 Å.
                    </li>
                    <li>
                        <b>Transmembrane protein.</b> This model predicts transmembrane 
                        alpha-helices in single-pass membrane proteins and generates their 3D models
                        positioned in the DOPC bilayer.
                    </li>
                    <li>
                        <b>Water-soluble protein (molten globule state).</b> This model identifies 
                        stable alpha-helices of water-soluble proteins. It is based on the 
                        assumption that alpha-helices are stabilized by non-specific hydrophobic 
                        interactions with the “molten globule” created by the rest of the protein. 
                    </li>
                </ol>
                <div>
                    <h1>Input</h1>
                    <p>
                        The input to FMAP is a one-letter amino acid sequence of a protein or a 
                        peptide of interest. Modified (acetylated and amidated) N- and C-termini can
                        be indicated as the first or last “-“ symbol in a peptide sequence. User can
                         choose one of the provided <b>models</b> (see above) and define
                         experimental conditions (pH and temperature, K) that are used only for
                         models with peptides. The user can also
                         predefine the location of α-helices in the input amino acid sequence. Then
                         FMAP only quickly generates 3D models of the predefined α-helices in
                         bilayer or a micelle.
                    </p>
                </div>
                <div>
                    <h1>Output</h1>
                    <div className="output">
                        <b>Alpha-helical segments and their stability.</b> Depending on the selected
                        model, FMAP generates the list of predicted alpha-helical segments and their
                        stabilities relative to coil in water (model 4 provides only transmembrane
                        helices). Free energy of every micelle- or membrane-bound alpha-helical
                        segment is a sum of helix folding energy in water and transfer energy of the
                        helix from water to the protein, micelle or membrane interior, depending on
                        the model. Note that for peptides (models 1, 2 and 3) FMAP predicts
                        α-helices which would be detected by solution NMR spectroscopy. Some of them
                        may be slightly less stable than coil (α-helices with occupancy &gt;20% are
                        typically detectable by NMR) or originate from averaging of several
                        different α-helical segments. The calculated lowest energy helix-coil
                        partition is included in messages. It provides the most stable α-helices.
                    </div>
                    <div className="output">
                        <b>Coordinate files of alpha-helices in membranes.</b> For models 2, 3 and 4
                        FMAP generates all-atom 3D structures (without hydrogens) of individual
                        alpha-helices and optimizes their spatial positions with respect to the
                        planar lipid bilayer or a spherical micelle. The downloadable coordinate
                        files are provided in the PDB format. DUMMY atoms show boundaries of the
                        nonpolar acyl chain region of the bilayer of micelle.  If there are several
                        alpha-helical segments in a protein, each segment is provided as a separate
                        MODEL in the file.
                    </div>
                    <div className="output">
                        <b>Model visualization.</b> GLmol and Jmol are used for interactive 3D
                        presentation of calculated models. GLmol allows looking at the individual
                        α-helices, which are represented as different MODELs in the output PDB file,
                        by clicking at Helix 1, Helix 2, and so on in a table. Charged, polar,
                        aliphatic and aromatic residues are shown by different colors in GLmol.
                        Jmol shows model of only first helix. The models of multiple helices can be
                        also visualized by downloading the output coordinate file and using PyMOL
                        with "load XXXout.pdb, multiplex=1" command.
                    </div>
                    <div className="output">
                        <b>Messages.</b> The server produces messages in a separate window. They
                        include additional information about α-helices in the lipid bilayer: their
                        tilt angle with respect to the bilayer normal, their maximal membrane
                        penetration depth or hydrophobic thickness, and transfer energy from water
                        to membrane.
                    </div>
                </div>

                <h1>Contact</h1>
                <div>
                    Please send any questions or requests to Andrei Lomize (
                    <a href="mailto:almz@umich.edu">almz@umich.edu)</a>
                </div>
                
                <div>
                <h1>Source code</h1>
				<p>FMAP&nbsp;
                    <a href="https://console.cloud.google.com/storage/browser/membranome-assets/fmap_code">
                        source code
                    </a> to run the program for multiple peptides</p>
                </div>
                <br />
            </div>
        );
    }

    render(): React.ReactNode {
        const { computationServer } = this.props;

        if (this.state.loading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        if (this.props.submitted) {
            return (
                <div className="server">
                    <div className="server-header">FMAP Server</div>
                    <Submitted submitAgain={this.handleReset} userEmail={this.state.userEmail} />
                </div>
            );
        }

        const useOtherServer = computationServer == "memprot"
            ? <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/fmap_cgopm">cgopm-based</Link> version
            </div>
            : <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/fmap">AWS-based</Link> version
            </div>;

        return (
            <div className="server fmap">
                {this.renderHeader()}
                {this.state.instructions && this.renderInstructions()}
                <table className="table-form">
                    <tbody>
                        <tr>
                            <th colSpan={2} className="table-section-header">
                                <b>Input</b>
                            </th>
                        </tr>
                        {this.renderInputSection()}

                        <tr>
                            <th colSpan={2} className="table-section-header">
                                <b>Options</b>
                            </th>
                        </tr>
                        {this.renderOptions()}

                        <tr>
                            <th colSpan={2} className="table-section-header">
                                <b>Output</b>
                            </th>
                        </tr>
                        {this.renderOutputSection()}
                    </tbody>
                </table>
                {useOtherServer}
            </div>
        );
    }
}

export default connector(FMAP);
