import * as appendQuery from "append-query";
import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { ComputationServer, Urls } from "../../config/Urls";
import { RootState } from "../../store";
import Submitted from "../submitted/Submitted";
import "./Fold.scss";

const mapStateToProps = (state: RootState) => ({
    currentUrl: state.currentUrl,
});
const connector = connect(mapStateToProps);

type FoldProps = ConnectedProps<typeof connector> & {
    submitted?: boolean,
};

type TemplateMode = "NONE" | "PDB70" | "CUSTOM";
type PairMode = "UNPAIRED_PAIRED" | "PAIRED" | "UNPAIRED";
type NumRecycle = "_1" | "_3" | "_6" | "_12" | "_24" | "_48";

type TemplateFile = {
    name: string,
    contents: string,
};

type FoldForm = {
    sequences: string[],
    useAmber: boolean,
    templateMode: TemplateMode,
    templateFiles: TemplateFile[],
    pairMode: PairMode,
    numRecycles: NumRecycle,
    userEmail: string | null,
};

type FoldState = FoldForm & {
    loading: boolean,
    instructions: boolean,
    errorText: string,
};

const defaultFoldState: FoldState = {
    // form,
    sequences: [""],
    useAmber: false,
    templateMode: "NONE",
    templateFiles: [],
    pairMode: "UNPAIRED_PAIRED",
    numRecycles: "_3",
    userEmail: null,
    // page state
    loading: false,
    instructions: false,
    errorText: "",
};

class Fold extends React.Component<FoldProps, FoldState> {
    constructor(props: FoldProps) {
        super(props);
        this.state = defaultFoldState;
        this.handleUseAmberChange = this.handleUseAmberChange.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleTemplateFileChange = this.handleTemplateFileChange.bind(this);
        this.reset = this.reset.bind(this);
        this.submit = this.submit.bind(this);
    }

    reset() {
        this.setState(defaultFoldState);
    }

    handleSequenceChange(index: number, event: React.ChangeEvent<HTMLTextAreaElement>) {
        event.persist();
        this.setState(({ sequences }: FoldState) => {
            sequences = [...sequences];
            sequences[index] = event.target.value;
            return { sequences };
        });
    }

    handleSequenceAmountChange(isAdd: boolean) {
        this.setState(({ sequences }: FoldState) => {
            sequences = [...sequences];

            if (isAdd) {
                if (sequences.length < 5) {
                    sequences.push("");
                }
            } else if (sequences.length > 1) {
                sequences.pop();
            }

            return { sequences };
        });
    }

    handleUseAmberChange() {
        this.setState(({ useAmber }: FoldState) => ({
            useAmber: !useAmber,
        }));
    }

    handlePairModeChange(event: React.ChangeEvent<HTMLSelectElement>) {
        this.setState({
            pairMode: event.target.value as PairMode,
        });
    }

    handleChange(event: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) {
        event.persist();
        this.setState((state: FoldState) => {
            state = {...state, [event.target.name]: event.target.value};
            if (event.target.name === "templateMode" && event.target.value !== "CUSTOM") {
                state.templateFiles = [];
            }
            return state;
        });
    }

    async handleTemplateFileChange(event: React.ChangeEvent<HTMLInputElement>) {
        const templateFiles: TemplateFile[] = [];
        const files = event.target.files as FileList;

        for (let i = 0; i < files.length; i++) {
            const file = files.item(i);
            if (file === null) {
                console.log(`null file #${i}`);
                continue;
            }
            templateFiles.push({
                name: file.name,
                contents: await file.text(),
            });
        }

        this.setState({ templateFiles });
    }

    submit() {
        const errors = [];

        if (this.state.templateMode === "CUSTOM") {
            if (this.state.templateFiles.length === 0) {
                errors.push("must upload custom template files");
            } else {
                // we only need to perform validation here because the backend fixes up the filenames
                const filenames: string[] = [];

                for (let i = 0; i < this.state.templateFiles.length; i++) {
                    // turn filename into 4 lowercase letters
                    const file = this.state.templateFiles[i];
                    const filenameParts = file.name.split(".");
                    const extension = filenameParts.pop();
                    const name = filenameParts.join(".").substr(0, 4).toLowerCase();
                    const filename = `${name}.${extension}`;

                    if (filenames.includes(filename)) {
                        errors.push("duplicate filename detected: " + filename);
                    } else {
                        filenames.push(filename);
                    }
                }
            }
        }

        if (this.state.sequences.some(sequence => sequence === "")) {
            errors.push("must input sequence");
        }

        if (errors.length > 0) {
            this.setState({ errorText: errors.join("; ") });
            return;
        }

        const formData: FoldForm = {
            sequences: this.state.sequences,
            useAmber: this.state.useAmber,
            templateMode: this.state.templateMode,
            templateFiles: this.state.templateFiles,
            pairMode: this.state.pairMode,
            numRecycles: this.state.numRecycles,
            userEmail: this.state.userEmail || null,
        };
        this.setState({ loading: true });
        console.log(formData);
        axios.post(Urls.computation_servers.cgopm + "fold", formData)
            .then(res => {
                const urlParams = {
                    resultsUrl: res.data.resultsUrl || "",
                    waitingUrl: res.data.waitingUrl || "",
                    message: res.data.message,
                };
                let s = this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === '/' ? "submitted" : "/submitted";
                const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
                this.props.currentUrl.history.push(submitUrl);
                this.setState({ loading: false, errorText: "" });
            })
            .catch(err => {
                let error: string;
                if (err.response) {
                    // Request made and server responded
                    error = "Request made and server responded with failure status code: " + err.response.status.toString();
                }
                else if (err.request) {
                    // Request made but no response received,
                    error = "Request was made to the server but no response was received, this may be because the server is down.";
                }
                else {
                    // Something happened in setting up the request that triggered an error
                    error = "Setting up the request triggered an error: " + err.message;
                }
                console.log("An error occurred: " + JSON.stringify(err))
                this.setState({
                    errorText: error,
                    loading: false
                });
            });
    }

    renderContent() {
        if (this.props.submitted) {
            return (
                <Submitted
                    submitAgain={this.reset}
                    userEmail={this.state.userEmail || undefined}
                />
            );
        }

        return (
            <>
                <div className="fold-introduction">
                    <h1>Fold Subtitle</h1>
                    Fold Introduction
                </div>
                <table className="table-form">
                    <thead>
                        <tr>
                            <td colSpan={2}>
                                ColabFold
                            </td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Sequences</td>
                            <td className="sequences-input">
                                <div>
                                    <input
                                        type="button"
                                        value="+"
                                        onClick={() => this.handleSequenceAmountChange(true /* isAdd */)}
                                    />
                                    <input
                                        type="button"
                                        value="-"
                                        onClick={() => this.handleSequenceAmountChange(false /* !isAdd */)}
                                    />
                                </div>
                                {this.state.sequences.map((sequence, index) => (<>
                                    <textarea
                                        placeholder={`sequence #${index+1}`}
                                        value={sequence}
                                        onChange={(event) => this.handleSequenceChange(index, event)}
                                    />
                                </>))}
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>Use amber</td>
                            <td>
                                <input
                                    type="checkbox"
                                    checked={this.state.useAmber}
                                    onChange={this.handleUseAmberChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>Template mode</td>
                            <td>
                                <select
                                    name="templateMode"
                                    value={this.state.templateMode}
                                    onChange={this.handleChange}
                                >
                                    <option value="NONE">none</option>
                                    <option value="PDB70">pdb70</option>
                                    <option value="CUSTOM">custom</option>
                                </select>
                            </td>
                        </tr>
                        {this.state.templateMode === "CUSTOM"
                            ? (<tr>
                                    <td>Custom template files</td>
                                    <td>
                                        <input type="file" multiple onChange={this.handleTemplateFileChange} />
                                        Filenames must be at most 4 letters long, lowercase, and unique <br />
                                        Example: 7rsy.pdb
                                    </td>
                            </tr>)
                            : undefined
                        }
                        <tr className="dark">
                            <td>Pair mode</td>
                            <td>
                                <select
                                    name="pairMode"
                                    value={this.state.pairMode}
                                    onChange={this.handleChange}
                                >
                                    <option value="UNPAIRED_PAIRED">unpaired+paired</option>
                                    <option value="PAIRED">paired</option>
                                    <option value="UNPAIRED">unpaired</option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td>Number of recycles</td>
                            <td>
                                <select
                                    name="numRecycles"
                                    value={this.state.numRecycles}
                                    onChange={this.handleChange}
                                >
                                    <option value="_1">1</option>
                                    <option value="_3">3</option>
                                    <option value="_6">6</option>
                                    <option value="_12">12</option>
                                    <option value="_24">24</option>
                                    <option value="_48">48</option>
                                </select>
                            </td>
                        </tr>
                        <tr className="dark">
                            <td>
                                Email
                                <br />
                                You will be notified by email when the calculations are complete.
                            </td>
                            <td>
                                <input
                                    type="text"
                                    name="userEmail"
                                    placeholder="email (optional)"
                                    value={this.state.userEmail || undefined}
                                    onChange={this.handleChange}
                                />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Please do not submit too many jobs at the same time.
                                If you would like to run ColabFold yourself, see{" "}
                                <a href="https://github.com/sokrypton/ColabFold" target="_blank">sokrypton/ColabFold</a>.
                            </td>
                            <td>
                                <input
                                    type="button"
                                    value="Reset"
                                    onClick={this.reset}
                                />
                                <input
                                    type="button"
                                    value="Submit"
                                    onClick={this.submit}
                                />
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div className="error">
                    {this.state.errorText}
                </div>
            </>
        );
    }

    render() {
        return (
            <div className="fold">
                <div className="server-header">Fold</div>
                {this.renderContent()}
            </div>
        );
    }
}

export default connector(Fold);
