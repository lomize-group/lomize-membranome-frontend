import Axios from "axios";
import { Properties as CSSProperties } from "csstype";
import * as React from "react";
import scriptLoader from "react-async-script-loader";
import { Assets } from "../../config/Assets";
import { Classification, CTypeCategory } from "../../config/Classification";
import { RecursivePartial } from "../../config/Types";
import "./GLmolViewer.scss";

// Get these objects from lazy loading scripts over CDN
declare let GLmol: any;
declare let THREE: any;

type GLmolViewerComponent<T = undefined> = {
    show: boolean;
    value: T;
};

type GLmolViewerComponents = {
    colorBy: GLmolViewerComponent<string>;
    sideChains: GLmolViewerComponent<boolean>;
    table: boolean;
    footer: boolean;
};

type GLmolViewerProps = {
    pdbid?: string;
    pdbUrl?: string;
    ctype?: CTypeCategory;
    style?: CSSProperties;
    isFmap?: boolean;
    components?: RecursivePartial<GLmolViewerComponents>;
    autoScaleInitialRender?: boolean;
    isScriptLoaded: boolean;
    isScriptLoadSucceed: boolean;
};

type GLMolModel = {
    source: Array<string[] | string>;
    modelNumber: number;
    seq1?: string;
    seq2?: string;
    seq3?: string;
    seq4?: string;
    [key: string]: any;
};

type GLmolViewerState = {
    models: Array<GLMolModel>;
    modelNumber: number;
    loaded: boolean;
    loadSucceeded: boolean;
    isDimer: boolean;
    src: string;
    seq1: string;
    seq2: string;
    seq3: string;
    seq4: string;
    components: GLmolViewerComponents;
};

class GLmolViewer extends React.Component<GLmolViewerProps, GLmolViewerState> {
    static defaultProps = {
        pdbid: "",
        pdbUrl: "",
        ctype: "proteins",
        isFmap: false,
        style: {},
        autoScaleInitialRender: false,
        components: {},
    };

    private glmolController: typeof GLmol;

    constructor(props: GLmolViewerProps) {
        super(props);
        this.state = {
            models: [],
            loaded: false,
            loadSucceeded: false,
            isDimer: false,
            modelNumber: 0,
            src: "",
            seq1: "",
            seq2: "",
            seq3: "",
            seq4: "",
            components: {
                colorBy: {
                    show: props.components!.colorBy?.show ?? true,
                    value: props.components!.colorBy?.value ?? "chain",
                },
                sideChains: {
                    show: props.components!.sideChains?.show ?? true,
                    value: props.components!.sideChains?.value ?? false,
                },
                table: props.components!.table ?? true,
                footer: props.components!.footer ?? true,
            },
        };
        this.glmolController = undefined;
    }

    init = () => {
        // needed for access within defineRepresentation (scope of "this" changes)
        const glmol = this;
        this.glmolController = new GLmol("glmol", true);

        // define the glmol object and its defineRepresentation method.
        // defineRepresentation is used by glmol loadMolecule rendering
        if (this.props.ctype === "complex_structures") {
            // glmol render options for complexes
            this.glmolController.defineRepresentation = function () {
                const { colorBy, sideChains } = glmol.state.components;

                const all = this.getAllAtoms();
                const allHet = this.getHetatms(all);
                const hetatm = this.removeSolvents(allHet);

                this.colorByAtom(all, {});

                // additional coloring
                if (colorBy.value === "chainbow") {
                    this.colorChainbow(all);
                } else if (colorBy.value === "chain") {
                    this.colorByChain(all);
                } else if (colorBy.value === "b") {
                    this.colorByBFactor(all);
                } else if (colorBy.value === "ss") {
                    this.colorByStructure(all, 0xcc00cc, 0x00cccc);
                } else if (colorBy.value === "polarity") {
                    this.colorByPolarity(all, 0xcc0000, 0xcccccc);
                }

                let asu = new THREE.Object3D();
                this.drawAtomsAsSphere(asu, hetatm, this.sphereRadius);
                if (sideChains.value) {
                    this.drawBondsAsStick(
                        asu,
                        this.getSidechains(all),
                        this.cylinderRadius,
                        this.cylinderRadius
                    );
                }
                this.drawCartoon(asu, all, this.curveWidth, this.thickness);
                this.drawSymmetryMates2(this.modelGroup, asu, this.protein.biomtMatrices);
                this.modelGroup.add(asu);
            };
        } else if (this.props.ctype === "proteins") {
            // glmol render options for non-complexes
            this.glmolController.defineRepresentation = function () {
                const { colorBy } = glmol.state.components;

                const all = this.getAllAtoms();
                const allHet = this.getHetatms(all);

                this.colorByAtom(all, {});

                // additional coloring
                if (colorBy.value === "chain") {
                    this.colorByChain(all);
                } else if (colorBy.value === "residue") {
                    this.colorByResidue(this.getSidechains(all), this.AminoAcidResidueColors);
                }

                let asu = new THREE.Object3D();
                this.drawAtomsAsSphere(asu, allHet, this.sphereRadius);
                this.drawBondsAsStick(
                    asu,
                    this.getSidechains(all),
                    this.cylinderRadius,
                    this.cylinderRadius
                );
                this.drawCartoon(asu, all, this.curveWidth, this.thickness);
                this.drawSymmetryMates2(this.modelGroup, asu, this.protein.biomtMatrices);
                this.modelGroup.add(asu);
            };
        }

        const url: string = this.props.pdbUrl
            || Classification
                .definitions[this.props.ctype!]
                .pdb_file(this.props.pdbid, /* glmol */ true);

        Axios.get(url)
            .then((res) => this.parsePdbData(res.data))
            .catch(() => {
                this.setState({
                    loaded: true,
                    loadSucceeded: false,
                });
            });
    };

    /**
     * PDB data may be multi-model, denoted by lines prefixed by "MODEL"
     * If it is multi-model, parse/store each model separately and render the
     * first model by default.
     * Otherwise, the entire PDB file will be used as the src data.
     *
     * @param data {string}: PDB file contents
     */
    parsePdbData = (data: string) => {
        // if a complex does not start with MODEL then it is a single-model
        // so we can just call glmol to render the entire complex PDB file
        if (this.props.ctype === "complex_structures" && !data.startsWith("MODEL")) {
            let setStateCallback: () => void;
            if (this.props.autoScaleInitialRender) {
                const lines = data.split("\n");
                let startCount = false;
                let count = 0;
                for (let i = 0; i < lines.length; ++i) {
                    const line = lines[i];
                    if (startCount) {
                        ++count;
                    }
                    if (line.startsWith("MODEL")) {
                        startCount = true;
                    } else if (line.startsWith("ENDMDL")) {
                        break;
                    }
                }

                if (count === 0) {
                    count = lines.length;
                }

                setStateCallback = () => {
                    this.glmolController.loadMolecule();
                    this.glmolController.rotationGroup.position.z = Math.pow(count, 0.5);
                    this.glmolController.show();
                };
            } else {
                setStateCallback = () => this.glmolController.loadMolecule();
            }

            this.setState(
                {
                    loaded: true,
                    loadSucceeded: true,
                    src: data,
                },
                setStateCallback
            );
            return;
        }

        // data will be parsed line-by-line
        const lines = data.split("\n");
        let curModelNumber = 0;

        // create object for first model
        let models: Array<GLMolModel> = [{ source: [[" "]], modelNumber: 0 }];
        let isDimer = false;
        let shouldPop = false;
        let firstSeq = false;

        // do parsing
        for (let i = 0; i < lines.length; ++i) {
            const line = lines[i];
            models[curModelNumber].source.push(line);

            if (line.startsWith("MODEL")) {
                isDimer = true;
                const modelNumber = lines[i].trim().replace(/^\D+/g, "");
                models[curModelNumber].modelNumber = parseInt(modelNumber);
                shouldPop = false;
            } else if (line.startsWith("REMARK")) {
                const remarks = line.substring(7, line.length);
                let properties = remarks.split(",");
                const hasNames = properties.length > 1;

                // complexes handled differently
                if (this.props.ctype === "complex_structures") {
                    for (let j = 0; j < properties.length; ++j) {
                        if (hasNames) {
                            const attrName = properties[j].split("=")[0].trim();
                            const propVal = properties[j].match("\\-?\\d*\\.\\d?")![0]; //detect floats in the remarks line
                            models[curModelNumber][attrName] = propVal;
                        } else {
                            let seq: "" | "seq1" | "seq2" | "seq3" | "seq4" = "";
                            if (firstSeq == false) {
                                if (properties[0].indexOf(".") !== -1) {
                                    seq = "seq2";
                                } else {
                                    seq = "seq1";
                                    firstSeq = true;
                                }
                            } else {
                                if (properties[0].indexOf(".") !== -1) {
                                    seq = "seq4";
                                } else {
                                    seq = "seq3";
                                    firstSeq = false;
                                }
                            }

                            models[curModelNumber][seq] = properties[0].replace(/\s/g, "");
                        }
                    }
                } else {
                    // not a complex
                    for (let j = 0; j < properties.length; ++j) {
                        if (hasNames) {
                            const attrName = properties[j].split("=")[0].trim();
                            const propVal = properties[j].match("\\-?\\d*\\.\\d?")![0]; //detect floats in the remarks line
                            models[curModelNumber][attrName] = propVal;
                        } else {
                            const seq = properties[0].indexOf(".") === -1 ? "seq1" : "seq2";
                            models[curModelNumber][seq] = properties[0].replace(/\s/g, "");
                        }
                    }
                }
            } else if (line.startsWith("ENDMDL")) {
                curModelNumber += 1;
                models.push({ source: [[" "]], modelNumber: 0 });
                shouldPop = true;
            }
        }

        // the final model will be empty from the loop if there was an ENDMDL for it
        if (shouldPop) {
            models.pop();
        }

        const src = models[0].source.join("\n");

        // sequences shown below the glmol viewer window
        let seq1 = "";
        let seq2 = "";
        let seq3 = "";
        let seq4 = "";

        if (isDimer) {
            if (this.props.ctype === "complex_structures") {
                // magic number 1200 for some reason
                const headArr = src.substring(0, 1200).split("\n");
                seq1 = headArr[4].slice(8, -1);
                seq2 = headArr[5].slice(8, -1);
                seq3 = headArr[6].slice(8, -1);
                seq4 = headArr[7].slice(8, -1);
            } else {
                seq1 = models[0].seq1 || "";
                seq2 = models[0].seq2 || "";
            }
        }

        // Update state/DOM re-render first.
        // Glmol render as the callback because this.state.src must be updated
        // so that the glmol_src textarea gets updated. This data needs to be
        // present for loadMolecule to work.
        this.setState(
            {
                loaded: true,
                loadSucceeded: true,
                models,
                isDimer,
                src,
                seq1,
                seq2,
                seq3,
                seq4,
            },
            () => this.glmolController.loadMolecule()
        );
    };

    // for multi-model PDB files - handle user change by clicking on a model
    changeModel = (newModelNum: number) => {
        // keep within bounds of available models
        if (newModelNum >= this.state.models.length) {
            newModelNum = this.state.models.length - 1;
        }
        if (newModelNum < 0) {
            newModelNum = 0;
        }

        // don't need to update if the model did not change
        if (newModelNum === this.state.modelNumber) {
            return;
        }

        let { seq1, seq2 } = this.state;
        if (this.props.ctype !== "complex_structures") {
            seq1 = this.state.models[newModelNum].seq1 || "";
            seq2 = this.state.models[newModelNum].seq2 || "";
        }

        // update current model and source
        this.setState(
            {
                modelNumber: newModelNum,
                src: this.state.models[newModelNum].source.join("\n"),
                seq1,
                seq2,
            },
            () => this.glmolController.loadMolecule()
        );
    };

    // handle change of user input options; update state and glmol re-render
    handleColorChange = (event: React.SyntheticEvent<HTMLSelectElement>) => {
        let components = { ...this.state.components };
        components.colorBy.value = (event.target as HTMLSelectElement).value;
        this.setState({ components }, () => this.glmolController.loadMolecule());
    };

    handleSidechainsChange = () => {
        let components = { ...this.state.components };
        components.sideChains.value = !components.sideChains.value;
        this.setState({ components }, () => this.glmolController.loadMolecule());
    };

    componentWillReceiveProps({ isScriptLoaded, isScriptLoadSucceed }: GLmolViewerProps) {
        if (isScriptLoaded && !this.props.isScriptLoaded) {
            // load finished
            if (isScriptLoadSucceed) {
                this.init();
            }
        }
    }

    componentDidMount() {
        const { isScriptLoaded, isScriptLoadSucceed } = this.props;
        if (isScriptLoaded && isScriptLoadSucceed) {
            this.init();
        }
    }

    renderControls() {
        let items = [];
        if (this.state.components.colorBy.show) {
            items.push(
                <span key={items.length}>
                    Color by{" "}
                    <select
                        id="glmol_color"
                        value={this.state.components.colorBy.value}
                        onChange={this.handleColorChange}
                    >
                        <option value="chain">chain</option>
                        <option value="chainbow">spectrum</option>
                        <option value="ss">secondary strcuture</option>
                        <option value="b">B factor</option>
                        <option value="polarity">polar/nonpolar</option>
                    </select>
                </span>
            );
        }
        if (this.state.components.sideChains.show) {
            items.push(
                <span key={items.length}>
                    {" "}
                    Sidechains{" "}
                    <input
                        id="show-sidechains"
                        type="checkbox"
                        value={this.state.components.sideChains.value ? 1 : 0}
                        onChange={this.handleSidechainsChange}
                    />
                </span>
            );
        }

        return (
            <div
                id="view-controller"
                className={this.props.ctype === "complex_structures" ? "" : "hidden"}
            >
                {items}
            </div>
        );
    }

    renderRemarksTable() {
        if (!this.state.isDimer) {
            return null;
        }

        if (this.props.isFmap) {
            return (
                <div>
                    <table className="remarks-table">
                        <thead>
                            <tr>
                                <td>
                                    <b>Selection of alpha-helices</b>
                                </td>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                this.state.models.map((model, i) => 
                                    <tr
                                        className={"glmol-tr" + (this.state.modelNumber === i ? " selected" : "")}
                                        key={i}
                                        onClick={() => {
                                            this.changeModel(i);
                                        }}
                                    >
                                        <td>Helix {model.modelNumber}</td>
                                    </tr>
                                )
                            }
                        </tbody>
                    </table>
                </div>
            );
        }

        let rows = [];
        for (let i = 0; i < this.state.models.length; ++i) {
            const model = this.state.models[i];
            rows.push(
                <tr
                    className={"glmol-tr" + (this.state.modelNumber === i ? " selected" : "")}
                    key={i}
                    onClick={() => {
                        this.changeModel(i);
                    }}
                >
                    <td>{model.modelNumber}</td>
                    <td>{model.DGasc}</td>
                    <td>{model.DGstb}</td>
                    <td>{model.Easc}</td>
                    <td>{model.Rasym}</td>
                    <td>{model["Interhelix distance"]}</td>
                    <td>{model.Angle}</td>
                </tr>
            );
        }

        return (
            <div>
                <table className="remarks-table">
                    <thead>
                        <tr>
                            <td>
                                <b>Model</b>
                            </td>
                            <td>
                                <b>
                                    &Delta;G<sub>asc</sub>
                                </b>
                            </td>
                            <td>
                                <b>
                                    &Delta;G<sub>stb</sub>
                                </b>
                            </td>
                            <td>
                                <b>
                                    E<sub>asc</sub>
                                </b>
                            </td>
                            <td>
                                <b>
                                    R<sub>asym</sub>
                                </b>
                            </td>
                            <td>
                                <b>Interhelix Distance</b>
                            </td>
                            <td>
                                <b>Angle</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b></b>
                            </td>
                            <td>kcal/mol</td>
                            <td>kcal/mol</td>
                            <td>kcal/mol</td>
                            <td>Å</td>
                            <td>Å</td>
                            <td>Degrees</td>
                        </tr>
                    </thead>
                    <tbody>{rows}</tbody>
                </table>
            </div>
        );
    }

    render() {
        if (this.state.loaded && !this.state.loadSucceeded) {
            return null;
        }

        return (
            <div className="glmol">
                <div className="molcontainer">
                    <div className="molheader">
                        <b>GLMol Visualization</b>
                    </div>
                    {!this.state.loaded && <img src={Assets.images.loading} alt="Loading" />}
                    <div
                        id="glmol"
                        style={this.state.loaded ? this.props.style : { display: "none" }}
                    />
                    {this.state.loaded && this.state.components.footer && (
                        <div className="molfooter">
                            <p id="model_number"></p>
                            <p id="sequence_1">{this.state.seq1}</p>
                            <p id="sequence_2">{this.state.seq2}</p>
                            <p
                                id="sequence_3"
                                className={
                                    this.props.ctype === "complex_structures" ? "" : "hidden"
                                }
                            >
                                {this.state.seq3}
                            </p>
                            <p
                                id="sequence_4"
                                className={
                                    this.props.ctype === "complex_structures" ? "" : "hidden"
                                }
                            >
                                {this.state.seq4}
                            </p>
                        </div>
                    )}
                    {this.state.loaded && this.renderControls()}
                </div>
                <textarea id="glmol_src" className="hidden" value={this.state.src} readOnly />
                {this.state.loaded && this.state.components.table && this.renderRemarksTable()}
            </div>
        );
    }
}

export default scriptLoader<GLmolViewerProps>(
    "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js",
    "https://storage.googleapis.com/membranome-assets/server_assets/Three49custom.js",
    "https://storage.googleapis.com/membranome-assets/server_assets/GLmol.js"
)(GLmolViewer);
