import { Properties as CSSProperties } from "csstype";
import * as cytoscape from "cytoscape";
import * as React from "react";
import { useEventListener } from "../../hooks";
import "./Graph.scss";

type GraphProps = {
    // container is automatically handled by Graph component
    cytoscapeOptions: Omit<cytoscape.CytoscapeOptions, "container">;
    title?: string;
    className?: string;
    targetStyle?: CSSProperties;
};

/**
 * Graph component.
 * Uses cytoscape graph theory library to render a graph as a react component.
 * This component will manage creating the container element for the graph,
 * rendering the graph in the container, and handling resizing of the graph
 * when necessary.
 */
const Graph: React.FC<GraphProps> = (props) => {
    // ref to div that the graph will be drawn in
    let containerRef = React.useRef<HTMLDivElement>(null);

    // mutable ref to the cytoscape object
    let cy = React.useRef<cytoscape.Core | null>(null);

    React.useEffect(() => {
        // create cytoscape graph in the container after the container element is rendered
        if (containerRef.current !== null) {
            cy.current = cytoscape({
                ...props.cytoscapeOptions,
                // all options should be passed by props besides container element
                container: containerRef.current,
            });
        }
    }, [containerRef.current]);

    useEventListener(
        "resize",
        () => {
            // resize() and fit() to re-render graph when the window is resized.
            if (cy.current) {
                cy.current.resize();
                cy.current.fit();
            }
        },
        [cy.current]
    );

    return (
        <div className="graph-component">
            {props.title && (
                <div className="graph-title">
                    <b>{props.title}</b>
                </div>
            )}
            <div
                ref={containerRef}
                className={props.className || "graph-target"}
                style={props.targetStyle}
            />
        </div>
    );
};

export default Graph;
