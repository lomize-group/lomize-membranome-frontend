import * as React from "react";
import { Link } from "react-router-dom";
import { useStore } from "../../hooks";
import "./Header.scss";

type NavbarInfo = {
    name: string;
    route: string;
    prefixMatch?: boolean;
};

const navbarItems: ReadonlyArray<NavbarInfo> = [
    {
        name: "HOME",
        route: "/",
    },
    {
        name: "ABOUT Membranome",
        route: "/about",
    },
    {
        name: "SEARCH",
        route: "/advanced_search",
    },
    {
        name: "1TMnet",
        route: "/1tmnet",
        prefixMatch: true,
    },
    {
        name: "FMAP SERVER",
        route: "/fmap_cgopm",
    },
    {
        name: "TMDOCK SERVER",
        route: "/tmdock",
    },
    {
        name: "DOWNLOAD FILES",
        route: "/download",
    },
    {
        name: "CONTACT US",
        route: "/contact",
    },
] as const;

function renderNavItem(key: number, name: string, route: string, selected: boolean): JSX.Element {
    if (selected) {
        return (
            <div key={key} className="selected-item">
                <strong>{name}</strong>
            </div>
        );
    }

    return (
        <div key={key} className="navbar-item">
            <Link to={route}>
                <strong>{name}</strong>
            </Link>
        </div>
    );
}

const Header: React.FC = () => {
    const currentUrl = useStore((state) => state.currentUrl);
    let items: Array<React.ReactNode> = [];
    for (let i = 0; i < navbarItems.length; i++) {
        const navItem = navbarItems[i];
        const curRoute = navItem.prefixMatch ? currentUrl.urlPrefix : currentUrl.url;
        items.push(renderNavItem(i, navItem.name, navItem.route, curRoute === navItem.route));
    }

    return (
        <div className="header">
            <img className="header-img" src="/images/membranome.gif" alt="" />
            <div className="navbar">{items}</div>
        </div>
    );
};

export default Header;
