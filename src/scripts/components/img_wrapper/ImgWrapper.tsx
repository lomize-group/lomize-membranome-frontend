import * as React from "react";

import { Assets } from "../../config/Assets";

import "./ImgWrapper.scss";

type OptionalImgWrapperProps = {
    className: string;
};

export type ImgWrapperProps = OptionalImgWrapperProps & {
    src: string;
    fallback?: string;
};

type ImgWrapperState = {
    loaded: boolean;
    found: boolean;
};

export default class ImgWrapper extends React.Component<ImgWrapperProps, ImgWrapperState> {
    static defaultProps: OptionalImgWrapperProps = {
        className: "",
    };

    state: ImgWrapperState = {
        loaded: false,
        found: true,
    };

    handleLoad = (): void => {
        this.setState({ loaded: true });
    };

    handleError = (e: React.ChangeEvent<HTMLImageElement>): void => {
        e.target.src = this.props.fallback ?? Assets.images.no_image;
    };

    componentWillReceiveProps(nextProps: ImgWrapperProps): void {
        if (this.props.src !== nextProps.src) {
            this.setState({
                loaded: false,
                found: true,
            });
        }
    }

    render(): JSX.Element {
        return (
            <div className="image-wrapper">
                <img
                    className={this.state.loaded ? this.props.className : "hidden"}
                    src={this.props.src}
                    onLoad={this.handleLoad}
                    onError={this.handleError}
                />
                <img
                    className={this.state.loaded ? "hidden" : this.props.className}
                    src={Assets.images.loading}
                />
            </div>
        );
    }
}
