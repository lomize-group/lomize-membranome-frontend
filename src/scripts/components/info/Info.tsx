import * as React from "react";
import { Classification, CType, CTypeCategory } from "../../config/Classification";
import { Urls } from "../../config/Urls";
import { capitalize } from "../../config/Util";
import ListItem from "../list/list_item/ListItem";
import "./Info.scss";

export type ClassificationInfo = {
    id: number;
    name: string;
    subclasses: number;
    pfam?: string;
    interpro?: string;
};

export type InfoProps = {
    protein_types?: Array<ClassificationInfo>;
    protein_classes?: Array<ClassificationInfo>;
    protein_superfamilies?: Array<ClassificationInfo>;
    protein_families?: Array<ClassificationInfo>;
    species?: Array<ClassificationInfo>;
    protein_localizations?: Array<ClassificationInfo>;
    pathway_classes?: Array<ClassificationInfo>;
    pathway_subclasses?: Array<ClassificationInfo>;
    complex_classes?: Array<ClassificationInfo>;
    complex_superfamilies?: Array<ClassificationInfo>;
    complex_families?: Array<ClassificationInfo>;
    complex_localizations?: Array<ClassificationInfo>;
    base?: string;
};

function getTitle(data: ReadonlyArray<ClassificationInfo>, ctype: CType) {
    const def = Classification.definitions[ctype];
    return capitalize(data.length > 1 ? def.name : def.name_singular);
}

export default class Info extends React.Component<InfoProps> {
    createListItems = (
        data: Array<ClassificationInfo>,
        ctype: CType,
        expandable: boolean,
        category: CTypeCategory = "proteins",
        showSubclasses = true
    ): React.ReactNodeArray => {
        let listItems: React.ReactNodeArray = [];
        for (let i = 0; i < data.length; ++i) {
            listItems.push(
                <span key={i}>
                    {i > 0 && ", "}
                    <ListItem
                        ctype={ctype}
                        id={data[i].id}
                        name={data[i].name}
                        subclasses={showSubclasses ? data[i].subclasses : undefined}
                        base={this.props.base === ctype}
                        pfam={data[i].pfam}
                        interpro={data[i].interpro}
                        expandable={expandable}
                        expandUrl={
                            expandable
                                ? Urls.baseUrl +
                                  Classification.definitions[ctype].api.route +
                                  "/" +
                                  data[i].id +
                                  Classification.definitions[category].api.route
                                : undefined
                        }
                    />
                </span>
            );
        }
        return listItems;
    };

    renderInfo(): React.ReactNode {
        let items = [];

        if (this.props.protein_types !== undefined && this.props.protein_types.length > 0) {
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Type:</i>{" "}
                    {this.createListItems(this.props.protein_types, "protein_types", false)}
                </li>
            );
        }

        if (this.props.protein_classes !== undefined && this.props.protein_classes.length > 0) {
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Class:</i>{" "}
                    {this.createListItems(this.props.protein_classes, "protein_classes", false)}
                </li>
            );
        }

        if (
            this.props.protein_superfamilies !== undefined &&
            this.props.protein_superfamilies.length > 0
        ) {
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Superfamily:</i>{" "}
                    {this.createListItems(
                        this.props.protein_superfamilies,
                        "protein_superfamilies",
                        false
                    )}
                </li>
            );
        }

        if (this.props.protein_families !== undefined && this.props.protein_families.length > 0) {
            const expandable: boolean = this.props.base === "protein_families";
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Family:</i>{" "}
                    {this.createListItems(
                        this.props.protein_families,
                        "protein_families",
                        expandable,
                        "proteins"
                    )}
                </li>
            );
        }

        if (this.props.species !== undefined && this.props.species.length > 0) {
            const expandable: boolean = this.props.base === "species";
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Species:</i>{" "}
                    {this.createListItems(this.props.species, "species", expandable, "proteins")}
                </li>
            );
        }

        if (
            this.props.protein_localizations !== undefined &&
            this.props.protein_localizations.length > 0
        ) {
            const expandable: boolean = this.props.base === "protein_localizations";
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Localization:</i>{" "}
                    {this.createListItems(
                        this.props.protein_localizations,
                        "protein_localizations",
                        expandable,
                        "proteins"
                    )}
                </li>
            );
        }

        if (this.props.complex_classes !== undefined && this.props.complex_classes.length > 0) {
            const ctype = "complex_classes";
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">{getTitle(this.props.complex_classes, ctype)}:</i>{" "}
                    {this.createListItems(
                        this.props.complex_classes,
                        ctype,
                        false,
                        "complex_structures",
                        false
                    )}
                </li>
            );
        }

        if (
            this.props.complex_superfamilies !== undefined &&
            this.props.complex_superfamilies.length > 0
        ) {
            const ctype = "complex_superfamilies";
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">
                        {getTitle(this.props.complex_superfamilies, ctype)}:
                    </i>{" "}
                    {this.createListItems(
                        this.props.complex_superfamilies,
                        ctype,
                        false,
                        "complex_structures",
                        false
                    )}
                </li>
            );
        }

        if (this.props.complex_families !== undefined && this.props.complex_families.length > 0) {
            const ctype = "complex_families";
            const expandable: boolean = this.props.base === ctype;
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">{getTitle(this.props.complex_families, ctype)}:</i>{" "}
                    {this.createListItems(
                        this.props.complex_families,
                        ctype,
                        expandable,
                        "complex_structures"
                    )}
                </li>
            );
        }

        if (
            this.props.complex_localizations !== undefined &&
            this.props.complex_localizations.length > 0
        ) {
            const ctype = "complex_localizations";
            const expandable: boolean = this.props.base === ctype;
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">
                        {getTitle(this.props.complex_localizations, ctype)}:
                    </i>{" "}
                    {this.createListItems(
                        this.props.complex_localizations,
                        ctype,
                        expandable,
                        "complex_structures"
                    )}
                </li>
            );
        }

        if (this.props.pathway_classes !== undefined && this.props.pathway_classes.length > 0) {
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Class:</i>{" "}
                    {this.createListItems(this.props.pathway_classes, "pathway_classes", false)}
                </li>
            );
        }

        if (
            this.props.pathway_subclasses !== undefined &&
            this.props.pathway_subclasses.length > 0
        ) {
            items.push(
                <li key={items.length} className="info-item">
                    <i className="info-name">Subclass:</i>{" "}
                    {this.createListItems(
                        this.props.pathway_subclasses,
                        "pathway_subclasses",
                        false
                    )}
                </li>
            );
        }

        return items;
    }

    render(): React.ReactNode {
        return (
            <div className="info">
                <ul className="info-data">{this.renderInfo()}</ul>
            </div>
        );
    }
}
