import * as appendQuery from "append-query";
import axios, { AxiosResponse } from "axios";
import * as React from "react";
import { Assets } from "../../config/Assets";
import { Classification, CType } from "../../config/Classification";
import { ApiPagedResponse } from "../../config/Types";
import { Urls } from "../../config/Urls";
import Search from "../search/Search";
import "./List.scss";
import ListItem, { ListItemProps } from "./list_item/ListItem";

// List usage example:
//	<List
//		url={'http://localhost:3000/superfamilies'}
//		type='superfamilies'
//		sub_type='families'
//	/>

export type ListProps = {
    expandByDefault?: boolean;
    searchable?: boolean;
    expandableItems?: boolean;
    data?: ApiPagedResponse;
    onResize?: () => void;
    renderListItem?: (item: ListItemProps) => JSX.Element;
    url: string;
    ctype: CType;
};

type ListState = {
    expandDisabled: boolean;
    data: ApiPagedResponse;
    listData: Array<ListItemProps>;
    search: string;
    allShown: boolean;
    loading: boolean;
};

function getInitialState(): ListState {
    return {
        expandDisabled: true,
        data: {
            page_size: 50,
            page_num: 0,
            total_objects: 0,
            objects: [],
            sort: "",
            direction: "",
        },
        listData: [],
        search: "",
        allShown: false,
        loading: true,
    };
}

export default class List extends React.Component<ListProps, ListState> {
    static defaultProps: Partial<ListProps> = {
        expandByDefault: false,
        searchable: true,
        expandableItems: false,
        onResize: undefined,
    };

    private listItemRefs: Array<any>;

    constructor(props: ListProps) {
        super(props);
        this.state = getInitialState();

        this.listItemRefs = [];

        if (props.data === undefined) {
            this.fetchData();
        } else {
            const listData = this.generateEntries(props.data.objects);
            this.state = {
                ...this.state,
                data: props.data,
                listData,
                expandDisabled: listData.length === props.data.total_objects,
                loading: false,
            };
        }
    }

    private fetchData = async () => {
        const params = {
            pageSize: this.state.data.page_size,
        };
        const fetchUrl = appendQuery(this.props.url, params);
        const { data } = await axios.get(fetchUrl);
        const newListData = this.generateEntries(data.objects);

        this.setState(
            {
                data,
                listData: newListData,
                expandDisabled: newListData.length === data.total_objects,
                loading: false,
            },
            () => {
                if (this.props.expandByDefault) {
                    this.handleExpansion();
                }
            }
        );
        this.handleExpansion();
    };

    private handleExpansion = (): void => {
        // Load next page of data
        let params: any = {
            pageSize: this.state.data.total_objects,
        };
        if (this.state.search !== "") {
            params.search = this.state.search;
        }

        const new_url = appendQuery(this.props.url, params);
        axios.get(new_url).then((response: AxiosResponse<ApiPagedResponse>) => {
            let new_list_data = this.generateEntries(
                response.data.objects.slice(
                    this.state.listData.length,
                    response.data.objects.length
                )
            );
            const curr_list_data = this.state.listData.slice().concat(new_list_data);

            // Render the new data
            this.setState({
                listData: curr_list_data,
                expandDisabled: true,
                data: response.data,
                loading: false,
            });
        });

        this.setState({
            expandDisabled: true,
            loading: true,
        });
    };

    private expandCollapseAll = (expand: boolean) => {
        for (let i = 0; i < this.listItemRefs.length; ++i) {
            this.listItemRefs[i].setExpand(expand);
        }
        this.setState({ allShown: expand });
    };

    private handleSearch = (value: string) => {
        this.setState({
            expandDisabled: true,
            loading: true,
            listData: [],
        });

        this.listItemRefs = [];

        let params: any = {
            pageSize: this.state.data.page_size,
        };
        if (value !== "") {
            params.search = value;
        }

        const fetchUrl = appendQuery(this.props.url, params);
        axios.get(fetchUrl).then((response: AxiosResponse<ApiPagedResponse>) => {
            let new_list_data = this.generateEntries(response.data.objects);
            let disable = new_list_data.length === response.data.total_objects;
            this.setState({
                listData: new_list_data,
                search: value,
                expandDisabled: disable,
                data: response.data,
                loading: false,
            });
        });
    };

    private generateEntries = (objects: Array<any>): Array<ListItemProps> => {
        let listData = [];
        for (let i = 0; i < objects.length; i++) {
            listData.push(this.createListData(objects[i]));
        }
        return listData;
    };

    private createListData = (obj: any): ListItemProps => {
        const type_config = Classification.definitions[this.props.ctype];
        const child_config = Classification.definitions[type_config.child!];
        return {
            orderingCache: obj.ordering_cache,
            expandOnOpen: this.state.allShown,
            ctype: this.props.ctype,
            name: obj.name,
            id: obj.id,
            subclasses: obj[child_config.api.accessor.count],
            pfam: obj.pfam,
            interpro: obj.interpro,
            expandable: this.props.expandableItems ?? false,
            expandUrl: Urls.baseUrl + type_config.api.route + "/" + obj.id + child_config.api.route,
            onResize: this.props.onResize,
        };
    };

    componentDidMount(): void {
        if (this.props.onResize !== undefined) {
            this.props.onResize();
        }
    }

    componentDidUpdate(prevProps: ListProps): void {
        if (this.props.onResize !== undefined) {
            this.props.onResize();
        }
        if (this.props.url !== prevProps.url || this.props.ctype !== prevProps.ctype) {
            this.setState(getInitialState());
            this.fetchData();
        }
    }

    renderListItems() {
        let entries = [];

        for (let i = 0; i < this.state.listData.length; ++i) {
            const { orderingCache } = this.state.listData[i];
            const className = orderingCache ? "list-link" : "numbered-list-item";
            const item = this.props.renderListItem ? (
                this.props.renderListItem(this.state.listData[i])
            ) : (
                <ListItem
                    ref={(ref: any) => {
                        if (this.listItemRefs.length <= i && ref !== null) {
                            this.listItemRefs.push(ref);
                        }
                    }}
                    {...this.state.listData[i]}
                />
            );
            entries.push(
                <li key={i} className={className}>
                    {orderingCache && <span className="ordering">{orderingCache}.</span>}
                    {item}
                </li>
            );
        }

        return entries;
    }

    render() {
        return (
            <div className="list">
                {this.props.expandableItems && (
                    <div>
                        <span className="expand-item" onClick={() => this.expandCollapseAll(true)}>
                            show all
                        </span>{" "}
                        <span className="expand-item" onClick={() => this.expandCollapseAll(false)}>
                            hide all
                        </span>
                    </div>
                )}
                <div className={this.props.searchable ? "search-section" : "hidden"}>
                    <Search onSearch={this.handleSearch} placeholder="Search list..." />
                </div>
                <ol className="list-data">{this.renderListItems()}</ol>
                <div
                    className={this.state.expandDisabled ? "hidden" : "expand"}
                    onClick={this.handleExpansion}
                >
                    See All...
                </div>
                <div className={this.state.loading ? "info-loading" : "hidden"}>
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            </div>
        );
    }
}
