import * as appendQuery from "append-query";
import axios from "axios";
import * as React from "react";
import { Link } from "react-router-dom";
import { Assets } from "../../../config/Assets";
import { Classification, CType, Definition } from "../../../config/Classification";
import List from "../List";
import ProteinList from "../protein/ProteinList";
import "./ListItem.scss";

export type ListItemProps = {
    ctype: CType;
    name: string;
    id: number;
    subclasses?: number;
    tcdb?: string;
    orderingCache?: string;
    pfam?: string;
    interpro?: string;
    base?: boolean;
    expandable: boolean;
    expandUrl?: string;
    expandOnOpen?: boolean;
    onResize?: () => void;
};

type ListItemState = {
    expanded: boolean;
    loading: boolean;
    listData?: any;
};

export default class ListItem extends React.Component<ListItemProps, ListItemState> {
    static defaultProps: Partial<ListItemProps> = {
        subclasses: undefined,
        tcdb: undefined,
        pfam: undefined,
        interpro: undefined,
        base: false,
        expandable: false,
        expandUrl: undefined,
        expandOnOpen: undefined,
    };

    state = {
        expanded: false,
        loading: false,
        listData: undefined,
    };

    setExpand = (should_expand: boolean): void => {
        if ((should_expand && !this.state.expanded) || (!should_expand && this.state.expanded)) {
            this.expand();
        }
    };

    expand = (): void => {
        if (!this.state.expanded && this.props.expandUrl !== undefined) {
            let params = {
                pageSize: "20",
            };
            let new_url = appendQuery(this.props.expandUrl, params);
            axios.get(new_url).then((response) => {
                this.setState({
                    listData: response.data,
                    loading: false,
                });
            });
        }
        this.setState({
            expanded: !this.state.expanded,
            loading: !this.state.expanded,
        });
    };

    componentWillReceiveProps(nextProps: ListItemProps): void {
        if (this.props.ctype !== nextProps.ctype || this.props.id !== nextProps.id) {
            this.setState({
                expanded: false,
            });
        }
    }

    componentDidMount(): void {
        if (this.props.expandOnOpen) {
            this.expand();
        }
        if (this.props.onResize !== undefined) {
            this.props.onResize();
        }
    }

    componentDidUpdate(): void {
        if (this.props.onResize !== undefined) {
            this.props.onResize();
        }
    }

    renderItem(): React.ReactNode {
        let parts: React.ReactNodeArray = [];
        const type_config: Definition = Classification.definitions[this.props.ctype];
        const child_config: Definition = Classification.definitions[type_config.child!];

        // name link
        if (this.props.base) {
            parts.push(<strong key={parts.length}>{this.props.name}</strong>);
        } else if (this.props.subclasses === 0) {
            parts.push(<span key={parts.length}>{this.props.name}</span>);
        } else {
            parts.push(
                <Link
                    key={parts.length}
                    to={"/" + this.props.ctype + "/" + this.props.id}
                    className="list-item-link"
                >
                    {this.props.name}
                </Link>
            );
        }

        if (this.props.subclasses !== undefined) {
            if (this.props.base) {
                if (this.props.expandable && !this.state.loading) {
                    parts.push(
                        <strong key={parts.length}>
                            {" "}
                            ({this.props.subclasses} {child_config.name}{" "}
                            <span className="expand-item" onClick={this.expand}>
                                {this.state.expanded ? "hide" : "show"}
                            </span>{" "}
                            )
                        </strong>
                    );
                } else if (this.props.expandable && this.state.loading) {
                    parts.push(
                        <strong key={parts.length}>
                            {" "}
                            ({this.props.subclasses} {child_config.name}{" "}
                            <span className="small-loader">
                                <img src={Assets.images.loading_small} />
                            </span>
                            )
                        </strong>
                    );
                } else {
                    parts.push(
                        <strong key={parts.length}>
                            {" "}
                            ({this.props.subclasses} {child_config.name})
                        </strong>
                    );
                }
            } else {
                if (this.props.expandable && !this.state.loading) {
                    parts.push(
                        <span key={parts.length}>
                            {" "}
                            ({this.props.subclasses} {child_config.name}{" "}
                            <span className="expand-item" onClick={this.expand}>
                                {this.state.expanded ? "hide" : "show"}
                            </span>{" "}
                            )
                        </span>
                    );
                } else if (this.props.expandable && this.state.loading) {
                    parts.push(
                        <span key={parts.length}>
                            {" "}
                            ({this.props.subclasses} {child_config.name}{" "}
                            <span className="small-loader">
                                <img src={Assets.images.loading_small} />
                            </span>
                            )
                        </span>
                    );
                } else {
                    parts.push(
                        <span key={parts.length}>
                            {" "}
                            ({this.props.subclasses} {child_config.name})
                        </span>
                    );
                }
            }
        }

        if (type_config.hasOwnProperty("pfam") && this.props.pfam) {
            parts.push(
                <span key={parts.length}>
                    {" "}
                    <a
                        className="external"
                        target="_blank"
                        href={type_config.pfam(this.props.pfam)}
                    >
                        {this.props.pfam}
                    </a>
                </span>
            );
        }

        if (type_config.hasOwnProperty("interpro") && this.props.interpro) {
            parts.push(
                <span key={parts.length}>
                    {" "}
                    <a
                        className="external"
                        target="_blank"
                        href={type_config.interpro(this.props.interpro)}
                    >
                        {this.props.interpro}
                    </a>
                </span>
            );
        }

        if (
            this.state.expanded &&
            this.props.expandUrl &&
            !this.state.loading &&
            type_config.child !== "proteins" &&
            type_config.child !== "complex_structures"
        ) {
            parts.push(
                <div key={parts.length} className="expanded-list-section">
                    <List
                        url={this.props.expandUrl}
                        ctype={type_config.child!}
                        searchable={false}
                        expandableItems={true}
                        data={this.state.listData}
                        onResize={this.props.onResize}
                    />
                </div>
            );
        } else if (
            this.state.expanded &&
            !this.state.loading &&
            this.props.expandUrl !== undefined
        ) {
            parts.push(
                <div key={parts.length}>
                    <ProteinList
                        url={this.props.expandUrl}
                        data={this.state.listData}
                        onResize={this.props.onResize}
                        sorted={this.props.ctype === "protein_families"}
                        complex={type_config.child === "complex_structures"}
                    />
                </div>
            );
        }

        return parts;
    }

    render(): JSX.Element {
        return <span className="list-item">{this.renderItem()}</span>;
    }
}
