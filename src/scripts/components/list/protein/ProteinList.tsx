import * as appendQuery from "append-query";
import axios from "axios";
import * as React from "react";
import { Link } from "react-router-dom";
import { Assets } from "../../../config/Assets";
import { Classification } from "../../../config/Classification";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./ProteinList.scss";

type OptionalProteinListProps = {
    onResize?: () => void;
    sorted: boolean;
    complex: boolean;
};

type Protein = {
    id: number;
    name: string;
    species_id: number;
    species_name_cache: string;
    pdbid: string;
    refpdbcode?: string; // provided if props.complex
};

export type ProteinListProps = OptionalProteinListProps & {
    url?: string; // provided if data is undefined
    data?: {
        objects: Array<Protein>;
        total_objects?: number; // default to objects.length
    };
};

interface ProteinListState {
    entries: Array<any>;
    totalProteins: number;
    totalObjects: number;
    url: string | undefined;
    pageSize: number;
    pageNum: number;
    expandDisabled: boolean;
    loading: boolean;
}

export default class ProteinList extends React.Component<ProteinListProps, ProteinListState> {
    static defaultProps: OptionalProteinListProps = {
        onResize: undefined,
        sorted: true,
        complex: false,
    };

    constructor(props: ProteinListProps) {
        super(props);
        this.state = {
            entries: [],
            totalProteins: 0,
            totalObjects: 0,
            url: props.url,
            pageSize: 20,
            pageNum: 1,
            expandDisabled: true,
            loading: true,
        };

        if (props.data === undefined) {
            this.fetchData();
        } else {
            const totalProteins = props.data.objects.length;
            const totalObjects = props.data.total_objects !== undefined ? props.data.total_objects : totalProteins;
            this.state = {
                entries: this.generateEntries(props.data.objects),
                totalProteins,
                totalObjects,
                url: props.url,
                pageSize: 20,
                pageNum: 1,
                expandDisabled: totalProteins === totalObjects,
                loading: false,
            };
        }
    }

    handleExpand = (): void => {
        this.setState({
            loading: true,
            expandDisabled: true,
        });
        this.fetchData(true);
    };

    generateEntries = (objects: Array<Protein>): Array<any> => {
        let new_entries = this.state.entries;
        for (let i = 0; i < objects.length; i++) {
            const object = objects[i];
            const name = new_entries.length === 0 ? "" : new_entries[new_entries.length - 1].name;

            if (!this.props.complex && this.props.sorted && object.name === name) {
                new_entries[new_entries.length - 1].proteins.push({
                    id: object.id,
                    species: {
                        id: object.species_id,
                        name: object.species_name_cache,
                    },
                    pdbid: object.pdbid,
                });
            } else {
                new_entries.push({
                    name: object.name,
                    proteins: [
                        {
                            id: object.id,
                            species: {
                                id: object.species_id,
                                name: object.species_name_cache,
                            },
                            pdbid: this.props.complex ? object.refpdbcode : object.pdbid,
                        },
                    ],
                });
            }
        }
        return new_entries;
    };

    private async fetchData(expanding: boolean = false): Promise<void> {
        let params: any = {
            pageNum: this.state.pageNum,
            pageSize: expanding ? this.state.totalObjects : this.state.pageSize,
        };
        if (this.props.sorted) {
            params.sort = "name";
        }

        let url = this.state.url;
        if (typeof url !== "string") throw new Error("url not provided");
        const { data } = await axios.get(appendQuery(url, params));

        let new_entries = this.generateEntries(
            data.objects.slice(this.state.totalProteins, data.objects.length)
        );
        let total_proteins = this.state.totalProteins + data.objects.length;
        let disable = expanding || total_proteins === data.total_objects;

        this.setState({
            entries: new_entries,
            expandDisabled: disable,
            totalProteins: total_proteins,
            pageNum: this.state.pageNum + 1,
            loading: false,
            totalObjects: data.total_objects,
        });
    }

    renderListItem(proteins: Array<any>): React.ReactNode {
        let items = [];
        let type = this.props.complex ? "complex_structures" : "proteins";

        for (let i = 0; i < proteins.length; i++) {
            if (!this.props.complex) {
                items.push(
                    <div key={i} className="protein-section">
                        <div className="image-section">
                            <Link to={"/" + type + "/" + proteins[i].id}>
                                <ImgWrapper
                                    className="pdb-image"
                                    src={Classification.definitions.proteins.pdb_image(
                                        proteins[i].pdbid
                                    )}
                                />
                            </Link>
                        </div>
                        <div className="protein-text">
                            <Link to={"/species/" + proteins[i].species.id}>
                                <i className="species-header">{proteins[i].species.name}</i>
                            </Link>
                            <div className="pdbids">
                                <Link to={"/" + type + "/" + proteins[i].id}>
                                    {proteins[i].pdbid}
                                </Link>
                            </div>
                        </div>
                    </div>
                );
            } else if (proteins[i].pdbid) {
                items.push(
                    <span key={i} className="protein-section">
                        <span className="protein-text">
                            (<Link to={"/" + type + "/" + proteins[i].id}>{proteins[i].pdbid}</Link>
                            )
                        </span>
                    </span>
                );
            }
        }
        return items;
    }

    renderList(): React.ReactNode {
        let items = [];
        for (let i = 0; i < this.state.entries.length; i++) {
            items.push(
                <li key={i}>
                    {this.state.entries[i].name}{" "}
                    {this.renderListItem(this.state.entries[i].proteins)}
                </li>
            );
        }
        return items;
    }

    componentDidMount(): void {
        if (this.props.onResize !== undefined) {
            this.props.onResize();
        }
    }

    componentDidUpdate(): void {
        if (this.props.onResize !== undefined) {
            this.props.onResize();
        }
    }

    render(): JSX.Element {
        return (
            <div className="protein-list">
                <ol className="protein-data">{this.renderList()}</ol>
                <div
                    className={this.state.expandDisabled ? "hidden" : "expand"}
                    onClick={this.handleExpand}
                >
                    See All...
                </div>
                <div className={this.state.loading ? "info-loading" : "hidden"} key={1}>
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            </div>
        );
    }
}
