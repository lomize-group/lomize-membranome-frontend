import * as React from "react";
import "./MainContent.scss";

export type MainContentProps = {
    children: React.ReactNode;
};

const MainContent: React.FC<MainContentProps> = (props) => {
    return <div className="main-content">{props.children}</div>;
};

export default MainContent;
