import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { Assets } from "../../config/Assets";
import { Classification } from "../../config/Classification";
import { Urls } from "../../config/Urls";
import { createInfo, group, sortStrings } from "../../config/Util";
import { RootState } from "../../store";
import ExternalLink from "../external_link/ExternalLink";
import Info, { InfoProps } from "../info/Info";
import { RelatedComplexes } from "../protein/related_complexes/RelatedComplexes";
import ProteinList from "../list/protein/ProteinList";
import "./Pathway.scss";

function mapStateToProps(state: RootState) {
    return { currentUrl: state.currentUrl };
}

const connector = connect(mapStateToProps);

interface PathwayProps extends ConnectedProps<typeof connector> {}

interface PathwayState {
    pathway?: any;
    info?: InfoProps;
    loaded: boolean;
}

class Pathway extends React.Component<PathwayProps, PathwayState> {
    constructor(props: PathwayProps) {
        super(props);

        this.state = {
            pathway: undefined,
            info: undefined,
            loaded: false,
        };

        this.fetchData(props.currentUrl.params.id);
    }

    private fetchData = async (id: number | string) => {
        let result = await axios.get(Urls.baseUrl + Classification.definitions.pathways.api.route + "/" + id);
        const pathway = result.data;

        // map protein_pathways to proteins
        let protein_pathways = (await Promise.all(pathway.protein_pathways.map((protein_pathway: any) => (
            axios.get(Urls.baseUrl + Classification.definitions.proteins.api.route + "/" + protein_pathway.protein_id)
        )))).map((result: any) => result.data);

        // sort and group data
        pathway.complex_structures = sortStrings(
            pathway.complex_structures,
            "complex_structure_name_cache"
        );
        pathway.protein_pathways = group(
            sortStrings(protein_pathways, "pdbid"),
            (item: any) => item.pdbid.split("_")[1]
        );
        pathway.membranes = sortStrings(pathway.membranes, "membrane_name_cache");
        pathway.speciesNames = sortStrings(Object.keys(pathway.protein_pathways));

        this.setState({
            pathway,
            info: createInfo("pathways", pathway),
            loaded: true,
        });
    };

    private renderClassificationInfo(): React.ReactNode {
        if (this.state.info !== undefined) {
            return (
                <div>
                    <Info {...this.state.info} />
                </div>
            );
        }
        return null;
    }

    private renderTableRow(
        right: React.ReactNode,
        left: React.ReactNode,
        css_class: string
    ): React.ReactNode {
        return (
            <tr className={css_class}>
                <td className="right">
                    <b>{right}</b>
                </td>
                <td className="left">{left}</td>
            </tr>
        );
    }

    private renderProteinsCell(): React.ReactNode {
        if (this.state.pathway.protein_pathways.length === 0) {
            return <span>none</span>;
        }

        let items: JSX.Element[] = [];

        for (let i = 0; i < this.state.pathway.speciesNames.length; ++i) {
            const species = this.state.pathway.speciesNames[i];
            const proteins = this.state.pathway.protein_pathways[species];
            let subitems: JSX.Element[] = [];
            for (let j = 0; j < proteins.length; ++j) {
                const protein = proteins[j];
                subitems.push(
                    <span key={j}>
                        {j > 0 && ", "}
                        <Link to={"/proteins/" + protein.id}>{protein.pdbid}</Link>
                    </span>
                );
            }
            items.push(
                <div className="spaced">
                    <b>{species}:</b> {subitems}
                </div>
            );
        }

        return items;
    }

    private renderProteinsList(): React.ReactNode {
        return (<>
            <h2>Bitopic proteins</h2>
            {this.state.pathway.speciesNames.map((species: string) => (<>
                <b>{species}:</b>
                <ProteinList
                    key={species}
                    data={{ objects: this.state.pathway.protein_pathways[species] }} />
            </>))}
        </>);
    }

    renderMembranes(): React.ReactNode {
        if (this.state.pathway.membranes.length === 0) {
            return <span>none</span>;
        }

        let items: JSX.Element[] = [];
        for (let i = 0; i < this.state.pathway.membranes.length; ++i) {
            const mem = this.state.pathway.membranes[i];
            items.push(
                <div key={i}>
                    <Link to={"/protein_localizations/" + mem.membrane_id}>
                        {mem.membrane_name_cache}
                    </Link>
                </div>
            );
        }

        return items;
    }

    render(): JSX.Element {
        if (!this.state.loaded) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        return (
            <div className="pathway">
                <div className="page-header">{this.state.pathway.name}</div>
                {this.renderClassificationInfo()}
                <div className="info-table">
                    <table>
                        <tbody>
                            <tr className="dark">
                                <th colSpan={2}>
                                    <b>{this.state.pathway.name}</b>
                                </th>
                            </tr>
                            {this.renderTableRow(
                                "Bitopic proteins involved",
                                this.renderProteinsCell(),
                                "light"
                            )}
                            {/* TEMPORARY: Complexes are incorrect. Do not show until fixed. */}
                            {/* {this.renderTableRow(
                                "Complexes involved",
                                <RelatedComplexes
                                    complexes={this.state.pathway.complex_structures}
                                />,
                                "dark"
                            )} */}
                            {this.renderTableRow(
                                "Localizations involved",
                                this.renderMembranes(),
                                "dark"
                            )}
                            {this.renderTableRow(
                                "Source database",
                                <ExternalLink href={this.state.pathway.link} prepend_https>
                                    {this.state.pathway.dbname}
                                </ExternalLink>,
                                "light"
                            )}
                        </tbody>
                    </table>
                    {this.renderProteinsList()}
                </div>
            </div>
        );
    }
}

export default connector(Pathway);
