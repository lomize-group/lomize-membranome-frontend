import axios from "axios";
import { sanitize } from "dompurify";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { Assets } from "../../config/Assets";
import { Classification } from "../../config/Classification";
import { Columns } from "../../config/Columns";
import { OPM } from "../../config/OPM";
import { Urls } from "../../config/Urls";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import { createInfo, sortStrings } from "../../config/Util";
import { RootState } from "../../store";
import ExternalLink from "../external_link/ExternalLink";
import GLmolViewer from "../glmol/GLmolViewer";
import Info, { InfoProps } from "../info/Info";
import Table from "../table/Table";
import PDB from "./pdb/PDB";
import "./Protein.scss";
import { RelatedComplexes } from "./related_complexes/RelatedComplexes";
import { RelatedPathways } from "./related_pathways/RelatedPathways";
import Topology from "./topology/Topology";

function mapStateToProps(state: RootState) {
    return { currentUrl: state.currentUrl };
}

const connector = connect(mapStateToProps);

type ProteinProps = ConnectedProps<typeof connector>;

type ProteinState = {
    glmol: boolean;
    glmolLoaded: boolean;
    expanded: { [key: string]: boolean };
    hasData: boolean;
    protein: any;
    info: InfoProps;
    dimer_pdb_exists: boolean | null;
};

function createResidues(mutations: ReadonlyArray<any>): Array<any> {
    let residues = [];
    for (let i = 0; i < mutations.length; ++i) {
        residues.push({
            diseases: mutations[i].diseases_name_cache,
            residueNumber: parseInt(mutations[i].residue),
        });
    }
    residues.sort((a: any, b: any) => a.residueNumber - b.residueNumber);

    let uniqueResidues = [];
    for (let i = 0; i < residues.length; ++i) {
        if (
            uniqueResidues.length === 0 ||
            uniqueResidues[uniqueResidues.length - 1].residueNumber !== residues[i].residueNumber
        ) {
            uniqueResidues.push(residues[i]);
        }
    }

    return uniqueResidues;
}

function createRelatedProteins(related_proteins: Array<any>) {
    related_proteins = sortStrings(related_proteins, "related_protein_name_cache");
    for (let i = 0; i < related_proteins.length; ++i) {
        related_proteins[i].pubmed = related_proteins[i].pubmed
            ? related_proteins[i].pubmed.split(",")[0]
            : "";
        related_proteins[i].pdbid = related_proteins[i].pdbid
            ? related_proteins[i].pdbid.split(",")
            : [];
    }
    return related_proteins;
}

class Protein extends React.Component<ProteinProps, ProteinState> {
    constructor(props: ProteinProps) {
        super(props);
        this.state = {
            glmol: false,
            glmolLoaded: false,
            expanded: {},
            info: {},
            protein: null,
            hasData: false,
            dimer_pdb_exists: null,
        };
        this.getData(props.currentUrl.params.id);
    }

    toggleExpandCollapse = (title: string) => {
        const expanded = {
            ...this.state.expanded,
            [title]: !this.state.expanded[title],
        };
        this.setState({ expanded });
    };

    private async getData(id: string | number): Promise<void> {
        const res = await axios.get(
            Urls.baseUrl + Classification.definitions.proteins.api.route + "/" + id
        );
        let protein = res.data;
        protein.domains.sort((a: any, b: any) => a.firstaa - b.firstaa);
        protein.related_proteins = sortStrings(
            protein.related_proteins,
            "related_protein_name_cache"
        );
        protein.residues = createResidues(protein.mutations);
        protein.related_proteins = createRelatedProteins(protein.related_proteins);

        protein.topology = "Und";
        if (res.data.topology_show_in !== null) {
            protein.topology = protein.topology_show_in
                ? protein.membrane.topology_in
                : protein.membrane.topology_out;
        }

        // check for existance of dimer pdb file
        let dimer_pdb_exists = true;

        try {
            const res2 = await axios.head(Urls.dimer.pdbFile(protein.pdbid));

            if (res2.status >= 400) {
                dimer_pdb_exists = false;
            }
        } catch {
            dimer_pdb_exists = false;
        }

        this.setState({
            info: createInfo("proteins", res.data),
            protein,
            hasData: true,
            dimer_pdb_exists,
        });
    }

    renderInfo() {
        let items = [];
        if (this.state.hasOwnProperty("info")) {
            items.push(
                <div key={1}>
                    <Info {...this.state.info} />
                </div>
            );
        }
        return items;
    }

    renderLinks() {
        return (
            <span>
                <a
                    className="cursor-hand external"
                    target="_blank"
                    href={Classification.definitions.proteins.uniprotcode.uniprot(
                        this.state.protein.uniprotcode
                    )}
                >
                    UniProtKB
                </a>
                {", "}
                <a
                    className="cursor-hand external"
                    target="_blank"
                    href={Classification.definitions.proteins.uniprotcode.pfam(
                        this.state.protein.uniprotcode
                    )}
                >
                    Pfam
                </a>
                {", "}
                <a
                    className="cursor-hand external"
                    target="_blank"
                    href={Classification.definitions.proteins.uniprotcode.interpro(
                        this.state.protein.uniprotcode
                    )}
                >
                    Interpro
                </a>
                {this.state.protein.intact && (
                    <span>
                        {", "}
                        <a
                            className="cursor-hand external"
                            target="_blank"
                            href={Classification.definitions.proteins.uniprotcode.intact(
                                this.state.protein.uniprotcode
                            )}
                        >
                            IntAct
                        </a>
                    </span>
                )}
                {this.state.protein.biogrid && (
                    <span>
                        {", "}
                        <a
                            className="cursor-hand external"
                            target="_blank"
                            href={Classification.definitions.proteins.biogrid(
                                this.state.protein.biogrid
                            )}
                        >
                            BioGrid
                        </a>
                    </span>
                )}
                {", "}
                <a
                    className="cursor-hand external"
                    target="_blank"
                    href={Classification.definitions.proteins.uniprotcode.string(
                        this.state.protein.uniprotcode
                    )}
                >
                    STRING
                </a>
                {this.state.protein.hmp !== "" && (
                    <span>
                        {", "}
                        <a
                            className="cursor-hand external"
                            target="_blank"
                            href={Classification.definitions.proteins.hmp(this.state.protein.hmp)}
                        >
                            HMDB
                        </a>
                    </span>
                )}
                {this.state.protein.species_id === 1 && this.state.protein.genename !== "" && (
                    <span>
                        {", "}
                        <a
                            className="cursor-hand external"
                            target="_blank"
                            href={Classification.definitions.proteins.wiki(
                                this.state.protein.genename
                            )}
                        >
                            Wiki
                        </a>
                    </span>
                )}
                {", "}
                <a
                    className="cursor-hand external"
                    target="_blank"
                    href={Classification.definitions.proteins.uniprotcode.alphafold(
                        this.state.protein.uniprotcode
                    )}
                >
                   AlphaFold 
                </a>
            </span>
        );
    }

    renderPDBs() {
        if (this.state.protein.pdb_links.length === 0) {
            return <span>none</span>;
        }
        let items = [];
        for (let i = 0; i < this.state.protein.pdb_links.length; i++) {
            items.push(
                <span key={i}>
                    <ExternalLink
                        href={Classification.definitions.proteins.pdb(
                            this.state.protein.pdb_links[i].pdb
                        )}
                    >
                        {this.state.protein.pdb_links[i].pdb}
                    </ExternalLink>{" "}
                    ({this.state.protein.pdb_links[i].subunit_name})
                    {i + 1 < this.state.protein.pdb_links.length && ", "}
                </span>
            );
        }
        return items;
    }

    renderOPMs() {
        if (this.state.protein.opm_links.length === 0) {
            return <span>none</span>;
        }
        let items = [];
        for (let i = 0; i < this.state.protein.opm_links.length; i++) {
            items.push(
                <span key={i}>
                    <a
                        href={OPM.protein_by_pdbid(this.state.protein.opm_links[i].opm_link)}
                        target="_blank"
                    >
                        {this.state.protein.opm_links[i].opm_link}
                    </a>
                    {this.state.protein.opm_links[i].subunit_name &&
                        " (" + this.state.protein.opm_links[i].subunit_name + ")"}
                    {i + 1 < this.state.protein.opm_links.length && ", "}
                </span>
            );
        }
        return items;
    }

    renderProteins() {
        if (this.state.protein.related_proteins.length === 0) {
            return <span>none</span>;
        }

        let items = [];
        for (let i = 0; i < this.state.protein.related_proteins.length; i++) {
            const prot = this.state.protein.related_proteins[i];
            let pdbid_links = [];
            for (let j = 0; j < this.state.protein.related_proteins[i].pdbid.length; ++j) {
                pdbid_links.push(
                    <span key={j}>
                        , {j == 0 && "PDBID: "}
                        <a
                            className="external"
                            target="_blank"
                            href={Classification.definitions.proteins.interaction.pdb(
                                this.state.protein.related_proteins[i].pdbid[j]
                            )}
                        >
                            {prot.pdbid[j].toUpperCase()}
                        </a>
                    </span>
                );
            }

            items.push(
                <div key={i} style={{ paddingTop: "2px", paddingBottom: "2px" }}>
                    <Link
                        to={
                            "/proteins/" + this.state.protein.related_proteins[i].related_protein_id
                        }
                    >
                        {prot.related_protein_name_cache}
                    </Link>
                    : {this.state.protein.related_proteins[i].complex}
                    {pdbid_links}
                    {this.state.protein.related_proteins[i].pubmed && (
                        <span>
                            {", "}
                            <a
                                className="external"
                                target="_blank"
                                href={Urls.pubmed(this.state.protein.related_proteins[i].pubmed)}
                            >
                                PubMed
                            </a>
                            {prot.experiment_type === "ht" && "; high-throughput"}
                            {prot.interaction_type === "c" && "; co-complex"}
                        </span>
                    )} ({this.state.protein.related_proteins[i].dbname})
                    
                </div>
            );
        }
        return items;
    }

    renderDomains() {
        if (this.state.protein.domains.length === 0) {
            return <span>none</span>;
        }
        let items = [];
        for (let i = 0; i < this.state.protein.domains.length; i++) {
            let d = this.state.protein.domains[i];
            items.push(
                <p key={i} className="domain">
                    AA:&nbsp;{d.firstaa}-{d.lastaa},
                    {d.protein_pdbid !== "" && (
                        <span>
                            {" "}
                            PDBID:&nbsp;
                            <a
                                className="external"
                                target="_blank"
                                href={Classification.definitions.proteins.domain.pdb(
                                    d.protein_pdbid
                                )}
                            >
                                {d.protein_pdbid}
                            </a>
                            ,
                        </span>
                    )}{" "}
                    Subunit&nbsp;<b>{d.pdbsubunit}</b>, Seq Identity:&nbsp;{d.seqidentity}%,{" "}
                    {d.pfam_link !== "" && (
                        <a
                            target="_blank"
                            href={Classification.definitions.proteins.domain.pfam(d.pfamlink)}
                        >
                            {d.domain}
                        </a>
                    )}
                </p>
            );
        }
        return items;
    }

    renderTableRow(right: React.ReactNode, left: React.ReactNode, attr: string): JSX.Element {
        return (
            <tr className={attr}>
                <td className="right">
                    <b>{right}</b>
                </td>
                <td className="left">{left}</td>
            </tr>
        );
    }

    renderTable(title: string, content: any, collapsible = false) {
        if (!content) {
            return;
        }

        const expanded = this.state.expanded[title] || !collapsible;

        return (
            <div className="info-table full">
                <table>
                    <tbody>
                        <tr className="dark">
                            <th colSpan={2}>
                                <span className="green italic">{title}: </span>
                                {this.state.protein.name}{" "}
                                {collapsible && (
                                    <span
                                        className="link expand-link"
                                        onClick={() => this.toggleExpandCollapse(title)}
                                    >
                                        {expanded ? "(hide)" : "(show)"}
                                    </span>
                                )}
                            </th>
                        </tr>
                        {expanded && (
                            <tr>
                                <td
                                    colSpan={2}
                                    dangerouslySetInnerHTML={{ __html: sanitize(content) }}
                                />
                            </tr>
                        )}
                    </tbody>
                </table>
            </div>
        );
    }

    renderSequence(): JSX.Element | null {
        interface Segment {
            start: number;
            end: number;
        }
        interface ColorSegments {
            red: Segment;
            blue: Segment;
        }

        if (this.state.protein.aaseq === "") {
            return null;
        }
        const segment = this.state.protein.segment.replace(/\s+/g, "");
        // split of empty string is array with empty string, so avoid it
        const segment_strs: string = segment !== '' ? segment.split(",") : [];

        let segments: Array<ColorSegments> = [];

        for (let i = 0; i < segment_strs.length; i++) {
            const parts = segment_strs[i].split("(");
            const reds = parts[0].split("-");
            const red: Segment = {
                start: parseInt(reds[0]) - 1,
                end: parseInt(reds[1]),
            };
            const blues = parts[1].slice(0, parts[1].length - 1).split("-");
            const blue: Segment = {
                start: parseInt(blues[0]) - 1,
                end: parseInt(blues[1]),
            };
            segments.push({ red, blue });
        }

        segments = segments.sort((a: ColorSegments, b: ColorSegments) => {
            const left = a.blue.start < a.red.start ? a.blue.start : a.red.start;
            const right = b.blue.start < b.red.start ? b.blue.start : b.red.start;
            return left - right;
        });

        const { residues } = this.state.protein;

        // green/yellow highlighting of residues
        let sequence = [];
        let residueIdx = 0;
        let spaceCount = 0;
        for (let i = 0; i < this.state.protein.aaseq.length; i++) {
            if (this.state.protein.aaseq[i] !== " ") {
                if (
                    residues.length > residueIdx &&
                    i - spaceCount == residues[residueIdx].residueNumber - 1
                ) {
                    let highlight =
                        residues[residueIdx].diseases == "polymorphism"
                            ? "highlight-green"
                            : "highlight-yellow";
                    sequence.push(
                        <span key={-i} className={highlight}>
                            {this.state.protein.aaseq[i]}
                        </span>
                    );
                    ++residueIdx;
                } else {
                    sequence.push(<span key={-i}>{this.state.protein.aaseq[i]}</span>);
                }
            } else {
                ++spaceCount;
            }
        }

        let content = [];

        // red/blue coloring of residues
        let position = 0;

        for (let i = 0; i < segments.length; i++) {
            const { red, blue } = segments[i];
            const red_starts = red.start <= blue.start;
            const red_ends = red.end >= blue.end;
            let color = "";
            let next = red_starts ? red.start : blue.start;
            const end = red_ends ? red.end : blue.end;

            while (position !== end) {
                let seq: Array<JSX.Element> = [];
                for (let i = position; i < next; i++) {
                    if (i !== 0 && i % 10 === 0) {
                        seq.push(<span key={seq.length}> </span>);
                    }
                    seq.push(sequence[i]);
                }
                content.push(
                    <span key={content.length} className={color}>
                        {seq}
                    </span>
                );
                position = next;
                if (next === red.start) {
                    next = red.end;
                    color = "red";
                } else if (next === blue.start && blue.end < red.start) {
                    next = blue.end;
                    color = "blue";
                } else if (next === blue.start) {
                    next = red.start;
                    color = "blue";
                } else if (next === red.end && !red_ends) {
                    next = blue.end;
                    color = "blue";
                } else if (next === blue.end) {
                    next = red.start;
                    color = "";
                }
            }
        }

        if (position < sequence.length) {
            let seq: Array<JSX.Element> = [];
            for (let i = position; i < sequence.length; i++) {
                if (i !== 0 && i % 10 === 0) {
                    seq.push(<span key={seq.length}> </span>);
                }
                seq.push(sequence[i]);
            }
            content.push(<span key={content.length}>{seq}</span>);
        }

        return (
            <div className="info-table full">
                <table>
                    <tbody>
                        <tr className="dark">
                            <th colSpan={2}>
                                <span className="green italic">Amino Acid Sequence:</span>{" "}
                                {this.state.protein.name}{" "}
                            </th>
                        </tr>
                        <tr>
                            <td colSpan={2} className="mono">
                                {content}
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }

    renderMutationTable() {
        const id = this.props.currentUrl.params.id;
        const url =
            Urls.baseUrl +
            Classification.definitions.proteins.api.route +
            "/" +
            id +
            Classification.definitions.mutations.api.route;
        const cols = Columns.protein_mutations.columns;
        return (
            <Table
                title={"Mutations in transmembrane helix of " + this.state.protein.name}
                url={url}
                rowHeight={Columns.protein_mutations.rowHeight}
                headerSize="small"
                columns={cols}
                defaultSort={cols[0].id}
                showSearch={false}
                showSpeciesFilter={false}
                showWhenEmpty={false}
                redirect={false}
            />
        );
    }

    renderAdditionalMembranes() {
        if (this.state.protein.additional_membranes.length === 0) {
            return <span>none</span>;
        }

        let content = [];
        for (let i = 0; i < this.state.protein.additional_membranes.length; ++i) {
            const am = this.state.protein.additional_membranes[i];
            content.push(
                <span key={i}>
                    {i > 0 && ","} {am.membrane_name_cache}{" "}
                    <a target="_blank" className="external" href={Urls.pubmed(am.pubmed)}>
                        PubMed
                    </a>
                </span>
            );
        }
        return content;
    }
    componentWillReceiveProps(nextProps: ProteinProps) {
        if (nextProps.currentUrl.params.id !== this.props.currentUrl.params.id) {
            this.getData(nextProps.currentUrl.params.id);
            this.setState({
                hasData: false,
                glmol: false,
                glmolLoaded: false,
            });
        }
    }

    render() {
        if (!this.state.hasData) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        let dimer_info;

        if (this.state.dimer_pdb_exists) {
            dimer_info = (<>
                <div className="section-header">TM domain</div>
                <PDB
                    pdbid={this.state.protein.pdbid}
                    linkText="Download FMAP/TMDOCK model"
                    imageUrl={Urls.dimer.image(this.state.protein.pdbid)}
                    pdbFileUrl={Urls.dimer.pdbFile(this.state.protein.pdbid)}
                    className="small"
                />
                <div>
                    3D view in{" "}
                    <span
                        className="link"
                        onClick={() => this.setState({ glmol: true, glmolLoaded: true })}
                    >
                        GLMol
                    </span>
                </div>
            </>);
        } else if (this.state.dimer_pdb_exists === false) {
            // display nothing if dimer pdb does not exist
            dimer_info = undefined;
        } else /* null */ {
            dimer_info = <ImgWrapper src={Assets.images.loading} />
        }

        const pdbFileUrl = Classification.definitions.proteins.pdb_file(this.state.protein.pdbid);

        return (
            <div className="protein">
                <div className="page-header">
                    {this.state.protein.pdbid} » {this.state.protein.name}
                </div>
                <div className={this.state.glmol ? "protein-glmol" : "hidden"}>
                    {this.state.glmolLoaded && (
                        <div>
                            <div>
                                <GLmolViewer pdbid={this.state.protein.pdbid} />
                            </div>
                            <div className="go-back">
                                <span onClick={() => this.setState({ glmol: false })}>
                                    Return to Protein Info
                                </span>
                            </div>
                        </div>
                    )}
                </div>
                <div className={!this.state.glmol ? "protein-content" : "hidden"}>
                    <div className="top-grid">
                        <div>
                            <div className="protein-info">{this.renderInfo()}</div>
                            <div className="info-table">
                                <table>
                                    <tbody>
                                        <tr className="dark">
                                            <th colSpan={2}>
                                                <b>
                                                    {this.state.protein.pdbid} »{" "}
                                                    {this.state.protein.altname
                                                        ? this.state.protein.altname
                                                        : this.state.protein.name}
                                                </b>
                                            </th>
                                        </tr>
                                        {this.renderTableRow(
                                            "Hydrophobic Thickness",
                                            this.state.protein.thickness + " Å",
                                            "light"
                                        )}
                                        {this.renderTableRow(
                                            "Tilt Angle",
                                            this.state.protein.tilt + "°",
                                            "dark"
                                        )}
                                        {this.renderTableRow(
                                            <span>
                                                ΔG<sub>transfer</sub>
                                            </span>,
                                            this.state.protein.interpro + " kcal/mol",
                                            "light"
                                        )}
                                        {this.renderTableRow(
                                            <span>
                                                ΔG<sub>fold</sub>
                                            </span>,
                                            this.state.protein.gibbs + " kcal/mol",
                                            "dark"
                                        )}
                                        {this.renderTableRow("Links", this.renderLinks(), "light")}
                                        {this.renderTableRow(
                                            "TM Segments",
                                            this.state.protein.segment,
                                            "dark"
                                        )}
                                        {this.renderTableRow(
                                            "Topology",
                                            this.state.protein.topology,
                                            "light"
                                        )}
                                        {this.renderTableRow(
                                            "Additional localizations",
                                            this.renderAdditionalMembranes(),
                                            "dark"
                                        )}
                                        {this.renderTableRow(
                                            "Pathways",
                                            <RelatedPathways
                                                pathways={this.state.protein.protein_pathways}
                                            />,
                                            "light"
                                        )}
                                        {this.renderTableRow("PDB", this.renderPDBs(), "dark")}
                                        {this.renderTableRow("OPM", this.renderOPMs(), "light")}
                                        {this.renderTableRow(
                                            "Complexes",
                                            <RelatedComplexes
                                                complexes={this.state.protein.complex_structures}
                                            />,
                                            "dark"
                                        )}
                                        {this.renderTableRow(
                                            "Interactions",
                                            this.renderProteins(),
                                            "light"
                                        )}
                                        {this.renderTableRow("Domains", this.renderDomains(), "dark")}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div>
                            <div className="random-protein">
                                <PDB pdbid={this.state.protein.pdbid} linkText="Download AlphaFold-based model" />
                                <div>
                                    3D view in{" "}
                                    <a
                                        target="_blank"
                                        className="external"
                                        href={Classification.definitions.proteins.jmol(
                                            this.state.protein.pdbid
                                        )}
                                    >
                                        Jmol
                                    </a>
                                    {" or "}
                                    <ExternalLink href={Urls.icn3d(pdbFileUrl)}>
                                        iCn3D
                                    </ExternalLink>
                                </div>
                                <Topology
                                    membraneName={this.state.protein.membrane.name}
                                    topologyOut={this.state.protein.membrane.topology_out}
                                    topologyIn={this.state.protein.membrane.topology_in}
                                />
                                {dimer_info}
                            </div>
                        </div>
                    </div>
                    {this.renderTable("Uniprot Annotation", this.state.protein.verification)}
                    {this.renderTable("Uniprot Features", this.state.protein.comments, true)}
                    {this.renderSequence()}
                </div>
                {this.renderMutationTable()}
            </div>
        );
    }
}

export default connector(Protein);
