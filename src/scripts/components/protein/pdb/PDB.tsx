import * as React from "react";
import { Classification } from "../../../config/Classification";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./PDB.scss";

export type PDBProps = {
    pdbid: string;
    linkText: string;
    imageUrl?: string;
    pdbFileUrl?: string;
    className?: string;
};

class PDB extends React.Component<PDBProps> {
    static defaultProps = {
        imageUrl: null,
        pdbFileUrl: null,
        className: "img-style",
    };

    render() {
        const { pdbid, linkText, className } = this.props;
        let { imageUrl, pdbFileUrl } = this.props;
        if (imageUrl === null) {
            imageUrl = Classification.definitions.proteins.pdb_image(pdbid);
        }
        if (pdbFileUrl === null) {
            pdbFileUrl = Classification.definitions.proteins.pdb_file(pdbid);
        }
        return (
            <div className="pdb">
                <a href={imageUrl} target="_blank">
                    <ImgWrapper
                        className={className}
                        src={imageUrl!}
                    />
                </a>
                <hr className="divider" />
                <div className="protein-link">
                    <a href={pdbFileUrl} download>
                        {linkText}
                    </a>
                </div>
            </div>
        );
    }
}

export default PDB;
