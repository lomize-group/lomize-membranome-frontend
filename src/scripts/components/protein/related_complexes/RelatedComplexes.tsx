import * as React from "react";
import { Link } from "react-router-dom";
import { sortStrings } from "../../../config/Util";

type RelatedComplexesProps = {
    complexes: Array<any>;
    sort?: boolean;
};

export const RelatedComplexes: React.FC<RelatedComplexesProps> = (props) => {
    if (props.complexes.length === 0) {
        return <span>none</span>;
    }

    if (props.sort) {
        props.complexes = sortStrings(props.complexes, "complex_structure_name_cache");
    }

    let items: JSX.Element[] = [];
    for (let i = 0; i < props.complexes.length; i++) {
        items.push(
            <div key={i}>
                <Link to={"/complex_structures/" + props.complexes[i].complex_structure_id}>
                    {props.complexes[i].complex_structure_name_cache}
                </Link>
            </div>
        );
    }

    return <span>{items}</span>;
};
