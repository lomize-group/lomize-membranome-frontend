import { sanitize } from "dompurify";
import * as React from "react";
import { Link } from "react-router-dom";
import { sortStrings } from "../../../config/Util";
import ExternalLink from "../../external_link/ExternalLink";

type PathwaysProps = {
    pathways: Array<any>;
    sort?: boolean;
};

export const RelatedPathways: React.SFC<PathwaysProps> = (props) => {
    if (props.pathways.length === 0) {
        return <span>none</span>;
    }

    if (props.sort) {
        props.pathways = sortStrings(props.pathways, "pathway_name_cache");
    }

    let items: JSX.Element[] = [];
    for (let i = 0; i < props.pathways.length; i++) {
        const pathway_link_text = sanitize(props.pathways[i].pathway_name_cache);
        items.push(
            <span key={i}>
                <Link
                    to={"/pathways/" + props.pathways[i].pathway_id}
                    dangerouslySetInnerHTML={{ __html: pathway_link_text }}
                />{" "}
                (
                <ExternalLink href={props.pathways[i].pathway_link_cache} prepend_https>
                    {props.pathways[i].pathway_dbname_cache}
                </ExternalLink>
                )
                <br />
            </span>
        );
    }

    return <span>{items}</span>;
};
