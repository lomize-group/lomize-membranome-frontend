import * as React from "react";
import { Assets } from "../../../config/Assets";
import ImgWrapper from "../../img_wrapper/ImgWrapper";
import "./Topology.scss";

export type TopologyProps = {
    membraneName: string;
    topologyIn: string;
    topologyOut: string;
};

const Topology: React.FC<TopologyProps> = (props) => {
    return (
        <div className="topology-section">
            <div className="topology-header">Topology in {props.membraneName}</div>
            <table>
                <tbody>
                    <tr>
                        <td rowSpan={2}>
                            <ImgWrapper className="img-style" src={Assets.images.topology} />
                        </td>
                        <td className="topology-info out">{props.topologyOut}</td>
                    </tr>
                    <tr>
                        <td className="topology-info in">{props.topologyIn}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
};

export default Topology;
