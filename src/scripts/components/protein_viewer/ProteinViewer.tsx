import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Assets } from "../../config/Assets";
import { Classification, CTypeCategory } from "../../config/Classification";
import { Urls } from "../../config/Urls";
import { RootState } from "../../store";
import GLmolViewer from "../glmol/GLmolViewer";
import "./ProteinViewer.scss";

function mapStateToProps(state: RootState) {
    return { currentUrl: state.currentUrl };
}

const connector = connect(mapStateToProps);

interface ProteinViewerProps extends ConnectedProps<typeof connector> {
    ctype?: CTypeCategory;
}

interface ProteinViewerState {
    pdbid: string;
    ctype: CTypeCategory;
    pdbUrl: string;
    name: string;
    loaded: boolean;
    isFmap: boolean;
}

class ProteinViewer extends React.Component<ProteinViewerProps, ProteinViewerState> {
    constructor(props: ProteinViewerProps) {
        super(props);

        if (props.currentUrl.params.hasOwnProperty("id")) {
            const { id, pdbid } = props.currentUrl.params;
            const ctype = props.ctype || "proteins";
            this.state = {
                pdbid,
                ctype,
                pdbUrl: Classification.definitions[ctype].pdb_file(pdbid),
                name: "",
                loaded: false,
                isFmap: false
            };

            axios
                .get(Urls.baseUrl + Classification.definitions[ctype].api.route + "/" + id)
                .then((res) => {
                    const pdbid =
                        this.state.ctype === "complex_structures"
                            ? res.data.refpdbcode
                            : res.data.pdbid;
                    this.setState({
                        pdbid,
                        name: res.data.name,
                        pdbUrl: Classification.definitions[ctype].pdb_file(pdbid),
                        loaded: true,
                    });
                });
        } else {
            this.state = {
                pdbid: "",
                ctype: props.currentUrl.query.complex ? "complex_structures" : "proteins",
                pdbUrl: props.currentUrl.query.url,
                name: "",
                loaded: true,
                isFmap: props.currentUrl.query.url.startsWith("https://fmap-results")
            };
        }
    }

    render() {
        if (!this.state.loaded) {
            return <img src={Assets.images.loading} alt="Loading" />;
        }

        return (
            <div className="protein-viewer">
                <div className="page-header">
                    {this.state.pdbid && this.state.name
                        ? this.state.pdbid + " » " + this.state.name
                        : "GLMol Visualization"}
                </div>
                <GLmolViewer
                    ctype={this.state.ctype}
                    pdbUrl={this.state.pdbUrl}
                    isFmap={this.state.isFmap}
                    components={{
                        colorBy: {
                            value: this.props.ctype === "complex_structures" ? "chain" : "residue",
                        },
                    }}
                />
            </div>
        );
    }
}

export default connector(ProteinViewer);
