import * as React from "react";
import ExternalLink from "../external_link/ExternalLink";

export type PubMedLinkProps = {
    id: string;
};

const PUBMED_BASE_URL = "http://www.ncbi.nlm.nih.gov/pubmed/" as const;

const PubMedLink: React.FC<PubMedLinkProps> = ({ id, children = "PubMed" }) => (
    <ExternalLink href={PUBMED_BASE_URL + id}>{children}</ExternalLink>
);

export default PubMedLink;
