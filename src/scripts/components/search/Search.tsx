import "bootstrap";
import * as React from "react";
import "./Search.scss";

type OptionalSearchProps = {
    placeholder: string;
    initial?: string;
};

export type SearchProps = OptionalSearchProps & {
    onSearch: (search: string) => any;
};

export default class Search extends React.Component<SearchProps> {
    static defaultProps: OptionalSearchProps = {
        placeholder: "Search...",
        initial: undefined,
    };

    private textInput!: HTMLInputElement;

    handleSearch = () => {
        this.props.onSearch(this.textInput.value);
    };

    handleKeyPress = (event: React.KeyboardEvent<HTMLInputElement>) => {
        if (event.key === "Enter") {
            this.handleSearch();
            // this.props.onSearch((event.target as HTMLInputElement).value);
        }
    };

    componentWillReceiveProps(nextProps: SearchProps) {
        if (nextProps.initial !== undefined) {
            this.textInput.value = nextProps.initial;
        }
    }

    render() {
        return (
            <div className="search">
                <div className="input-group">
                    <input
                        ref={(ref: HTMLInputElement) => (this.textInput = ref)}
                        type="text"
                        className="search-bar form-control input-sm"
                        onKeyPress={this.handleKeyPress}
                        placeholder={this.props.placeholder}
                    />
                    <span className="submit-button input-group-addon" onClick={this.handleSearch}>
                        <i>search</i>
                    </span>
                </div>
            </div>
        );
    }
}
