import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { Assets } from "../../config/Assets";
import { Classification, StatsType } from "../../config/Classification";
import { Urls } from "../../config/Urls";
import { RootState } from "../../store";
import { loadStats } from "../../store/stats";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import Section from "./section/Section";
import "./Sidebar.scss";

function mapStateToProps(state: RootState) {
    return {
        stats: state.stats,
        currentUrl: state.currentUrl,
    };
}

const connector = connect(mapStateToProps, { loadStats });

interface SidebarProps extends ConnectedProps<typeof connector> {}

class Sidebar extends React.Component<SidebarProps> {
    constructor(props: SidebarProps) {
        super(props);

        if (props.stats === null) {
            axios.get(Urls.baseUrl + "/stats").then((res) => {
                const { table } = res.data;
                props.loadStats({
                    protein_types: table.protein_types,
                    protein_classes: table.protein_classes,
                    protein_superfamilies: table.protein_superfamilies,
                    protein_families: table.protein_families,
                    proteins: table.proteins,
                    species: table.species,
                    protein_localizations: table.membranes,
                    complex_localizations: table.membranes,
                    complex_classes: table.complex_classes,
                    complex_superfamilies: table.complex_superfamilies,
                    complex_families: table.complex_families,
                    complex_structures: table.complex_structures,
                    mutation_types: table.mutation_types,
                    diseases: table.diseases,
                    mutations: table.mutations,
                    pathway_classes: table.pathway_classes,
                    pathway_subclasses: table.pathway_subclasses,
                    pathways: table.pathways,
                });
            });
        }
    }

    private getCtypeFromUrl = (): StatsType => {
        return this.props.currentUrl.url.split("/")[1] as StatsType;
    };

    render() {
        return (
            <div className="sidebar">
                <Section
                    title="Proteins"
                    currentCtype={this.getCtypeFromUrl()}
                    ctypeList={Classification.proteins}
                    stats={this.props.stats}
                />
                <Section
                    title="Protein Complexes"
                    currentCtype={this.getCtypeFromUrl()}
                    ctypeList={Classification.complex_structures}
                    stats={this.props.stats}
                />
                <Section
                    title="Mutations in TMH"
                    currentCtype={this.getCtypeFromUrl()}
                    ctypeList={Classification.mutations}
                    stats={this.props.stats}
                />
                <Section
                    title="Pathways"
                    currentCtype={this.getCtypeFromUrl()}
                    ctypeList={Classification.pathways}
                    stats={this.props.stats}
                />
                <div className="server-img">
                    <Link to="/fmap_cgopm">
                        <ImgWrapper className="img-style" src={Assets.images.fmap} />
                    </Link>
                </div>
                <div className="server-img">
                    <Link to="/tmdock_cgopm">
                        <ImgWrapper className="img-style" src={Assets.images.tmdock} />
                    </Link>
                </div>
            </div>
        );
    }
}

export default connector(Sidebar);
