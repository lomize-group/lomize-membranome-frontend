import * as React from "react";
import { Link } from "react-router-dom";
import { Assets } from "../../../config/Assets";
import { Classification, StatsType } from "../../../config/Classification";
import { capitalize } from "../../../config/Util";
import { StatsState } from "../../../store/stats/";

function navItem(
    key: number,
    name: string,
    route: string,
    subclasses: any,
    is_selected: boolean
): JSX.Element {
    if (is_selected) {
        return (
            <div key={key} className={"item"}>
                <strong>{name}</strong>
                <span> ({subclasses})</span>
            </div>
        );
    }

    return (
        <div key={key} className={"item not-selected"}>
            <Link to={route}>
                <strong>{name}</strong>
            </Link>
            <span> ({subclasses})</span>
        </div>
    );
}

export type SectionProps = {
    title: string;
    currentCtype: StatsType;
    ctypeList: Array<StatsType>;
    stats: StatsState | null;
};

function Section(props: SectionProps): JSX.Element {
    const renderNavItems = (): React.ReactNodeArray => {
        let navItems: React.ReactNodeArray = [];
        for (let i = 0; i < props.ctypeList.length; ++i) {
            const ctype: StatsType = props.ctypeList[i];
            const name: string = capitalize(Classification.definitions[ctype].name);
            const route: string = "/" + ctype;
            navItems.push(
                navItem(i, name, route, props.stats![ctype], props.currentCtype === ctype)
            );
        }
        return navItems;
    };

    const loading: boolean = props.stats === null;

    return (
        <div>
            <div className="title">
                <b>{props.title}</b>
            </div>
            <div className={loading ? "info-loading" : "hidden"}>
                <img className="loading-image" src={Assets.images.loading} />
            </div>
            {!loading && <div className={"sidebar-nav"}>{renderNavItems()}</div>}
        </div>
    );
}

export default Section;
