import MultiSelect, { MultiSelectOption } from "@khanacademy/react-multi-select";
import * as appendQuery from "append-query";
import axios from "axios";
import "bootstrap";
import { Properties as CSSProperties } from "csstype";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import {
    AutoSizer,
    Column,
    HeaderMouseEventHandlerParams,
    Table as VTable,
    WindowScroller,
} from "react-virtualized";
import { Assets } from "../../config/Assets";
import { Classification, CTypeCategory } from "../../config/Classification";
import {
    ApiPagedResponse,
    ApiParams,
    ApiSearchOptions,
    GenericObj,
    TableColumn,
} from "../../config/Types";
import { Urls } from "../../config/Urls";
import { RootState } from "../../store";
import ImgWrapper from "../img_wrapper/ImgWrapper";
import Search from "../search/Search";
import "./react-virtualized.scss";
import "./Table.scss";

function mapStateToProps(state: RootState) {
    return { currentUrl: state.currentUrl };
}

const connector = connect(mapStateToProps, null, null, { forwardRef: true });

export type TableProps = {
    url: string;
    title: string;
    rowHeight: number;
    columns: ReadonlyArray<TableColumn>;
    ctype?: CTypeCategory;
    search?: string;
    searchOptions?: ApiSearchOptions;
    defaultSort?: string;
    redirect?: boolean;
    headerSize?: "small" | "large";
    showSearch?: boolean;
    showSpeciesFilter?: boolean;
    showWhenEmpty?: boolean;
    pageSize?: number;
    autoLoad?: boolean;
    selectedSpecies?: Array<string>;
    data?: ApiPagedResponse;
    ref?: (ref: any) => any;
};

type ConnectedTableProps = TableProps & ConnectedProps<typeof connector>;

type TableState = {
    data: ApiPagedResponse;
    title: string;
    loading: boolean;
    speciesLoading: boolean;
    isPrinting: boolean;
    search: string;
    headerHeight: number;
    style: CSSProperties;
    displayFloatingImg: boolean;
    floatingImgName: string;
    floatingImageSrc: string;
    speciesList: Array<MultiSelectOption>;
    selectedSpecies: Array<string>;
    tableWidth: number;
};

class Table extends React.Component<ConnectedTableProps, TableState> {
    static defaultProps: Partial<TableProps> = {
        search: "",
        defaultSort: "",
        redirect: true,
        ctype: "proteins",
        headerSize: "large",
        showSearch: true,
        showSpeciesFilter: true,
        showWhenEmpty: true,
        pageSize: 500,
        autoLoad: true,
        selectedSpecies: [],
    };

    private requests: number;
    private unmounted: boolean;
    private windowScrollerRef: any;

    constructor(props: ConnectedTableProps) {
        super(props);

        const genMinWidths = (): number => {
            let width = 0;
            for (let i = 0; i < this.props.columns.length; i++) {
                const col = this.props.columns[i];
                if (col.width) {
                    width += col.width;
                } else if (col.minWidth) {
                    width += col.minWidth;
                }
            }
            return width;
        };

        this.state = {
            data: {
                total_objects: 0,
                objects: [],
                sort: props.defaultSort!,
                direction: props.defaultSort === "" ? "" : "ASC",
                page_size: 0,
                page_num: 0,
            },
            title: props.title,
            loading: props.autoLoad || false,
            speciesLoading: true,
            isPrinting: false,
            search: props.search!,
            headerHeight: 65,
            style: {},
            displayFloatingImg: false,
            floatingImgName: "",
            floatingImageSrc: "",
            speciesList: [],
            selectedSpecies: props.selectedSpecies || [],
            tableWidth: genMinWidths(),
        };

        this.unmounted = false;
        this.requests = 0;

        if (props.showSpeciesFilter) {
            axios.get(Urls.baseUrl + Classification.definitions.species.api.route).then((res) => {
                let speciesList: MultiSelectOption[] = [];
                for (let i = 0; i < res.data.objects.length; i++) {
                    speciesList.push({
                        label: res.data.objects[i].name,
                        value: res.data.objects[i].id,
                    });
                }
                this.setState({
                    speciesLoading: false,
                    speciesList,
                });
            });
        }

        if (props.data !== undefined) {
            this.state = {
                ...this.state,
                data: props.data,
            };
        } else if (props.autoLoad) {
            const params = {
                search: props.search,
                sort: this.state.data.sort,
                direction: this.state.data.direction,
                species: this.state.selectedSpecies.join(","),
            };
            this.fetchData(params, props.search !== "", true);
            if (props.search !== "") {
                this.state = {
                    ...this.state,
                    title: 'Search Results for "' + props.search + '"',
                };
            }
        }
    }

    createParams = (params: ApiParams): ApiParams => {
        let result: ApiParams = { ...params };

        if (!params.search_by && this.props.searchOptions?.search_by) {
            result.search_by = this.props.searchOptions.search_by;
        }
        if (!params.search_separator && this.props.searchOptions?.search_separator) {
            result.search_separator = this.props.searchOptions.search_separator;
        }

        return result;
    };

    renderCell = (column: TableColumn) => {
        return (props: any) => {
            let row = props.rowData;
            row.onImgChange = (state: any) => {
                this.setState(state);
            };

            let color_class = props.rowIndex % 2 === 0 ? "even" : "odd";

            return <div className={color_class}>{column.Cell(row)}</div>;
        };
    };

    renderHeader = (column: TableColumn) => {
        return () => {
            let sorted_class = "hidden";
            if (this.state.data.sort === column.id) {
                sorted_class = this.state.data.direction.toLowerCase();
            }

            return (
                <div className={"header-wrapper" + (column.id === "" ? " unsortable" : "")}>
                    {column.Header()}
                    <div className={sorted_class}></div>
                </div>
            );
        };
    };

    rowHeight = (): number => {
        return this.props.rowHeight;
    };

    renderBody() {
        let items = [];
        for (let i = 0; i < this.props.columns.length; i++) {
            const col = this.props.columns[i];
            items.push(
                <Column
                    key={i}
                    headerRenderer={this.renderHeader(col)}
                    cellRenderer={this.renderCell(col)}
                    width={!col.width ? col.minWidth! : col.width}
                    flexGrow={!col.flexGrow ? 0 : col.flexGrow}
                    flexShrink={!col.hasOwnProperty("flexGrow") ? 0 : 1}
                    minWidth={!col.hasOwnProperty("minWidth") ? 0 : col.minWidth}
                    dataKey={col.id}
                />
            );
        }
        return items;
    }

    fetchData = (params: ApiParams, search = false, replace = false, page = 1) => {
        params.pageSize = this.props.pageSize;
        params.pageNum = page;
        const url = appendQuery(this.props.url, this.createParams(params) as GenericObj);

        this.requests++;
        const req = this.requests;
        axios.get(url).then((res) => {
            if (this.unmounted || req !== this.requests) {
                return;
            }
            res.data.objects = this.state.data.objects.concat(res.data.objects);

            if (search && this.props.redirect && res.data.total_objects === 1) {
                const route = this.props.ctype;
                if (replace) {
                    this.props.currentUrl.history.replace(
                        "/" + route + "/" + res.data.objects[0].id
                    );
                    return;
                }

                this.props.currentUrl.history.push("/" + route + "/" + res.data.objects[0].id);
                return;
            }
            if (res.data.total_objects <= res.data.objects.length) {
                this.setState({
                    data: res.data,
                    loading: false,
                });
                return;
            }
            this.setState({
                data: res.data,
            });

            this.fetchData(params, search, replace, page + 1);
        });
    };

    handleSort = (props: HeaderMouseEventHandlerParams) => {
        if (props.dataKey === "") {
            return;
        }
        let direction = "ASC";
        if (props.dataKey === this.state.data.sort && this.state.data.direction === "ASC") {
            direction = "DESC";
        }

        let params: ApiParams = {
            search: this.state.search,
            sort: props.dataKey,
            direction: direction,
            species: this.state.selectedSpecies.join(","),
        };

        this.fetchData(params);
        this.setState({
            loading: true,
            search: this.state.search,
            data: {
                total_objects: 0,
                objects: [],
                sort: props.dataKey,
                direction: direction,
                page_size: 0,
                page_num: 0,
            },
            title:
                this.state.search === ""
                    ? this.props.title
                    : 'Search Results for "' + this.state.search + '"',
        });
    };

    handleSearch = (value: string, replace = false) => {
        let params: ApiParams = {
            search: value,
            sort: this.state.data.sort,
            direction: this.state.data.direction,
            species: this.state.selectedSpecies.join(","),
        };

        this.fetchData(params, true, replace);
        this.setState({
            loading: true,
            search: value,
            data: {
                total_objects: 0,
                objects: [],
                sort: this.state.data.sort,
                direction: this.state.data.direction,
                page_size: 0,
                page_num: 0,
            },
            title: value === "" ? this.props.title : 'Search Results for "' + value + '"',
        });
    };

    handleNoRows = () => {
        return <div className={this.state.loading ? "hidden" : "no-rows"}>no results found</div>;
    };

    onSpeciesSelected = (val: Array<string>, curProps?: TableProps) => {
        const props = curProps ? curProps : this.props;

        if (val.length === this.state.selectedSpecies.length) {
            let diff = false;
            for (let i = 0; i < val.length; i++) {
                if (val[i] !== this.state.selectedSpecies[i]) {
                    diff = true;
                    break;
                }
            }
            if (!diff) {
                return;
            }
        }

        let params: ApiParams = {
            search: this.state.search,
            species: val.join(","),
            sort: this.state.data.sort,
            direction: this.state.data.direction,
        };

        this.fetchData(params);
        this.setState({
            loading: true,
            search: this.state.search,
            data: {
                total_objects: 0,
                objects: [],
                sort: this.state.data.sort,
                direction: this.state.data.direction,
                page_size: 0,
                page_num: 0,
            },
            title:
                this.state.search === ""
                    ? props.title
                    : 'Search Results for "' + this.state.search + '"',
            selectedSpecies: val,
        });
    };

    renderDropVal = (selected: Array<MultiSelectOption>, options: any) => {
        if (selected.length === 0) {
            return "Filter by Species";
        }
    };

    componentWillReceiveProps(nextProps: ConnectedTableProps) {
        if (nextProps.search !== this.props.search) {
            let params: ApiParams = {
                search: nextProps.search,
            };
            this.fetchData(params, true, true);
            this.setState({
                loading: true,
                search: nextProps.search!,
                data: {
                    total_objects: 0,
                    objects: [],
                    sort: "",
                    direction: "",
                    page_size: 0,
                    page_num: 0,
                },
                selectedSpecies: nextProps.selectedSpecies || [],
                title:
                    nextProps.search === ""
                        ? nextProps.title
                        : 'Search Results for "' + nextProps.search + '"',
            });
        }
        if (nextProps.selectedSpecies && nextProps.selectedSpecies !== this.props.selectedSpecies) {
            this.onSpeciesSelected(nextProps.selectedSpecies, nextProps);
        }
    }

    componentDidMount() {
        if (this.unmounted === false) {
            // set print handlers to print full page
            window.onbeforeprint = () => {
                this.setState({ isPrinting: true });
            };
            window.onafterprint = () => {
                this.setState({ isPrinting: false });
            };
        }
    }

    componentWillUnmount() {
        // remove print handlers
        window.onbeforeprint = null;
        window.onafterprint = null;
        this.unmounted = true;
    }

    updateSize = () => {
        this.windowScrollerRef.updatePosition();
    };

    render() {
        // this will render the full table if passed as a prop to a <VTable>.
        // this is important for printing!
        const overscanAllIndicesGetter = ({ cellCount }: { cellCount: number }) => ({
            overscanStartIndex: 0,
            overscanStopIndex: cellCount - 1,
        });

        if (
            !this.props.showWhenEmpty &&
            !this.state.loading &&
            this.state.data.total_objects == 0
        ) {
            return null;
        }

        return (
            <div className="light-table">
                <h2 className={"table-header table-header-" + this.props.headerSize}>
                    {this.state.title + " (" + this.state.data.total_objects + " entries)"}
                </h2>
                <div
                    className={
                        this.props.showSpeciesFilter ? "search-section" : "search-section right"
                    }
                >
                    {this.props.showSpeciesFilter && (
                        <div className="filter">
                            <MultiSelect
                                onSelectedChanged={this.onSpeciesSelected}
                                options={this.state.speciesList}
                                selected={this.state.selectedSpecies}
                                isLoading={this.state.speciesLoading}
                                disableSearch={true}
                                hasSelectAll={false}
                                valueRenderer={this.renderDropVal}
                            />
                        </div>
                    )}
                    {this.props.showSearch && (
                        <Search
                            onSearch={this.handleSearch}
                            placeholder="Search table..."
                            initial={this.state.search}
                        />
                    )}
                </div>
                <div className="virtual-table">
                    <WindowScroller ref={(ref) => (this.windowScrollerRef = ref)}>
                        {({ height, isScrolling, onChildScroll, scrollTop }) => (
                            <AutoSizer disableHeight>
                                {({ width }) => (
                                    <VTable
                                        autoHeight
                                        height={height}
                                        isScrolling={isScrolling}
                                        onScroll={onChildScroll}
                                        scrollTop={scrollTop}
                                        width={
                                            width > this.state.tableWidth
                                                ? width
                                                : this.state.tableWidth
                                        }
                                        headerHeight={this.state.headerHeight}
                                        rowHeight={this.rowHeight}
                                        rowCount={this.state.data.objects.length}
                                        rowGetter={({ index }) => this.state.data.objects[index]}
                                        onHeaderClick={this.handleSort}
                                        noRowsRenderer={this.handleNoRows}
                                        overscanIndicesGetter={
                                            /* BUGFIX: when not printing, the VTable will
                                             * erroneously hide some rows. So, override this by
                                             * always rendering the full table. */
                                            overscanAllIndicesGetter
                                        }
                                    >
                                        {this.renderBody()}
                                    </VTable>
                                )}
                            </AutoSizer>
                        )}
                    </WindowScroller>
                    <div
                        className={this.state.displayFloatingImg ? "floating-image" : "hidden"}
                        style={this.state.style}
                    >
                        <div className="image-header">
                            <span className="header-text">{this.state.floatingImgName}</span>
                        </div>
                        <ImgWrapper className="" src={this.state.floatingImageSrc} />
                    </div>
                </div>
                <div className={this.state.loading ? "info-loading" : "hidden"}>
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            </div>
        );
    }
}

export default connector(Table);
