import * as React from "react";
import { Link } from "react-router-dom";
import { Classification } from "../../../config/Classification";
import "./PDBLink.scss";

type OptionalPDBLinkProps = {
    complex?: boolean;
};

export type PDBLinkProps = OptionalPDBLinkProps & {
    id: number;
    name: string;
    pdb: string;
    cell: string;
    onImgChange: (e: any) => void;
};

export class PDBLink extends React.Component<PDBLinkProps> {
    static defaultProps: OptionalPDBLinkProps = {
        complex: false,
    };

    private handleMouseEnter = (e: React.MouseEvent): void => {
        this.props.onImgChange({
            display: true,
            src: Classification.definitions.proteins.pdb_image(this.props.pdb),
            name: this.props.name,
            style: {
                top: e.clientY + 1,
                left: e.clientX + 1,
            },
        });
    };

    private handleMouseMove = (e: React.MouseEvent): void => {
        this.props.onImgChange({
            style: {
                top: e.clientY + 1,
                left: e.clientX + 1,
            },
        });
    };

    private handleMouseLeave = (e: React.MouseEvent): void => {
        this.props.onImgChange({ display: false });
    };

    render(): React.ReactNode {
        return (
            <div className="pdb-link">
                <div
                    className="name-link"
                    onMouseEnter={!this.props.complex ? this.handleMouseEnter : undefined}
                    onMouseMove={!this.props.complex ? this.handleMouseMove : undefined}
                    onMouseLeave={!this.props.complex ? this.handleMouseLeave : undefined}
                >
                    <Link
                        to={
                            this.props.complex
                                ? "/complex_structures/" + this.props.id
                                : "/proteins/" + this.props.id
                        }
                    >
                        {this.props.cell}
                    </Link>
                </div>
            </div>
        );
    }
}
