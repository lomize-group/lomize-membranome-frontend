import axios from "axios";
import "bootstrap";
import * as React from "react";
import { Link } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import * as appendQuery from "append-query";
import { Assets } from "../../config/Assets";
import { GenericObj } from "../../config/Types";
import { ComputationServer, Urls } from "../../config/Urls";
import { validEmail, validFilename } from "../../config/Util";
import { RootState } from "../../store";
import Popup from "./popup/Popup";
import Submitted from "../submitted/Submitted";
import "./TMDOCK.scss";
import ExternalLink from "../external_link/ExternalLink";

const mapStateToProps = (state: RootState) => ({
    currentUrl: state.currentUrl,
});

const connector = connect(mapStateToProps);

type TMDOCKProps = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer;
    submitted?: boolean;
};

type TMDOCKState = {
    loading: boolean;
    instructions: boolean;
    error: boolean;
    error_text: string;
    email: string;
    amino_acid_sequence: string;
    filename: string;
};

function getInitialState(): TMDOCKState {
    return {
        loading: false,
        instructions: false,
        error: false,
        error_text: "",
        email: "",
        amino_acid_sequence: "",
        filename: "",
    };
}

class TMDOCK extends React.Component<TMDOCKProps, TMDOCKState> {
    state: TMDOCKState = getInitialState();

    submit = (): void => {
        const { computationServer } = this.props;
        let errText: Array<string> = [];
        let errFlag = false;

        if (!this.state.amino_acid_sequence || this.state.amino_acid_sequence == "") {
            errFlag = true;
            errText.push("You must enter a valid amino acid sequence");
        }
        if (!validEmail(this.state.email)) {
            errFlag = true;
            errText.push("invalid email");
        }

        if (!validFilename(this.state.filename)) {
            errFlag = true;
            errText.push("filename must be alphanumeric");
        }

        this.setState({
            error: errFlag,
            error_text: errText.join("; "),
        });

        if (errFlag) {
            return;
        }

        let data: GenericObj = {
            sequence: this.state.amino_acid_sequence,
        };
        if (this.state.email !== "") {
            data.userEmail = this.state.email;
        }
        if (this.state.filename !== "") {
            data.filename = this.state.filename;
        }

        axios
            .post(Urls.computation_servers[computationServer] + "tmdock", data)
            .then((res) => {
                // push url for submitted page
                const urlParams = {
                    resultsUrl: res.data.resultsUrl || "",
                    waitingUrl: res.data.waitingUrl || "",
                    message: res.data.message,
                };
                let s =
                    this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === "/"
                        ? "submitted"
                        : "/submitted";
                const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
                this.props.currentUrl.history.push(submitUrl);
                this.setState({ loading: false });
            })
            .catch((err) => {
                this.setState({
                    error: true,
                    error_text: "An error occurred when submitting: " + JSON.stringify(err),
                    loading: false,
                });
            });
        this.setState({ loading: true });
    };

    handleAminoAcidChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        this.setState({ amino_acid_sequence: e.target.value });
    };

    handleFilenameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ filename: e.target.value });
    };

    submitAgain = (): void => {
        this.setState(getInitialState());
    };

    render(): React.ReactNode {
        const { computationServer } = this.props;
        if (this.state.loading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        if (this.props.submitted) {
            return (
                <div className="server">
                    <div className="server-header">TMDOCK Server</div>
                    <Submitted submitAgain={this.submitAgain} userEmail={this.state.email} />
                </div>
            );
        }

        const serverType = computationServer == "memprot" ? "AWS" : computationServer;
        const useOtherServer =
            computationServer == "memprot" ? (
                <div>
                    <strong>Having trouble submitting?</strong> Try out the{" "}
                    <Link to="/tmdock_cgopm">cgopm-based</Link> version
                </div>
            ) : (
                <div>
                    <strong>Having trouble submitting?</strong> Try out the{" "}
                    <Link to="/tmdock">AWS-based</Link> version
                </div>
            );

        return (
            <div className="server tmdock">
                <div className="server-header">TMDOCK Server ({serverType})</div>
                <div>
                    The TMDOCK web server predicts formation of dimeric complexes by single-spanning
                    transmembrane (TM) proteins. It calculates thermodynamic stabilities, 3D
                    structures, and spatial positions in membranes of parallel homodimers of TM
                    alpha-helices. Note that input amino acid sequence must be longer than TM helix,
                    unless user wants to look at homodimerization of shorter peptides. The location
                    of TM helix in amino acid sequence will be refined by the server automatically.
                    <br />
                    <span
                        className="expand-link"
                        onClick={() => this.setState({ instructions: !this.state.instructions })}
                    >
                        {this.state.instructions ? "Hide Instructions -" : "Show Instructions +"}
                    </span>
                    <br />
                    {this.state.instructions ? <Popup /> : ""}
                </div>
                <table className="table-form">
                    <thead className="table-header">
                        <tr>
                            <td colSpan={2}>Run TMDOCK predictions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="dark">
                            <td className="file" colSpan={2}>
                                Amino Acid Sequence (single-letter code) of Peptide
                                <br />
                                <textarea
                                    className="text-input"
                                    placeholder="Enter your text here"
                                    value={this.state.amino_acid_sequence}
                                    onChange={this.handleAminoAcidChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td className="instructions" colSpan={2}>
                                <div className="input-group email-input-div">
                                    <input
                                        type="text"
                                        className="search-bar form-control input-sm email-input"
                                        placeholder="Filename (optional)"
                                        value={this.state.filename}
                                        onChange={this.handleFilenameChange}
                                    />
                                </div>
                                <p className="filename-helper">
                                    Filename for the resulting pdb file if one is generated
                                </p>
                            </td>
                        </tr>
                        <tr className="dark">
                            <td className="instructions" colSpan={2}>
                                <div className="input-group email-input-div">
                                    <input
                                        type="text"
                                        className="search-bar form-control input-sm email-input"
                                        placeholder="Email (optional)"
                                        value={this.state.email}
                                        onChange={(e) =>
                                            this.setState({
                                                email: e.target.value,
                                                error: false,
                                                error_text: "",
                                            })
                                        }
                                    />
                                </div>
                                <p className="helper">
                                    Recommended: Calculations may take 15 minutes or more. You will
                                    be notified when they are completed.
                                </p>
                                <button
                                    title={"submit"}
                                    className={"submit-button"}
                                    disabled={this.state.loading}
                                    onClick={this.submit}
                                >
                                    Submit
                                </button>
                                <br />
                                <span className={!this.state.error ? "hidden" : "error-msg"}>
                                    {this.state.error_text}
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                {useOtherServer}
                View the{" "}
                <ExternalLink href="https://cggit.cc.lehigh.edu/biomembhub/tmdock_server_code">
                    source code
                </ExternalLink>
                .
            </div>
        );
    }
}

export default connector(TMDOCK);
