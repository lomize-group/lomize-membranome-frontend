import axios from "axios";
import "bootstrap";
import * as React from "react";
import { Link } from "react-router-dom";
import { connect, ConnectedProps } from "react-redux";
import * as appendQuery from "append-query";
import { Assets } from "../../config/Assets";
import { GenericObj } from "../../config/Types";
import { ComputationServer, Urls } from "../../config/Urls";
import { validEmail, validFilename } from "../../config/Util";
import { RootState } from "../../store";
import Popup from "./popup/Popup";
import Submitted from "../submitted/Submitted";
import "./TMMatch.scss";

const mapStateToProps = (state: RootState) => ({
    currentUrl: state.currentUrl,
});

const connector = connect(mapStateToProps);

type TMMatchProps = ConnectedProps<typeof connector> & {
    computationServer: ComputationServer,
    submitted?: boolean;
};

type TMMatchState = {
    loading: boolean;
    instructions: boolean;
    error: boolean;
    error_text: string;
    email: string;
    sequence1: string;
    sequence2: string;
    filename: string;
};

function getInitialState(): TMMatchState {
    return {
        loading: false,
        instructions: false,
        error: false,
        error_text: "",
        email: "",
        sequence1: "",
        sequence2: "",
        filename: "",
    };
}

class TMMatch extends React.Component<TMMatchProps, TMMatchState> {
    state: TMMatchState = getInitialState();

    submit = (): void => {
        const { computationServer } = this.props;
        let errText: Array<string> = [];
        let errFlag = false;

        if (!this.state.sequence1 || this.state.sequence1 == "") {
            errFlag = true;
            errText.push("You must enter a valid amino acid sequence");
        }
        if (!this.state.sequence2 || this.state.sequence2 == "") {
            errFlag = true;
            errText.push("You must enter a valid amino acid sequence");
        }
        if (!validEmail(this.state.email)) {
            errFlag = true;
            errText.push("invalid email");
        }

        if (!validFilename(this.state.filename)) {
            errFlag = true;
            errText.push("filename must be alphanumeric");
        }

        this.setState({
            error: errFlag,
            error_text: errText.join("; "),
        });

        if (errFlag) {
            return;
        }

        let data: GenericObj = {
            sequence1: this.state.sequence1,
            sequence2: this.state.sequence2,
        };
        if (this.state.email !== "") {
            data.userEmail = this.state.email;
        }
        if (this.state.filename !== "") {
            data.filename = this.state.filename;
        }

        /*
        console.log(JSON.stringify(data));
        return;
        */

        axios
            .post(Urls.computation_servers[computationServer] + "tmmatch", data)
            .then((res) => {
                // push url for submitted page
                const urlParams = {
                    resultsUrl: res.data.resultsUrl || "",
                    waitingUrl: res.data.waitingUrl || "",
                    message: res.data.message,
                };
                let s = this.props.currentUrl.url.charAt(this.props.currentUrl.url.length - 1) === '/' ? "submitted" : "/submitted";
                const submitUrl = appendQuery(this.props.currentUrl.url + s, urlParams);
                this.props.currentUrl.history.push(submitUrl);
                this.setState({ loading: false });
            })
            .catch((err) => {
                this.setState({
                    error: true,
                    error_text: "An error occurred when submitting: " + JSON.stringify(err),
                    loading: false,
                });
            });
        this.setState({ loading: true });
    };

    handleAminoAcidChange = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
        // appease typescript with these if statements
        if (e.target.name === "sequence1") {
            this.setState({ [e.target.name]: e.target.value });
        }
        if (e.target.name === "sequence2") {
            this.setState({ [e.target.name]: e.target.value });
        }
    };

    handleFilenameChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ filename: e.target.value });
    };

    submitAgain = (): void => {
        this.setState(getInitialState());
    };

    render(): React.ReactNode {
        const { computationServer } = this.props;
        if (this.state.loading) {
            return (
                <div className="info-loading">
                    <img className="loading-image" src={Assets.images.loading} />
                </div>
            );
        }

        if (this.props.submitted) {
            return (
                <div className="server">
                    <div className="server-header">TMMatch Server</div>
                    <Submitted submitAgain={this.submitAgain} userEmail={this.state.email} />
                </div>
            );
        }

        const serverType = computationServer == "memprot"
            ? "AWS"
            : computationServer;
        const useOtherServer = computationServer == "memprot"
            ? <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/tmmatch_cgopm">cgopm-based</Link> version
            </div>
            : <div>
                <strong>Having trouble submitting?</strong> Try out the{" "}
                <Link to="/tmmatch">AWS-based</Link> version
            </div>;

        return (
            <div className="server tmmatch">
                <div className="server-header">TMMatch Server ({serverType})</div>
                <div>
                    The TMMatch web server predicts the existence of stable parallel or antiparallel
                    heterodimers formed by two single-spanning (bitopic) transmembrane (TM) proteins
                    and generates their 3D models. It calculates thermodynamic stabilities, atomic
                    structures, and spatial orientations in membranes of TM alpha-helical
                    heterodimers using either full-length amino acid sequences of two proteins of
                    just sequences of their TM domains. The location of TM helix in amino acid
                    sequence will be refined by the server automatically.
                    <br />
                    <span
                        className="expand-link"
                        onClick={() => this.setState({ instructions: !this.state.instructions })}
                    >
                        {this.state.instructions ? "Hide Instructions -" : "Show Instructions +"}
                    </span>
                    <br />
                    {this.state.instructions ? <Popup /> : ""}
                </div>
                <table className="table-form">
                    <thead className="table-header">
                        <tr>
                            <td colSpan={2}>Run TMMatch predictions</td>
                        </tr>
                    </thead>
                    <tbody>
                        <tr className="dark">
                            <td className="file" colSpan={2}>
                                Amino acid sequence (single-letter code) of first protein in the dimer
                                <br />
                                <textarea
                                    className="text-input"
                                    placeholder="Enter your text here"
                                    name="sequence1"
                                    value={this.state.sequence1}
                                    onChange={this.handleAminoAcidChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td className="file" colSpan={2}>
                                Amino acid sequence (single-letter code) of second protein
                                <br />
                                <textarea
                                    className="text-input"
                                    placeholder="Enter your text here"
                                    name="sequence2"
                                    value={this.state.sequence2}
                                    onChange={this.handleAminoAcidChange}
                                />
                            </td>
                        </tr>
                        <tr className="dark">
                            <td className="instructions" colSpan={2}>
                                <div className="input-group email-input-div">
                                    <input
                                        type="text"
                                        className="search-bar form-control input-sm email-input"
                                        placeholder="Filename (optional)"
                                        value={this.state.filename}
                                        onChange={this.handleFilenameChange}
                                    />
                                </div>
                                <p className="filename-helper">
                                    Filename for the resulting pdb file if one is generated
                                </p>
                            </td>
                        </tr>
                        <tr className="dark">
                            <td className="instructions" colSpan={2}>
                                <div className="input-group email-input-div">
                                    <input
                                        type="text"
                                        className="search-bar form-control input-sm email-input"
                                        placeholder="Email (optional)"
                                        value={this.state.email}
                                        onChange={(e) =>
                                            this.setState({
                                                email: e.target.value,
                                                error: false,
                                                error_text: "",
                                            })
                                        }
                                    />
                                </div>
                                <p className="helper">
                                    Recommended: Calculations may take 15 minutes or more. You will
                                    be notified when they are completed.
                                </p>
                                <button
                                    title={"submit"}
                                    className={"submit-button"}
                                    disabled={this.state.loading}
                                    onClick={this.submit}
                                >
                                    Submit
                                </button>
                                <br />
                                <span className={!this.state.error ? "hidden" : "error-msg"}>
                                    {this.state.error_text}
                                </span>
                            </td>
                        </tr>
                    </tbody>
                </table>
                {useOtherServer}
            </div>
        );
    }
}

export default connector(TMMatch);
