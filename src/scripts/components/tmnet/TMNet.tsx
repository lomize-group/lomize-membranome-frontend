import * as appendQuery from "append-query";
import axios from "axios";
import * as React from "react";
import { connect, ConnectedProps } from "react-redux";
import { Link } from "react-router-dom";
import { Assets } from "../../config/Assets";
import { Classification } from "../../config/Classification";
import { CType, TableColumn } from "../../config/Types";
import { Urls } from "../../config/Urls";
import { RootState } from "../../store";
import Search from "../search/Search";
import Table from "../table/Table";
import Selection from "./selection/Selection";
import Submitted from "./submitted/Submitted";
import "./TMNet.scss";
import { RelatedProtein, SelectedProtein, Species, TMNetResults } from "./Types";

function mapStateToProps(state: RootState) {
    return {
        currentUrl: state.currentUrl,
    };
}

const connector = connect(mapStateToProps);

type TMNetProps = ConnectedProps<typeof connector> & {
    ctype: CType;
    id?: string;
    submitted?: boolean;
};

type TMNetState = {
    selected: Array<SelectedProtein>;
    loadAll: boolean;
    results: TMNetResults;
    species: Array<Species>;
    speciesLoading: boolean;
    selectedSpecies?: Species;
    search: string;
};

function getInitialState(): TMNetState {
    return {
        selected: [],
        loadAll: false,
        species: [],
        speciesLoading: true,
        selectedSpecies: undefined,
        results: {
            interactions: {
                direct: [],
                indirect: [],
            },
            complexes: [],
            pathways: [],
        },
        search: "",
    };
}

/**
 * TMNet page component
 * Renders basic layout and sub-components based on props provided
 */
class TMNet extends React.Component<TMNetProps, TMNetState> {
    // table columns for selection <Table> component
    private columns: TableColumn[] = [
        {
            Header: () => (
                <div className="header-cell" title="Protein Name">
                    <div>Protein Name</div>
                </div>
            ),
            id: "name",
            flexGrow: 2,
            minWidth: 135,
            Cell: (row: any) => (
                <div className="cell">
                    <div>{row.name}</div>
                </div>
            ),
        },
        {
            Header: () => (
                <div className="header-cell" title="PDB ID">
                    <div>Protein ID</div>
                </div>
            ),
            width: 100,
            id: "pdbid",
            Cell: (row: any) => (
                <div className="cell">
                    <div>{row.pdbid}</div>
                </div>
            ),
        },
        {
            Header: () => (
                <div className="header-cell">
                    <div>Choose Proteins</div>
                </div>
            ),
            Cell: (row: any) => (
                <div className="cell">
                    <div>
                        <span
                            className="link-text"
                            onClick={() => this.selectProtein({
                                id: row.id,
                                name: row.name,
                                pdbid: row.pdbid,
                                related_proteins: row.related_proteins,
                            })}
                        >
                            select
                        </span>
                    </div>
                </div>
            ),
            id: "",
            width: 100,
        },
    ];

    constructor(props: TMNetProps) {
        super(props);
        this.state = getInitialState();
        this.fetchSpecies();
    }

    private fetchSpecies = async () => {
        if (!this.state.speciesLoading) {
            this.setState({ speciesLoading: true });
        }
        const { data } = await axios.get(
            Urls.baseUrl + Classification.definitions.species.api.route
        );
        const species: Species[] = data.objects.map(
            (spec: any): Species => ({ name: spec.name, id: spec.id.toString() })
        );

        this.setState({
            species,
            selectedSpecies: species[0],
            speciesLoading: false,
        });
    };

    private reset = (): void => {
        this.setState(getInitialState());
        this.fetchSpecies();
    };

    /**
     * Add a protein to the list of selected proteins
     * @param id: id of protien to add
     * @param name: name of protien to add
     * @param pdbid: pdbid of protien to add
     */
    private selectProtein = (protein: SelectedProtein): void => {
        if (this.state.selected.some((item) => item.id === protein.id)) {
            return;
        }

        this.setState({
            selected: [...this.state.selected, protein],
        });
    };

    /**
     * Remove a protein from the list of selected proteins.
     * @param id: id of the protein to remove.
     */
    private removeProtein = (id: number): void => {
        const selected = this.state.selected.filter((item) => item.id !== id);
        this.setState({ selected });
    };

    private renderSelectedProteins() {
        console.log("state:")
        console.log(this.state)
        if (this.state.selected.length === 0) {
            return <div>No proteins selected.</div>;
        }

        let items = [];
        for (let i = 0; i < this.state.selected.length; ++i) {
            const prot = this.state.selected[i];
            let interactions;

            if (prot.related_proteins !== undefined) {
                if (prot.related_proteins.length !== 0) {
                    interactions = prot.related_proteins.map((protein: RelatedProtein) =>
                        <>
                            <Link
                                target="_blank"
                                key={protein.related_protein_id}
                                to={"/proteins/" + protein.related_protein_id}
                            >
                                {protein.related_protein_name_cache}
                            </Link>
                            {", "}
                        </>
                    )
                } else {
                    interactions = <>No interactions</>
                }
            } else {
                interactions = undefined;
            }

            items.push(
                <li key={i}>
                    {prot.name} ({prot.pdbid})
                    {!this.props.submitted && (
                        <span>
                            {" "}
                            (
                            <span className="link-text" onClick={() => this.removeProtein(prot.id)}>
                                remove
                            </span>
                            )
                        </span>
                    )}
                    <br />
                    {interactions}
                </li>
            );
        }

        return <ol>{items}</ol>;
    }

    private renderAllProteins = () => {
        if (!this.state.selectedSpecies) {
            return null;
        }

        if (this.state.loadAll) {
            // render full proteins table
            return (
                <Selection
                    tableColumns={this.columns}
                    species={this.state.selectedSpecies}
                    ctype="proteins"
                />
            );
        }

        // render search bar without loading proteins table until search
        return (
            <div className="tmnet-search">
                <div className="search-section">
                    <Search
                        onSearch={(val) => this.setState({ search: val })}
                        placeholder={`Search for proteins of species "${this.state.selectedSpecies.name}"`}
                    />
                </div>
                {!this.state.loadAll && (
                    <span className="load-all-button">
                        <span
                            className="link-text"
                            onClick={() => this.setState({ loadAll: true })}
                        >
                            <strong>Show all</strong>
                        </span>
                    </span>
                )}
                {this.state.search && (
                    <Table
                        title={"Search results for " + this.state.search}
                        columns={this.columns}
                        showSpeciesFilter={false}
                        showSearch={false}
                        searchOptions={{ search_separator: "," }}
                        url={Urls.baseUrl + "/proteins"}
                        rowHeight={40}
                        search={this.state.search}
                        selectedSpecies={[this.state.selectedSpecies.id]}
                        redirect={false}
                    />
                )}
            </div>
        );
    };

    private renderSelectionArea() {
        if (!this.state.selectedSpecies) {
            return <img src={Assets.images.loading} alt="Loading" />;
        }

        if (this.props.submitted) {
            return <Submitted setSelected={(selected) => this.setState({ selected })} />;
        }

        return (
            <>
                <hr />
                <div className="nav-links">
                    {this.renderNavLink("/1tmnet", "Proteins")}
                    {this.renderNavLink("/1tmnet/protein_superfamilies", "Family selection")}
                    {this.renderNavLink("/1tmnet/protein_localizations", "Membrane selection")}
                </div>
                <hr />
                {this.renderSpeciesPicker()}
                {Classification.definitions[this.props.ctype].child ? (
                    <Selection
                        list
                        tableColumns={this.columns}
                        species={this.state.selectedSpecies}
                        ctype={this.props.ctype}
                        id={this.props.id}
                    />
                ) : (
                    this.renderAllProteins()
                )}
            </>
        );
    }

    private renderSpeciesPicker() {
        if (this.state.speciesLoading) {
            return <img src={Assets.images.loading_small} alt="Loading" />;
        }

        let options: JSX.Element[] = [];
        for (let i = 0; i < this.state.species.length; ++i) {
            const s = this.state.species[i];
            options.push(
                <option key={i} value={i}>
                    {s.name}
                </option>
            );
        }

        const disabled = this.state.selected.length > 0;
        return (
            <div>
                {disabled && (
                    <div>
                        {" "}
                        <i>Clear selection to change species.</i>
                    </div>
                )}
                <label htmlFor="selectedSpecies">
                    <b>Species:</b>
                </label>{" "}
                <select
                    name="selectedSpecies"
                    id="selectedSpecies"
                    disabled={disabled}
                    className={disabled ? "disabled-button" : ""}
                    onChange={(e: any) =>
                        this.setState({ selectedSpecies: this.state.species[e.target.value] })
                    }
                >
                    {options}
                </select>
            </div>
        );
    }

    /**
     * Render a navigation <Link> element.
     * Does not render as a link if the route is already loaded.
     * @param to route to navigate to
     * @param text the text of the link displayed on screen
     */
    private renderNavLink(to: string, text: string) {
        let inner = <strong>{text}</strong>;
        if (to !== this.props.currentUrl.url) {
            // onclick resets loadAll to false so one can navigate back to "search proteins" page
            // without loading the entire database of proteins
            inner = (
                <Link to={to} onClick={() => this.setState({ loadAll: false })}>
                    {inner}
                </Link>
            );
        }
        return <div>{inner}</div>;
    }

    /**
     * Render "submit" link if not on the submitted page, otherwise render
     * a "submit again" link.
     */
    private renderSubmitLink = () => {
        if (this.props.submitted) {
            return (
                <Link to="/1tmnet" onClick={this.reset}>
                    <button>Submit again</button>
                </Link>
            );
        }

        if (!this.state.selectedSpecies) {
            return null;
        }

        const params = {
            species: this.state.selectedSpecies.id,
            protein_ids: this.state.selected.map((item) => item.id),
        };

        return (
            <Link to={appendQuery("/1tmnet/submitted", params)}>
                <button
                    disabled={this.state.selected.length < 2}
                    className={this.state.selected.length < 2 ? "disabled-button" : ""}
                >
                    Submit
                </button>
            </Link>
        );
    };

    render() {
        return (
            <div className="tmnet">
                <div className="page-header">1TMnet {this.props.submitted && " Results"}</div>
                <p className="tmnet-description">
                    1TMnet allows identifying structural and functional associations between bitopic
                    proteins from Membranome based on their interactions, common complexes and
                    pathways. Please select a single species and several proteins of interest for
                    analyzing how they interact with each other in protein complexes and pathways.
                </p>
                <p>
                    Proteins can be selected
                    <ol>
                        <li>
                            by using search for text (for example, phosphatase) or Uniprot codes
                            (for example, P08575 or PTPRC_HUMAN),
                        </li>
                        <li>by selecting proteins that belong to certain families, or</li>
                        <li>by selecting proteins that are located in certain membranes.</li>
                    </ol>
                    <u>An example</u><br />
                    One can click at "Family selection" below and select first superfamily in the
                    list ("Protein-tyrosine phosphatase"). A table with proteins will appear. Then,
                    one can click "select" for the first six proteins in the list. The list of
                    selected proteins will appear under "Selected proteins". After submitting the
                    task, 1TMnet will generate a set of interactions between the selected proteins,
                    their complexes and biological pathways included to the Membranome database. The
                    list includes indirect interactions of proteins A and B through an intermediate
                    protein X.
                </p>
                <div className="selected-proteins">
                    <b>Select proteins</b>
                    <br />
                    {this.renderSelectedProteins()}
                    {this.renderSubmitLink()}
                </div>
                {this.renderSelectionArea()}
            </div>
        );
    }
}

export default connector(TMNet);
