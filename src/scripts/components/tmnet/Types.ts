import { ElementsDefinition } from "cytoscape";

export type RelatedProtein = {
    related_protein_id: number;
    related_protein_name_cache: string;
};

export type SelectedProtein = {
    id: number;
    name: string;
    pdbid: string;
    related_proteins: RelatedProtein[] | undefined;
};

export type Species = {
    id: string;
    name: string;
};

export type DirectInteraction = [SelectedProtein, SelectedProtein];

export type IndirectInteraction = {
    related_protein: SelectedProtein;
    proteins: [SelectedProtein, SelectedProtein];
};

export type InteractionResults = {
    direct: Array<DirectInteraction>;
    indirect: Array<IndirectInteraction>;
};

export type TMNetResults = {
    interactions: InteractionResults;
    complexes: Array<any>;
    pathways: Array<any>;
};

export type GraphElements = {
    direct?: ElementsDefinition;
    indirect?: ElementsDefinition;
};
