import * as appendQuery from "append-query";
import * as React from "react";
import { Link } from "react-router-dom";
import { Assets } from "../../../config/Assets";
import { Classification } from "../../../config/Classification";
import { CType, TableColumn } from "../../../config/Types";
import { Urls } from "../../../config/Urls";
import { capitalize, hasClassificationItems } from "../../../config/Util";
import { useClassificationInfo } from "../../../hooks";
import Info from "../../info/Info";
import List from "../../list/List";
import { ListItemProps } from "../../list/list_item/ListItem";
import Table from "../../table/Table";
import { Species } from "../Types";

// List item renderer callback used to generate custom list items for List component in TMNet.
function renderListItem(data: ListItemProps) {
    return (
        <span>
            {data.subclasses && data.subclasses > 0 ? (
                <Link to={`/1tmnet/${data.ctype}/${data.id}`}>{data.name}</Link>
            ) : (
                data.name
            )}{" "}
            <span>
                ({data.subclasses}{" "}
                {Classification.definitions[Classification.definitions[data.ctype].child!].name})
            </span>
        </span>
    );
}

export type SelectionProps = {
    ctype: CType;
    id?: string;
    tableColumns?: TableColumn[];
    list?: boolean;
    species: Species;
};

/**
 * TMNet protein selection area.
 * Renders Info, List, and Table components where appropriate to allow selection of proteins.
 */
const Selection: React.FC<SelectionProps> = ({ ctype, id, species, tableColumns, list = true }) => {
    const cdef = Classification.definitions[ctype];
    const childDef = cdef.child ? Classification.definitions[cdef.child] : undefined;

    // fetch data
    const data = useClassificationInfo({ id, ctype, stopAt: "protein_superfamilies" });

    // determine which displays to use
    const { hasInfo, hasList, hasTable } = hasClassificationItems(ctype, id);

    let items: JSX.Element[] = [];

    // add info component if applicable
    if (hasInfo) {
        if (data) {
            items.push(
                <div key={items.length}>
                    <br />
                    <Info {...data.info} />
                </div>
            );
        } else {
            items.push(<img key={items.length} src={Assets.images.loading} alt="Loading" />);
        }
    }

    // add list component if applicable and requested by props
    if (list && hasList) {
        const childType = cdef.child!;
        const listHeader = id ? childDef!.name : cdef.name;

        let url = Urls.baseUrl + cdef.api.route;
        if (id) {
            url += "/" + id + "/" + childType;
        }
        url = appendQuery(url, { species: species.id });

        items.push(
            <div key={items.length} className="list-section">
                <div className="list-header">{capitalize(listHeader)}:</div>
                <List
                    url={url}
                    ctype={id ? childType : ctype}
                    expandByDefault={true}
                    renderListItem={renderListItem}
                />
            </div>
        );
    }

    // add table component if applicable and if table columns were given by props
    if (tableColumns && hasTable) {
        let url = Urls.baseUrl + cdef.api.route;
        if (id) {
            url += "/" + id + "/proteins";
        }
        url = appendQuery(url, { species: species.id });

        items.push(
            <Table
                key={items.length}
                title={"Proteins"}
                url={url}
                columns={tableColumns}
                rowHeight={40}
                redirect={false}
                showSpeciesFilter={false}
                selectedSpecies={[species.id]}
                pageSize={200}
                searchOptions={{
                    search_separator: ",",
                    search_by: "limited",
                }}
            />
        );
    }

    return <div className="tmnet-selection">{items}</div>;
};

export default Selection;
