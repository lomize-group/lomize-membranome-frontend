import { EdgeDefinition, ElementsDefinition, NodeDefinition } from "cytoscape";
import * as React from "react";
import { Link } from "react-router-dom";
import { Classification } from "../../../config/Classification";
import { GenericObj } from "../../../config/Types";
import { sortStrings } from "../../../config/Util";
import ExternalLink from "../../external_link/ExternalLink";
import InfoTable, { InfoTableColumn } from "../../info_table/InfoTable";
import {
    DirectInteraction,
    GraphElements,
    IndirectInteraction,
    SelectedProtein,
    TMNetResults,
} from "../Types";

export function renderTableRow(
    right: React.ReactNode,
    left: React.ReactNode,
    attr: string
): JSX.Element {
    return (
        <tr className={attr}>
            <td className="right">
                <b>{right}</b>
            </td>
            <td className="left">{left}</td>
        </tr>
    );
}

export function renderComplexes(complexResults: Array<GenericObj>) {
    if (complexResults.length === 0) {
        return (
            <div>
                <h2 className="section-header">Complexes (0 results)</h2>
                No complexes containing the selected proteins.
            </div>
        );
    }

    let complexes: JSX.Element[] = [];
    for (let i = 0; i < complexResults.length; ++i) {
        const complex = complexResults[i];

        let selectedProteins = [];
        for (let j = 0; j < complex.selected_proteins.length; ++j) {
            const prot = complex.selected_proteins[j];
            selectedProteins.push(
                <div key={j}>
                    <Link to={"/proteins/" + prot.id}>{prot.name}</Link> (
                    <ExternalLink
                        href={Classification.definitions.proteins.uniprotcode.uniprot(prot.pdbid)}
                    >
                        {prot.pdbid}
                    </ExternalLink>
                    ){j < complex.selected_proteins.length - 1 && ", "}
                </div>
            );
        }

        let additionalProteins = [];
        for (let j = 0; j < complex.additional_proteins.length; ++j) {
            const prot = complex.additional_proteins[j];
            additionalProteins.push(
                <div key={j}>
                    <Link to={"/proteins/" + prot.id}>{prot.name}</Link> (
                    <ExternalLink
                        href={Classification.definitions.proteins.uniprotcode.uniprot(prot.pdbid)}
                    >
                        {prot.pdbid}
                    </ExternalLink>
                    ){j < complex.additional_proteins.length - 1 && ", "}
                </div>
            );
        }

        let membranes = [];
        for (let j = 0; j < complex.membranes.length; ++j) {
            const mem = complex.membranes[j];
            membranes.push(
                <span key={j}>
                    <Link to={"/complex_localizations/" + mem.id}>{mem.membrane_name_cache}</Link>
                    {j < complex.membranes.length - 1 && ", "}
                </span>
            );
        }

        complexes.push(
            <div key={i} className="info-table-container">
                <table>
                    <tbody>
                        <tr className="dark">
                            <th colSpan={2}>
                                {i + 1}. {complex.complex_structure.name}
                            </th>
                        </tr>
                        {renderTableRow(
                            "Complex",
                            <Link to={"/complex_structures/" + complex.complex_structure.id}>
                                {complex.complex_structure.name}
                            </Link>,
                            "dark"
                        )}
                        {renderTableRow("Selected bitopic proteins", selectedProteins, "dark")}
                        {renderTableRow(
                            "Additional bitopic proteins",
                            additionalProteins.length > 0 ? additionalProteins : "none",
                            "light"
                        )}
                        {renderTableRow("Localizations", membranes, "dark")}
                    </tbody>
                </table>
            </div>
        );
    }

    return (
        <div>
            <h2 className="section-header">Complexes ({complexResults.length} results)</h2>
            {complexes}
        </div>
    );
}

export function renderPathways(pathwayResults: Array<GenericObj>) {
    if (pathwayResults.length === 0) {
        return (
            <div>
                <h2 className="section-header">Pathways (0 results)</h2>
                No pathways containing the selected proteins.
            </div>
        );
    }

    let pathways: JSX.Element[] = [];
    for (let i = 0; i < pathwayResults.length; ++i) {
        const pathway = pathwayResults[i];

        let selectedProteins: JSX.Element[] = [];
        for (let j = 0; j < pathway.selected_proteins.length; ++j) {
            const prot = pathway.selected_proteins[j];
            selectedProteins.push(
                <div key={j}>
                    <Link to={"/proteins/" + prot.id}>{prot.name}</Link> (
                    <ExternalLink
                        href={Classification.definitions.proteins.uniprotcode.uniprot(prot.pdbid)}
                    >
                        {prot.pdbid}
                    </ExternalLink>
                    ){j < pathway.selected_proteins.length - 1 && ", "}
                </div>
            );
        }

        let additionalProteins: JSX.Element[] = [];
        for (let j = 0; j < pathway.additional_proteins.length; ++j) {
            const prot = pathway.additional_proteins[j];
            additionalProteins.push(
                <div key={j}>
                    <Link to={"/proteins/" + prot.id}>{prot.name}</Link> (
                    <ExternalLink
                        href={Classification.definitions.proteins.uniprotcode.uniprot(prot.pdbid)}
                    >
                        {prot.pdbid}
                    </ExternalLink>
                    ){j < pathway.additional_proteins.length - 1 && ", "}
                </div>
            );
        }

        pathways.push(
            <div key={i} className="info-table-container">
                <table>
                    <tbody>
                        <tr className="dark">
                            <th colSpan={2}>
                                {i + 1}. {pathway.pathway.name}
                            </th>
                        </tr>
                        {renderTableRow(
                            "Pathway",
                            <span>
                                <Link to={"/pathways/" + pathway.pathway.id}>
                                    {pathway.pathway.name}
                                </Link>{" "}
                                (
                                <ExternalLink prepend_https href={pathway.pathway.link}>
                                    {pathway.pathway.dbname}
                                </ExternalLink>
                                )
                            </span>,
                            "light"
                        )}
                        {renderTableRow("Selected bitopic proteins", selectedProteins, "dark")}
                        {renderTableRow(
                            "Additional bitopic proteins",
                            additionalProteins.length > 0 ? additionalProteins : "none",
                            "light"
                        )}
                    </tbody>
                </table>
            </div>
        );
    }

    return (
        <div>
            <h2 className="section-header">Pathways ({pathwayResults.length} results)</h2>
            {pathways}
        </div>
    );
}

function renderInteractionCellContent(prot: SelectedProtein) {
    return (
        <>
            <Link to={"/proteins/" + prot.id}>{prot.name}</Link> (
            <ExternalLink
                href={Classification.definitions.proteins.uniprotcode.uniprot(prot.pdbid)}
            >
                {prot.pdbid}
            </ExternalLink>
            )
        </>
    );
}

const directInteractionColumns: readonly InfoTableColumn[] = [
    {
        header: "Protein 1",
        renderCell: (pair: any) => renderInteractionCellContent(pair[0]),
        sortKey: (pair: SelectedProtein[]) => pair[0].name,
        defaultSort: true,
    },
    {
        header: "Protein 2",
        renderCell: (pair: any) => renderInteractionCellContent(pair[1]),
        sortKey: (pair: SelectedProtein[]) => pair[1].name,
    },
] as const;

export function renderDirectInteractions(directInteractions: Array<DirectInteraction>) {
    if (directInteractions.length === 0) {
        return <div>No direct interactions between the selected proteins.</div>;
    }

    return (
        <div className="direct-interactions">
            <InfoTable
                title={`Direct protein-protein Interactions (${directInteractions.length} results)`}
                showHeaders={true}
                columns={directInteractionColumns}
                data={directInteractions}
            />
        </div>
    );
}

const indirectInteractionColumns: readonly InfoTableColumn[] = [
    {
        header: "Protein 1",
        renderCell: (interaction: any) => renderInteractionCellContent(interaction.proteins[0]),
        sortKey: (interaction: any) => interaction.proteins[0].name,
        defaultSort: true,
    },
    {
        header: "Through",
        renderCell: (interaction: any) => renderInteractionCellContent(interaction.related_protein),
        sortKey: (interaction: any) => interaction.related_protein.name,
    },
    {
        header: "Protein 2",
        renderCell: (interaction: any) => renderInteractionCellContent(interaction.proteins[1]),
        sortKey: (interaction: any) => interaction.proteins[1].name,
    },
] as const;

export function renderIndirectInteractions(indirectInteractions: Array<IndirectInteraction>) {
    if (indirectInteractions.length === 0) {
        return <div>No indirect interactions between the selected proteins.</div>;
    }

    return (
        <div className="indirect-interactions">
            <InfoTable
                title={`Indirect protein-protein Interactions (${indirectInteractions.length} results)`}
                showHeaders={true}
                columns={indirectInteractionColumns}
                data={indirectInteractions}
            />
        </div>
    );
}

export function getResults(data: any): TMNetResults {
    const interactions = {
        direct: data.direct_interactions.map((pair: SelectedProtein[]) =>
            sortStrings(pair, (p) => p.name)
        ),
        indirect: data.indirect_interactions,
    };

    let complexes = sortStrings<GenericObj>(data.complexes, (item) => item.complex_structure.name);
    for (let i = 0; i < complexes.length; ++i) {
        complexes[i].selected_proteins = sortStrings(complexes[i].selected_proteins, "name");
        complexes[i].additional_proteins = sortStrings(complexes[i].additional_proteins, "name");
        complexes[i].membranes = sortStrings(complexes[i].membranes, "membrane_name_cache");
    }

    let pathways = sortStrings<GenericObj>(data.pathways, (item) => item.pathway.name);
    for (let i = 0; i < pathways.length; ++i) {
        pathways[i].selected_proteins = sortStrings(pathways[i].selected_proteins, "name");
        pathways[i].additional_proteins = sortStrings(pathways[i].additional_proteins, "name");
    }

    return {
        interactions,
        complexes,
        pathways,
    };
}

export function getGraphElements(results: TMNetResults): GraphElements {
    // get direct interaction graph nodes/edges
    let direct: ElementsDefinition | undefined = undefined;
    if (results.interactions.direct.length > 0) {
        let nodes: NodeDefinition[] = [];
        let edges: EdgeDefinition[] = [];
        for (let i = 0; i < results.interactions.direct.length; ++i) {
            const [p1, p2] = results.interactions.direct[i];
            const node1: NodeDefinition = {
                data: {
                    id: p1.id.toString(),
                    name: p1.name,
                    label: p1.pdbid,
                },
                selected: false,
                selectable: false,
            };
            const node2: NodeDefinition = {
                data: {
                    id: p2.id.toString(),
                    name: p2.name,
                    label: p2.pdbid,
                },
                selected: false,
                selectable: false,
            };
            const edge: EdgeDefinition = {
                data: {
                    id: `e${i}`,
                    source: node1.data.id!,
                    target: node2.data.id!,
                },
                selected: false,
                selectable: false,
            };
            nodes.push(node1, node2);
            edges.push(edge);
        }
        direct = { nodes, edges };
    }

    // get indirect interaction graph nodes/edges
    let indirect: ElementsDefinition | undefined = undefined;
    if (results.interactions.indirect.length > 0) {
        let edges = [];
        let nodes = [];
        for (let i = 0; i < results.interactions.indirect.length; ++i) {
            const [p1, p2] = results.interactions.indirect[i].proteins;
            const node1 = {
                data: {
                    id: p1.id.toString(),
                    name: p1.name,
                    label: p1.pdbid,
                },
                selected: false,
            };
            const node2 = {
                data: {
                    id: p2.id.toString(),
                    name: p2.name,
                    label: p2.pdbid,
                },
                selected: false,
            };
            const edge = {
                data: {
                    id: `e${i}`,
                    source: node1.data.id,
                    target: node2.data.id,
                },
                selected: false,
            };
            nodes.push(node1, node2);
            edges.push(edge);
        }
        indirect = { nodes, edges };
    }

    return { direct, indirect };
}
