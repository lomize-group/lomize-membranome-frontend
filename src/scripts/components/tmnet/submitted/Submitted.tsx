import * as appendQuery from "append-query";
import axios from "axios";
import { CytoscapeOptions } from "cytoscape";
import * as React from "react";
import { Assets } from "../../../config/Assets";
import { Urls } from "../../../config/Urls";
import { useStore } from "../../../hooks";
import Graph from "../../graph/Graph";
import { GraphElements, SelectedProtein, TMNetResults } from "../Types";
import {
    getGraphElements,
    getResults,
    renderComplexes,
    renderDirectInteractions,
    renderIndirectInteractions,
    renderPathways,
} from "./Helpers";

// basic graph options to append with elements
const graphOpts: CytoscapeOptions = {
    style: [
        {
            selector: "node",
            style: {
                label: "data(label)",
                backgroundColor: "#9fbbc6",
                width: 65,
                height: 40,
                "text-valign": "center",
                "text-halign": "center",
                "font-weight": "bold",
                "font-size": 14,
                shape: "roundrectangle",
            },
        },
        {
            selector: "edge",
            style: {
                width: 5,
                opacity: 0.65,
                "line-color": "#8fb0bd",
            },
        },
    ],
    layout: {
        name: "breadthfirst",
    },
    userZoomingEnabled: false,
    userPanningEnabled: false,
};

type SubmittedProps = {
    setSelected: (vals: SelectedProtein[]) => void;
};

type SubmittedState = {
    results?: TMNetResults;
    elements?: GraphElements;
};

/**
 * TMNet Submitted Page
 * Fetches results from url query parameters and renders as tables and graphs.
 */
const Submitted: React.FC<SubmittedProps> = (props) => {
    // data to render
    const [state, setState] = React.useState<SubmittedState>({});

    // get query params
    const query = useStore((state) => state.currentUrl.query);
    const proteinIds: string[] = query["protein_ids[]"] || [];
    const speciesId: string | undefined = query.species;

    // fetch data
    React.useEffect(() => {
        if (!state.results && proteinIds.length > 1 && speciesId) {
            const fetchData = async () => {
                const urlParams = {
                    species: speciesId,
                    protein_ids: proteinIds,
                };

                const fetchUrl = appendQuery(Urls.baseUrl + "/1tmnet", urlParams);
                let { data } = await axios.get(fetchUrl);

                const results = getResults(data);
                const elements = getGraphElements(results);

                setState({ results, elements });
                props.setSelected(data.selected);
            };
            fetchData();
        }
    }, [state.results, proteinIds, proteinIds.length, speciesId]);

    // on mount/unmount remove all selected proteins
    React.useEffect(() => () => props.setSelected([]), []);

    // error msg, can't do anything if unspecified species/protein ids
    if (!speciesId || proteinIds.length < 2) {
        return <div>Invalid species id and/or protein ids specified in query.</div>;
    }

    // loading image if results not in
    if (!state.results) {
        return <img src={Assets.images.loading} alt="Loading" />;
    }

    return (
        <div className="submitted">
            <hr />
            <div>
                <h2 className="section-header">
                    Interactions (
                    {state.results.interactions.direct.length +
                        state.results.interactions.indirect.length}{" "}
                    results)
                </h2>
                {renderDirectInteractions(state.results.interactions.direct)}
                {state.elements?.direct && (
                    <div className="graph-container">
                        <Graph
                            title="Visualization of Direct Protein-Protein Interactions"
                            cytoscapeOptions={{ ...graphOpts, elements: state.elements.direct }}
                        />
                    </div>
                )}
                {renderIndirectInteractions(state.results.interactions.indirect)}
                {state.elements?.indirect && (
                    <div className="graph-container">
                        <Graph
                            title="Visualization of Indirect Protein-Protein Interactions"
                            cytoscapeOptions={{ ...graphOpts, elements: state.elements.indirect }}
                        />
                    </div>
                )}
            </div>
            <hr />
            {renderComplexes(state.results.complexes)}
            <hr />
            {renderPathways(state.results.pathways)}
            <br />
        </div>
    );
};

export default Submitted;
