import * as React from "react";
import "./WaitingPage.scss";

type WaitingPageProps = {
    redirectUrl: string;
    waitingMessage?: React.ReactNode;
};

type WaitingPageState = {
    waiting: boolean;
};

const POLL_DELAY_MS = 5000 as const;

export default class WaitingPage extends React.Component<WaitingPageProps, WaitingPageState> {
    static defaultProps = {
        waitingMessage: (
            <h4>
                {" "}
                Your computation is still running. When it is complete, your results will be shown
                here.
            </h4>
        ),
    };

    state = {
        waiting: true,
    };

    waitForCompleted = () => {
        const httpGetAsync = () => {
            let req = new XMLHttpRequest();
            req.onloadend = () => {
                if (req.status == 200) {
                    // done waiting, will set waiting to false to render loaded page
                    this.setState({ waiting: false });
                    return;
                } else {
                    // try again
                    setTimeout(httpGetAsync, POLL_DELAY_MS);
                }
            };
            req.open("GET", this.props.redirectUrl, true); // true for asynchronous
            req.send();
        };
        httpGetAsync();
    };

    componentDidMount() {
        this.waitForCompleted();
    }

    render() {
        if (this.state.waiting) {
            return <div className="waiting-page">{this.props.waitingMessage}</div>;
        }

        if (this.props.children) {
            return <div className="waiting-page">{this.props.children}</div>;
        }

        return (
            <div className="waiting-page">
                <iframe className="results-frame" src={this.props.redirectUrl} frameBorder="0" />
            </div>
        );
    }
}
