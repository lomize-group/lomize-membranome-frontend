export const Assets = {
    membranome_pdf: (pdf_name: string) =>
        "https://storage.googleapis.com/membranome-assets/pdfs/" + pdf_name + ".pdf",
    images: {
        no_image: "/images/pdb_fallback.png",
        loading: "/images/loading.gif",
        loading_small: "/images/loading_small.gif",
        andrei: "https://storage.googleapis.com/membranome-assets/images/assets/andrei_lomize.jpg",
        irina: "https://storage.googleapis.com/membranome-assets/images/assets/irina_pogozheva.jpg",
        alexey: "https://storage.googleapis.com/membranome-assets/images/assets/alexey_kovalenko.jpg",
        topology: "https://storage.googleapis.com/opm-assets/images/bio_assets/topology.gif",
        fmap: "https://storage.googleapis.com/membranome-assets/images/assets/FMAPserver.jpg",
        tmdock: "https://storage.googleapis.com/membranome-assets/images/assets/TMDOCKserver.jpg",
        nsf: "https://storage.googleapis.com/membranome-assets/images/assets/nsf.gif",
        about: (img_name: string) =>
            "https://storage.googleapis.com/membranome-assets/images/about/" + img_name,
    },
    protein_pdb_tar:
        "https://storage.googleapis.com/membranome-assets/pdb_files/all_protein_pdbs.tar.gz",
    complex_pdb_tar:
        "https://storage.googleapis.com/membranome-assets/pdb_files/all_complex_pdbs.tar.gz",
    dimer_xlsx:
        "https://storage.googleapis.com/membranome-assets/pdb_files/Homodimers_05_2017.xlsx",
    dimers_pdbs:
        "https://storage.googleapis.com/membranome-assets/pdb_files/dimers_pdbs.tar.gz",
    TM_helices:
        "https://storage.googleapis.com/membranome-assets/pdb_files/TM_helices.tar.gz",
} as const;
