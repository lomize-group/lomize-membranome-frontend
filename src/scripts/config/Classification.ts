const categories = ["proteins", "complex_structures", "mutations", "pathways"] as const;

const proteins = [
    "protein_types",
    "protein_classes",
    "protein_superfamilies",
    "protein_families",
    "species",
    "protein_localizations",
    "proteins",
] as const;

const complex_structures = [
    "complex_classes",
    "complex_superfamilies",
    "complex_families",
    "complex_localizations",
    "complex_structures",
] as const;

const mutations = ["mutation_types", "diseases", "mutations"] as const;

const pathways = ["pathway_classes", "pathway_subclasses", "pathways"] as const;

export const statsTypes = [...proteins, ...complex_structures, ...mutations, ...pathways] as const;

const otherCtypes = ["protein_pathways"] as const;

const ctypes = [...statsTypes, ...otherCtypes] as const;

export type CTypeCategory = typeof categories[number];
export type ProteinCType = typeof proteins[number];
export type ComplexCType = typeof complex_structures[number];
export type MutationCType = typeof mutations[number];
export type PathwayCType = typeof pathways[number];
export type StatsType = typeof statsTypes[number];
export type CType = typeof ctypes[number];

export interface Definition {
    name: string;
    name_singular: string;
    api: {
        route: string;
        accessor: {
            object: string;
            count: string;
        };
    };
    parents: ReadonlyArray<CType>;
    child?: CType;
    column?: number;
    [key: string]: any;
}

interface ClassificationType {
    readonly definitions: { [key in CType]: Definition };
    [key: string]: any;
}

// The PFAM database was discontinued but a copy of it is at Interpro.
const interproPfamUrl = (pfam_id: string) => {
    if (pfam_id[0] === "C") {
        return "https://www.ebi.ac.uk/interpro/set/pfam/" + pfam_id + "/";
    } else if (pfam_id[0] === "I") {
        return "http://www.ebi.ac.uk/interpro/entry/" + pfam_id;
    } else {
        return "https://www.ebi.ac.uk/interpro/entry/pfam/" + pfam_id + "/";
    }
};

export const Classification: ClassificationType = {
    categories,
    proteins,
    complex_structures,
    mutations,
    pathways,
    tables: ctypes,

    category_of: function (ctype: CType): CTypeCategory | undefined {
        for (let i = 0; i < this.categories.length; ++i) {
            const category = this.categories[i];
            if (this[category].includes(ctype)) {
                return category;
            }
        }
        return undefined;
    },

    definitions: {
        protein_types: {
            name: "types",
            name_singular: "type",
            api: {
                route: "/protein_types",
                accessor: {
                    object: "protein_type",
                    count: "protein_types_count",
                },
            },
            child: "protein_classes",
            parents: [],
        },
        protein_classes: {
            name: "classes",
            name_singular: "class",
            api: {
                route: "/protein_classes",
                accessor: {
                    object: "protein_class",
                    count: "protein_classes_count",
                },
            },
            parents: ["protein_types"],
            child: "protein_superfamilies",
        },
        protein_superfamilies: {
            name: "superfamilies",
            name_singular: "superfamily",
            api: {
                route: "/protein_superfamilies",
                accessor: {
                    object: "protein_superfamily",
                    count: "protein_superfamilies_count",
                },
            },
            parents: ["protein_classes"],
            child: "protein_families",
            tcdb: (tcdb_id: string) => "http://www.tcdb.org/search/result.php?tc=" + tcdb_id,
            pfam: interproPfamUrl,
        },
        protein_families: {
            name: "families",
            name_singular: "family",
            column: 0,
            api: {
                route: "/protein_families",
                accessor: {
                    object: "protein_family",
                    count: "protein_families_count",
                },
            },
            parents: ["protein_superfamilies"],
            child: "proteins",
            interpro: interproPfamUrl,
        },
        species: {
            name: "species",
            name_singular: "species",
            column: 3,
            api: {
                route: "/species",
                accessor: {
                    object: "species",
                    count: "species_count",
                },
            },
            parents: [],
            child: "proteins",
        },
        protein_localizations: {
            name: "localizations",
            name_singular: "localization",
            column: 4,
            api: {
                route: "/membranes",
                accessor: {
                    object: "membrane",
                    count: "membranes_count",
                },
            },
            parents: [],
            child: "proteins",
        },
        protein_pathways: {
            name: "pathways",
            name_singular: "pathway",
            api: {
                route: "/protein_pathways",
                accessor: {
                    object: "protein_pathways",
                    count: "protein_pathways_count",
                },
            },
            parents: [],
        },
        proteins: {
            name: "proteins",
            name_singular: "protein",
            api: {
                route: "/proteins",
                accessor: {
                    object: "protein",
                    count: "proteins_count",
                },
            },
            parents: ["protein_families", "protein_localizations", "species"],
            domain: {
                pdb: (pdb: string) =>
                    "http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/pdbsum/GetPage.pl?pdbcode=" +
                    pdb,
                pfam: (pfam: string) => "https://www.ebi.ac.uk/interpro/entry/pfam/" + pfam,
            },
            interaction: {
                pdb: (pdb: string) => "https://www.rcsb.org/structure/" + pdb,
                pubmed: (num: number) => "http://www.ncbi.nlm.nih.gov/pubmed/" + num,
            },
            uniprotcode: {
                uniprot: (uni: string) => "http://www.uniprot.org/uniprot/" + uni,
                pfam: (uni: string) => "https://www.ebi.ac.uk/interpro/protein/UniProt/" + uni,
                interpro: (uni: string) => "http://www.ebi.ac.uk/interpro/protein/UniProt/" + uni,
                ihop: (uni: string) =>
                    "http://www.ihop-net.org/UniPub/iHOP/in?dbrefs_1=UNIPROT__AC|" + uni,
                string: (uni: string) =>
                    "http://string-db.org/newstring_cgi/show_network_section.pl?identifier=" + uni,
                reactome: (uni: string) =>
                    "http://www.reactome.org/cgi-bin/link?SOURCE=UniProt&ID=" + uni,
                intact: (uni: string) => "https://www.ebi.ac.uk/intact/interactors/id:" + uni + "*",
                alphafold: (uni: string) => "https://alphafold.ebi.ac.uk/entry/" + uni,
            },
            gene: (num: number) => "http://www.genenames.org/data/hgnc_data.php?hgnc_id=" + num,
            hmp: (id: number) => "http://www.hmdb.ca/proteins/" + id,
            biogrid: (id: number) => "https://thebiogrid.org/" + id,
            pdbsum: (pdb_id: string) =>
                "http://www.ebi.ac.uk/thornton-srv/databases/cgi-bin/pdbsum/GetPage.pl?pdbcode=" +
                pdb_id,
            pdb: (pdb_id: string) => "http://www.rcsb.org/pdb/explore.do?structureId=" + pdb_id,
            scop: (pdb_id: string) =>
                "http://scop.mrc-lmb.cam.ac.uk/scop/pdb.cgi?disp=scop&id=" + pdb_id,
            msd: (pdb_id: string) =>
                "http://www.ebi.ac.uk/pdbe-srv/view/entry/" + pdb_id + "/summary.html",
            oca: (pdb_id: string) => "http://bip.weizmann.ac.il/oca-bin/ocashort?id=" + pdb_id,
            mmdb: (pdb_id: string) =>
                "http://www.ncbi.nlm.nih.gov/Structure/mmdb/mmdbsrv.cgi?uid=" + pdb_id,
            pdb_image: (pdb_id: string) =>
                "https://storage.googleapis.com/membranome-assets/pdb_images/proteins/" +
                pdb_id +
                ".png",
            pdb_file: (pdb_id: string, glmol: boolean=false) => {
                if (glmol) {
                    return "https://storage.googleapis.com/membranome-assets/pdb_files/dimers/" +
                        pdb_id + ".pdb";
                } else {
                    return "https://storage.googleapis.com/membranome-assets/pdb_files/proteins/" +
                        pdb_id + ".pdb";
                }
            },
            jmol: function (pdb_id: string) {
                return (
                    "http://bioinformatics.org/firstglance/fgij//fg.htm?mol=" +
                    this.pdb_file(pdb_id)
                );
            },
            wiki: (gene: string) => "https://en.wikipedia.org/wiki/" + gene,
        },

        complex_classes: {
            name: "classes",
            name_singular: "class",
            api: {
                route: "/complex_classes",
                accessor: {
                    object: "protein_class",
                    count: "complex_classes_count",
                },
            },
            child: "complex_superfamilies",
            parents: [],
        },
        complex_superfamilies: {
            name: "superfamilies",
            name_singular: "superfamily",
            api: {
                route: "/complex_superfamilies",
                accessor: {
                    object: "protein_superfamily",
                    count: "protein_superfamilies_count",
                },
            },
            parents: ["protein_classes"],
            child: "complex_families",
        },
        complex_families: {
            column: 0,
            name: "families",
            name_singular: "family",
            api: {
                route: "/complex_families",
                accessor: {
                    object: "protein_family",
                    count: "protein_families_count",
                },
            },
            parents: ["protein_superfamilies"],
            child: "complex_structures",
        },
        complex_structures: {
            name: "complexes",
            name_singular: "complex",
            api: {
                route: "/complex_structures",
                accessor: {
                    object: "complex",
                    count: "complex_structures_count",
                },
            },
            parents: ["complex_families", "complex_localizations"],
            pdb_image: (pdb_id: string) =>
                "https://storage.googleapis.com/membranome-assets/pdb_images/complexes/" +
                pdb_id +
                ".png",
            pdb_file: (pdb_id: string) =>
                "https://storage.googleapis.com/membranome-assets/pdb_files/complexes/" +
                pdb_id +
                ".pdb",
            jmol: function (pdb_id: string) {
                return (
                    "http://bioinformatics.org/firstglance/fgij//fg.htm?mol=" +
                    this.pdb_file(pdb_id)
                );
            },
        },

        complex_localizations: {
            column: 3,
            name: "localizations",
            name_singular: "localization",
            api: {
                route: "/membranes",
                accessor: {
                    object: "membranes",
                    count: "membranes_count",
                },
            },
            parents: [],
            child: "complex_structures",
        },

        mutations: {
            name: "mutations",
            name_singular: "mutation",
            api: {
                route: "/mutations",
                accessor: {
                    object: "mutation",
                    count: "mutations_count",
                },
            },
            parents: ["mutation_types", "diseases"],
        },
        mutation_types: {
            name: "types",
            name_singular: "type",
            api: {
                route: "/mutation_types",
                accessor: {
                    object: "mutation_type",
                    count: "mutation_types_count",
                },
            },
            child: "mutations",
            parents: [],
        },
        diseases: {
            name: "diseases",
            name_singular: "disease",
            api: {
                route: "/diseases",
                accessor: {
                    object: "disease",
                    count: "diseases_count",
                },
            },
            child: "mutations",
            parents: [],
        },
        pathways: {
            name: "pathways",
            name_singular: "pathway",
            api: {
                route: "/pathways",
                accessor: {
                    object: "pathway",
                    count: "pathways_count",
                },
            },
            parents: ["pathway_subclasses"],
        },
        pathway_subclasses: {
            name: "subclasses",
            name_singular: "subclass",
            column: 0,
            api: {
                route: "/pathway_subclasses",
                accessor: {
                    object: "pathway_subclass",
                    count: "pathway_subclasses_count",
                },
            },
            parents: ["pathway_classes"],
            child: "pathways",
        },
        pathway_classes: {
            name: "classes",
            name_singular: "class",
            api: {
                route: "/pathway_classes",
                accessor: {
                    object: "pathway_class",
                    count: "pathway_classes_count",
                },
            },
            child: "pathway_subclasses",
            parents: [],
        },
    },
} as const;
