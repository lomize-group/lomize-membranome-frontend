import * as React from "react";
import { Link } from "react-router-dom";
import ExternalLink from "../components/external_link/ExternalLink";
import { PDBLink } from "../components/table/pdb_link/PDBLink";
import { GenericObj, TableColumn } from "./Types";
import { shortenString } from "./Util";

export class ColumnWrapper {
    private readonly _columnDefs: ReadonlyArray<TableColumn>;
    private readonly _rowHeight: number;

    constructor(columnDefs: ReadonlyArray<TableColumn>, rowHeight: number = 50) {
        this._columnDefs = columnDefs;
        this._rowHeight = rowHeight;
    }

    get rowHeight(): number {
        return this._rowHeight;
    }

    get columns(): ReadonlyArray<TableColumn> {
        return this._columnDefs;
    }

    columnsWithout(index: number): ReadonlyArray<TableColumn> {
        let arr: Array<TableColumn> = Array<TableColumn>();
        for (let i: number = 0; i < this._columnDefs.length; ++i) {
            if (i !== index) {
                arr.push(this._columnDefs[i]);
            }
        }
        return arr;
    }
}

export const Columns: GenericObj<ColumnWrapper> = {
    proteins: new ColumnWrapper(
        [
            {
                Header: () => (
                    <div className="header-cell" title="Family">
                        <div>Family</div>
                    </div>
                ),
                id: "protein_family_name_cache",
                flexGrow: 3,
                minWidth: 135,
                Cell: (row: any) => (
                    <div className="cell" title={row.protein_family_name_cache}>
                        <div>
                            <Link to={"/protein_families/" + row.protein_family_id}>
                                {shortenString(row.protein_family_name_cache, 40)}
                            </Link>
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Protein Name">
                        <div>Protein Name</div>
                    </div>
                ),
                id: "name",
                flexGrow: 3,
                minWidth: 135,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            <PDBLink
                                id={row.id}
                                name={row.name}
                                pdb={row.pdbid}
                                cell={shortenString(row.name, 40)}
                                onImgChange={row.onImgChange}
                            />
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="PDB ID">
                        <div>Protein ID</div>
                    </div>
                ),
                width: 95,
                id: "pdbid",
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            <PDBLink
                                id={row.id}
                                name={row.name}
                                pdb={row.pdbid}
                                cell={row.pdbid}
                                onImgChange={row.onImgChange}
                            />
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Species">
                        <div>Species</div>
                    </div>
                ),
                id: "species_name_cache",
                width: 85,
                Cell: (row: any) => {
                    let parts = row.species_name_cache.split(" ");
                    let name = row.species_name_cache;
                    if (parts.length > 1) {
                        name = parts[0][0] + ". " + parts.slice(1).join(" ");
                    }
                    return (
                        <div className="cell" title={row.species_name_cache}>
                            <div>
                                <Link to={"/species/" + row.species_id}>
                                    <i>{name}</i>
                                </Link>
                            </div>
                        </div>
                    );
                },
            },
            {
                Header: () => (
                    <div className="header-cell" title="Localization">
                        <div>Localization</div>
                    </div>
                ),
                id: "membrane_name_cache",
                flexGrow: 1,
                minWidth: 75,
                Cell: (row: any) => (
                    <div className="cell" title={row.membrane_name_cache}>
                        <div>
                            <Link to={"/protein_localizations/" + row.membrane_id}>
                                {row.membrane_name_cache}
                            </Link>
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Topology">
                        <div>Topology</div>
                    </div>
                ),
                id: "topology_show_in",
                width: 60,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            {row.topology_show_in === null
                                ? "Und"
                                : row.topology_show_in
                                ? "In"
                                : "Out"}
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="TM Segment">
                        <div>TM Segment (Helix)</div>
                    </div>
                ),
                id: "segment",
                width: 70,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>{row.segment}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Hydrophobic Thickness or Depth (Å)">
                        <div>Hydrophobic Thickness or Depth (Å)</div>
                    </div>
                ),
                width: 85,
                id: "thickness",
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            {row.thickness.toFixed(1)}
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Tilt Angle (°)">
                        <div>Tilt Angle (°)</div>
                    </div>
                ),
                width: 45,
                id: "tilt",
                Cell: (row: any) => (
                    <div className="cell">
                        <div>{row.tilt}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="ΔG (fold) (kcal/mol)">
                        <div>
                            <span>
                                ΔG<sub>fold</sub>
                                <br />
                                (kcal/mol)
                            </span>
                        </div>
                    </div>
                ),
                width: 70,
                id: "gibbs",
                Cell: (row: any) => (
                    <div className="cell">
                        <div>{row.gibbs.toFixed(1)}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="ΔG (transfer) (kcal/mol)">
                        <div>
                            <span>
                                ΔG<sub>transf</sub>
                                <br />
                                (kcal/mol)
                            </span>
                        </div>
                    </div>
                ),
                width: 70,
                id: "interpro",
                Cell: (row: any) => (
                    <div className="cell">
                        <div>{row.interpro.toFixed(1)}</div>
                    </div>
                ),
            },
        ],
        50
    ),

    protein_mutations: new ColumnWrapper(
        [
            {
                Header: () => (
                    <div className="header-cell" title="Substituted Resiude #">
                        <div>Substituted Residue #</div>
                    </div>
                ),
                id: "residue",
                flexGrow: 3,
                minWidth: 110,
                Cell: (row: any) => (
                    <div className="cell" title={row.residue}>
                        <div>{row.residue}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Mutation">
                        <div>Mutation</div>
                    </div>
                ),
                id: "mutation",
                flexGrow: 3,
                minWidth: 80,
                Cell: (row: any) => (
                    <div className="cell" title={row.mutation}>
                        <div>{row.mutation}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Mutation Type">
                        <div>Mutation Type</div>
                    </div>
                ),
                id: "mutation_type_name_cache",
                flexGrow: 3,
                minWidth: 100,
                Cell: (row: any) => (
                    <div className="cell" title={row.mutation_type_name_cache}>
                        <div>{row.mutation_type_name_cache}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Diseases">
                        <div>Diseases</div>
                    </div>
                ),
                id: "diseases_name_cache",
                flexGrow: 3,
                minWidth: 100,
                Cell: (row: any) => (
                    <div className="cell" title="Disease">
                        <div>{row.diseases_name_cache}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Comments">
                        <div>Comments</div>
                    </div>
                ),
                id: "comments",
                flexGrow: 3,
                minWidth: 220,
                Cell: (row: any) => (
                    <div className="cell" title={row.comments}>
                        <div>{row.comments}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Source">
                        <div>Source</div>
                    </div>
                ),
                id: "source",
                flexGrow: 3,
                minWidth: 120,
                Cell: (row: any) => (
                    <div className="cell" title={row.source}>
                        <div>{row.source}</div>
                    </div>
                ),
            },
        ],
        42
    ),

    mutations: new ColumnWrapper(
        [
            {
                Header: () => (
                    <div className="header-cell" title="Family Code">
                        <div>Family Code</div>
                    </div>
                ),
                id: "ordering_cache",
                flexGrow: 3,
                minWidth: 60,
                Cell: (row: any) => (
                    <div className="cell" title={row.ordering_cache}>
                        <div>{row.ordering_cache}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Family">
                        <div>Family</div>
                    </div>
                ),
                id: "protein_family_name_cache",
                flexGrow: 3,
                minWidth: 134,
                Cell: (row: any) => (
                    <div className="cell" title={row.protein_family_name_cache}>
                        <div>
                            <Link to={"/protein_families/" + row.protein_family_id}>
                                {shortenString(row.protein_family_name_cache, 40)}
                            </Link>
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Protein ID">
                        <div>Protein ID</div>
                    </div>
                ),
                id: "pdbid",
                flexGrow: 3,
                minWidth: 80,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            <PDBLink
                                id={row.protein_id}
                                name={row.pdbid}
                                pdb={row.pdbid}
                                cell={row.pdbid}
                                onImgChange={row.onImgChange}
                            />
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Mutation">
                        <div>Mutation</div>
                    </div>
                ),
                id: "mutation",
                flexGrow: 3,
                minWidth: 70,
                Cell: (row: any) => (
                    <div className="cell" title={row.mutation}>
                        <div>{row.mutation}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Mutation Type">
                        <div>Mutation Type</div>
                    </div>
                ),
                id: "mutation_type_name_cache",
                flexGrow: 3,
                minWidth: 90,
                Cell: (row: any) => (
                    <div className="cell" title={row.mutation_type_name_cache}>
                        <div>{row.mutation_type_name_cache}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Diseases">
                        <div>Diseases</div>
                    </div>
                ),
                id: "diseases_name_cache",
                flexGrow: 3,
                minWidth: 90,
                Cell: (row: any) => {
                    return (
                        <div className="cell" title="Disease">
                            <div>{row.diseases_name_cache}</div>
                        </div>
                    );
                },
            },
            {
                Header: () => (
                    <div className="header-cell" title="Comments">
                        <div>Comments</div>
                    </div>
                ),
                id: "comments",
                flexGrow: 3,
                minWidth: 135,
                Cell: (row: any) => (
                    <div className="cell" title={row.comments}>
                        <div>{shortenString(row.comments, 60)}</div>
                    </div>
                ),
            },
        ],
        50
    ),

    complex_structures: new ColumnWrapper(
        [
            {
                Header: () => (
                    <div className="header-cell" title="Families">
                        <div>Families</div>
                    </div>
                ),
                id: "",
                flexGrow: 3,
                minWidth: 115,
                width: 160,
                Cell: (row: any) => (
                    <div className="cell" title="Families">
                        <ul className="less-padding">
                            {row.complex_family_data.map((fam: any, i: number) => (
                                <li key={i}>
                                    <Link to={"/complex_families/" + fam.protein_family_id}>
                                        {shortenString(fam.protein_family_name_cache, 70)}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Protein Name">
                        <div>Complex Name</div>
                    </div>
                ),
                id: "name",
                flexGrow: 3,
                minWidth: 115,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            <PDBLink
                                id={row.id}
                                name={row.name}
                                pdb={row.refpdbcode}
                                cell={row.name}
                                onImgChange={row.onImgChange}
                                complex
                            />
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="PDB ID">
                        <div>Reference ID</div>
                    </div>
                ),
                width: 65,
                id: "refpdbcode",
                Cell: (row: any) => (
                    <div className="cell">
                        <div>
                            {row.refpdbcode ? (
                                <PDBLink
                                    id={row.id}
                                    name={row.name}
                                    pdb={row.refpdbcode}
                                    cell={row.refpdbcode}
                                    onImgChange={row.onImgChange}
                                    complex
                                />
                            ) : (
                                <span>none</span>
                            )}
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Localization">
                        <div>Localizations</div>
                    </div>
                ),
                id: "",
                flexGrow: 1,
                minWidth: 85,
                Cell: (row: any) => (
                    <div className="cell" title="Localizations">
                        <ul className="less-padding">
                            {row.complex_membrane_data.map((mem: any, i: number) => (
                                <li key={i}>
                                    <Link to={"/complex_localizations/" + mem.membrane_id}>
                                        {mem.membrane_name_cache}
                                    </Link>
                                </li>
                            ))}
                        </ul>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="TM Segment">
                        <div>Number of TM Subunits</div>
                    </div>
                ),
                id: "numsubunits",
                width: 60,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>{row.numsubunits}</div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Polytopic Protein Count">
                        <div>Number Polytopic Subunits</div>
                    </div>
                ),
                id: "polytopic_proteins_count",
                width: 60,
                Cell: (row: any) => (
                    <div className="cell">
                        <div>{row.polytopic_proteins_count}</div>
                    </div>
                ),
            },
        ],
        56
    ),

    pathways: new ColumnWrapper(
        [
            {
                Header: () => (
                    <div className="header-cell" title="Pathway Subclass">
                        <div>Pathway Subclass</div>
                    </div>
                ),
                id: "pathway_subclass_name_cache",
                flexGrow: 3,
                minWidth: 135,
                Cell: (row: any) => (
                    <div className="cell" title={row.pathway_subclass_name_cache}>
                        <div>
                            <Link to={"/pathway_subclasses/" + row.pathway_subclass_id}>
                                {row.pathway_subclass_name_cache}
                            </Link>
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Pathway Name">
                        <div>Pathway Name</div>
                    </div>
                ),
                id: "name",
                flexGrow: 3,
                minWidth: 135,
                Cell: (row: any) => (
                    <div className="cell" title={row.name}>
                        <div>
                            <Link to={"/pathways/" + row.id}>{row.name}</Link>
                        </div>
                    </div>
                ),
            },
            {
                Header: () => (
                    <div className="header-cell" title="Database">
                        <div>Database</div>
                    </div>
                ),
                id: "dbname",
                flexGrow: 3,
                minWidth: 135,
                Cell: (row: any) => (
                    <div className="cell" title={row.dbname}>
                        <div>
                            <ExternalLink href={row.link} prepend_https>
                                {row.dbname}
                            </ExternalLink>
                        </div>
                    </div>
                ),
            },
        ],
        50
    ),
} as const;
