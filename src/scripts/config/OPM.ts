export const OPM = {
    website_base_url: "https://opm.phar.umich.edu",
    protein_by_pdbid: function (pdbid: string) {
        return this.website_base_url + "/proteins?search=" + pdbid;
    },
} as const;
