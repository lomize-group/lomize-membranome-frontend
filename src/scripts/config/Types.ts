export {
    CTypeCategory,
    ProteinCType,
    ComplexCType,
    MutationCType,
    PathwayCType,
    CType,
    Definition,
} from "./Classification";

export type GenericObj<V = any> = { [key: string]: V };

export type TableColumn = {
    Header: () => JSX.Element;
    Cell: (row: any) => JSX.Element;
    id: string;
    flexGrow?: number;
    minWidth?: number;
    width?: number;
};

export type ApiPagedResponse<T = any> = {
    total_objects: number;
    objects: Array<T>;
    sort: string;
    direction: string;
    page_size: number;
    page_num: number;
};

export type ApiSearchOptions = {
    search_by?: "full" | "limited";
    search_separator?: string;
};

export type ApiParams = ApiSearchOptions & {
    search?: string;
    sort?: string;
    direction?: string;
    species?: string;
    pageSize?: number;
    pageNum?: number;
};

export type RecursivePartial<T> = {
    [P in keyof T]?: RecursivePartial<T[P]>;
};

export type PartialExcept<T, K extends keyof T> = RecursivePartial<T> & Pick<T, K>;
