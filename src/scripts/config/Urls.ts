import * as appendQuery from "append-query";

export type ComputationServer = "memprot" | "cgopm" | "local";

const sunshineBase = "https://sunshine.phar.umich.edu:8008/";
// const sunshineBase = "http://localhost:9000/";

export const Urls = {
    computation_servers: {
        memprot: "https://memprot.org/",
        cgopm: "https://cgopm.cc.lehigh.edu/",
        local: "http://localhost:9000/",
    },

    //baseUrl: "https://lomize-group-membranome.herokuapp.com",
    baseUrl: "https://opm-back.cc.lehigh.edu/membranome-backend",
    //baseUrl: "http://localhost:3000",

    icn3d: (pdbFileUrl: string): string =>
        "https://www.ncbi.nlm.nih.gov/Structure/icn3d/full.html?type=pdb&url=" + pdbFileUrl,

    dimer: {
        image: (pdb_id: string): string =>
            `https://storage.googleapis.com/membranome-assets/pdb_images/dimers/${pdb_id}.png`,
        pdbFile: (pdb_id: string): string =>
            `https://storage.googleapis.com/membranome-assets/pdb_files/dimers/${pdb_id}.pdb`,
    },

    pubmed: (pmid: string) => "https://www.ncbi.nlm.nih.gov/pubmed?cmd=search&term=" + pmid,
    corum: "https://mips.helmholtz-muenchen.de/corum/",

    //api_url, imported from Urls.ts in opm-frontend
    api_url: function (suffix: string, query: { [key: string]: any } = {}): string {
        const url = this.baseUrl + "/" + suffix;
        return appendQuery(url, query);
    },
} as const;
