import { InfoProps } from "../components/info/Info";
import { Classification, CType } from "./Classification";
import { GenericObj } from "./Types";

/**
 * Check if an email is formatted validly.
 * @param email the email to be checked
 * @param allowEmpty whether or not to consider an empty string a valid email
 * @returns true if the email is valid format, false otherwise
 */
export function validEmail(email: string, allowEmpty: boolean = true): boolean {
    if (email === "") {
        return allowEmpty;
    }
    const email_re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return email_re.test(String(email).toLowerCase());
}

/**
 * Check if a filename is valid.
 * @param filename the filename to check validity of
 * @returns true if the filename is valid, false otherwise
 */
export function validFilename(filename: string): boolean {
    const filename_re = /^[a-zA-Z0-9_]*$/;
    return filename_re.test(String(filename));
}

/**
 * Capitalizes the first letter of a string.
 * @param s the string to be capitalized.
 * @returns the capitalized string
 */
export const capitalize = (s: string) => s.charAt(0).toUpperCase() + s.slice(1);

/**
 * Shortens a string to a given length and appends "..." to indicate that it
 * has been shortened.
 * @param s the string to be shortened
 * @param maxLength the maximum length allowed of the string before trimming.
 * @returns the shortened string
 */
export function shortenString(s: string, maxLength: number) {
    if (!s) {
        return "";
    } else if (s.length > maxLength) {
        return s.slice(0, maxLength).trim() + "...";
    }
    return s;
}

/**
 * Prepends "https://" to a link if it does not already start with "http://" or "https://".
 * @param link any url
 * @returns http(s?)-prepended link
 */
export function prependHttps(link: string) {
    if (!link.startsWith("http://") && !link.startsWith("https://")) {
        return "https://" + link;
    }
    return link;
}

/**
 * Immutable sort of an array strings or array of objects with a string property.
 * @param objs array of string or object with string key to be sorted.
 * @param criteria the sort criteria.
 * 	if undefined: does a standard sort on the array of strings.
 * 		only makes sense to use if T is string.
 * 	if string: sorts the array of objects by key.
 * 		only makes sense to use if T is object.
 * 	if function: sorts the array of objects by the value returned by the function.
 * 		can be used if T is string or object.
 * @returns the objects sorted by given criteria
 */
export function sortStrings<T extends string | GenericObj>(
    [...objs]: Array<T>,
    criteria?: keyof T | ((item: any) => string),
    reverse = false
) {
    if (criteria !== undefined) {
        // sort object by criteria function
        if (typeof criteria === "function") {
            if (reverse) {
                return objs.sort((a, b) => {
                    const aCrit = criteria(a);
                    const bCrit = criteria(b);
                    return bCrit < aCrit ? -1 : bCrit > aCrit ? 1 : 0;
                });
            }
            return objs.sort((a, b) => {
                const aCrit = criteria(a);
                const bCrit = criteria(b);
                return aCrit < bCrit ? -1 : aCrit > bCrit ? 1 : 0;
            });
        }
        // sort object by string key
        if (reverse) {
            return objs.sort((a, b) =>
                b[criteria] < a[criteria] ? -1 : b[criteria] > a[criteria] ? 1 : 0
            );
        }
        return objs.sort((a, b) =>
            a[criteria] < b[criteria] ? -1 : a[criteria] > b[criteria] ? 1 : 0
        );
    }
    if (reverse) {
        return objs.sort((a, b) => (b < a ? -1 : b > a ? 1 : 0));
    }
    return objs.sort((a, b) => (a < b ? -1 : a > b ? 1 : 0));
}

/**
 * Group an array of items by a criteria.
 * @param arr the array of items to be grouped
 * @param criteria the criteria on which to group the items
 * @returns an object with the items grouped
 */
export function group<T>(
    arr: ReadonlyArray<T>,
    criteria: (item: T) => string | number
): GenericObj<Array<T>> {
    return arr.reduce<GenericObj<Array<T>>>(function (obj, item: T) {
        const key = criteria(item); // get group of item based on criteria
        // create group if it does not exist
        if (!obj.hasOwnProperty(key)) {
            obj[key] = [];
        }
        obj[key].push(item); // add item to group
        return obj;
    }, {});
}

/**
 * Gets props for Info component for every parent of a given CType.
 * @param ctype Classification type name.
 * @param obj The object to recurse on to create the Info props for each parent of the ctype.
 * @returns object with the Info props for each parent of the ctype.
 */
export function createInfo(ctype: CType, obj: GenericObj, stopAt?: CType): InfoProps {
    const cdef = Classification.definitions[ctype];
    let result: GenericObj = {};

    // if the classification type has children add data for info list item
    if (cdef.child) {
        result.base = ctype;
        result[ctype] = [
            {
                id: obj.id,
                name: obj.name,
                subclasses: obj[Classification.definitions[cdef.child].api.accessor.count],
                pfam: obj.pfam,
                interpro: obj.interpro,
            },
        ];
    }

    // create closure with result
    function inner(curType: CType, curObj: GenericObj) {
        if (curType === stopAt) {
            return;
        }

        const curCtypeDef = Classification.definitions[curType];
        for (let i = 0; i < curCtypeDef.parents.length; i++) {
            const parent: CType = curCtypeDef.parents[i];
            let parentObj = curObj[Classification.definitions[parent].api.accessor.object];
            if (parentObj !== undefined) {
                result[parent] = [
                    {
                        id: parentObj.id,
                        name: parentObj.name,
                        subclasses: parentObj[curCtypeDef.api.accessor.count],
                        pfam: parentObj.pfam,
                        interpro: parentObj.interpro,
                    },
                ];
                // recurse with parent to go up the classification tree
                inner(parent, parentObj);
            }
        }
    }
    // populate result by calling inner() closure
    inner(ctype, obj);
    return result;
}

export function hasClassificationItems(ctype: CType, id?: string) {
    const cdef = Classification.definitions[ctype];
    const childDef = cdef.child ? Classification.definitions[cdef.child] : undefined;
    const grandChild = childDef?.child;

    return {
        hasInfo: id !== undefined,
        hasList: childDef !== undefined && (grandChild !== undefined || !id),
        hasTable: id !== undefined || !childDef,
    };
}
