import { useState, useEffect } from "react";
import Axios from "axios";

import { Classification } from "../config/Classification";
import { InfoProps } from "../components/info/Info";
import { Urls } from "../config/Urls";
import { createInfo } from "../config/Util";
import { CType, GenericObj } from "../config/Types";

type Params = {
    ctype: CType;
    stopAt?: CType;
    id?: string;
};

type State = {
    object: GenericObj;
    info: InfoProps;
};

export default function useClassificationInfo({ ctype, id, stopAt }: Params) {
    const [state, setState] = useState<State>();

    useEffect(() => {
        if (id) {
            const fetchData = async () => {
                const cdef = Classification.definitions[ctype];
                const { data } = await Axios.get(Urls.baseUrl + cdef.api.route + "/" + id);
                setState({
                    object: data,
                    info: createInfo(ctype, data, stopAt),
                });
            };
            fetchData();
        }
    }, [ctype, id]);

    return state;
}
