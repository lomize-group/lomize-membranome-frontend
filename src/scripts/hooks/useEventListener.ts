import { useEffect, DependencyList } from "react";

export default function useEventListener(
    event: string,
    listener: EventListener | EventListenerObject,
    deps: DependencyList = [],
    watch = window
) {
    useEffect(() => {
        // create listener
        watch.addEventListener(event, listener);
        return () => {
            // remove listener
            watch.removeEventListener(event, listener);
        };
    }, [event, listener, ...deps, watch]);
}
