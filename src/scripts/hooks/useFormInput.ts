import * as React from "react";

type FormInputData<B, E = React.InputHTMLAttributes<B>> = {
    value: string;
    setValue: React.Dispatch<React.SetStateAction<string>>;
    reset: () => void;
    bind: React.InputHTMLAttributes<B> & E;
};

export default function useFormInput<
    T extends HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement
>(initialValue: string | number): FormInputData<T> {
    const [value, setValue] = React.useState(initialValue.toString());
    return {
        value,
        setValue,
        reset: () => setValue(initialValue.toString()),
        bind: {
            value,
            onChange: (e) => setValue(e.target.value),
        },
    };
}
