import { useState, useEffect } from "react";
import usePrevious from "./usePrevious";

export default function useHasChanged<T>(value: T, isEqualCmp?: (oldVal: T, newVal: T) => boolean) {
    const prev = usePrevious(value);
    const [hasChanged, setHasChanged] = useState(false);

    useEffect(() => {
        if (prev === undefined && value !== undefined) {
            setHasChanged(true);
        } else if (isEqualCmp) {
            setHasChanged(prev === undefined || !isEqualCmp(prev, value));
        } else {
            setHasChanged(prev !== value);
        }
    }, [value, isEqualCmp]);

    return hasChanged;
}
