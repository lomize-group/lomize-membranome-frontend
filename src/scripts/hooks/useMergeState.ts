import { useState } from "react";

type StateUpdateFunction<S> = (prevState: S) => S;
type StateUpdater<S> = StateUpdateFunction<S> | Partial<S>;

function useMergeState<S>(initial: S): [S, (updater: StateUpdater<S>) => void] {
    const [state, setState] = useState(initial);

    function setMergeState(updater: StateUpdater<S>) {
        setState((prevState) => {
            if (typeof updater === "function") {
                return { ...prevState, ...updater(prevState) };
            }
            return { ...prevState, ...updater };
        });
    }

    return [state, setMergeState];
}

export default useMergeState;
