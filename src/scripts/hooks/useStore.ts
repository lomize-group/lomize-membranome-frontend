import { useSelector, TypedUseSelectorHook } from "react-redux";
import { RootState } from "../store";

const useStore: TypedUseSelectorHook<RootState> = useSelector;

export default useStore;
