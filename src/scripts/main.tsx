import * as appendQuery from "append-query";
import "bootstrap-sass/assets/stylesheets/_bootstrap.scss";
import "core-js/es6/";
import "core-js/es6/map";
import "core-js/es6/set";
import * as qs from "query-string";
import "raf/polyfill";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Redirect, Route, Switch } from "react-router-dom";
import { applyMiddleware, createStore } from "redux";
import { logger } from "redux-logger";
import App from "./App";
import About from "./components/about/About";
import Classification from "./components/classification/Classification";
import Complex from "./components/complex/Complex";
import Contact from "./components/contact/Contact";
import Download from "./components/download/Download";
import AdvancedSearch from "./components/advanced_search/AdvancedSearch";
import FMAP from "./components/fmap/FMAP";
import Fold from "./components/fold/Fold";
import Home from "./components/home/Home";
import Pathway from "./components/pathway/Pathway";
import Protein from "./components/protein/Protein";
import ProteinViewer from "./components/protein_viewer/ProteinViewer";
import TMDOCK from "./components/tmdock/TMDOCK";
import TMMATCH from "./components/tmmatch/TMMatch";
import TMNet from "./components/tmnet/TMNet";
import Error404 from "./Error404";
import { rootReducer } from "./store";

// use logger middleware only in development environment
const store = createStore(rootReducer, applyMiddleware(logger));

const Main: React.FC = () => {
    return (
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route
                        exact
                        path="/"
                        component={(props: any) => <App children={<Home />} {...props} />}
                    />

                    <Route
                        exact
                        path="/about"
                        component={(props: any) => <App children={<About />} {...props} />}
                    />
                    <Route
                        exact
                        path="/advanced_search"
                        component={(props: any) => <App children={<AdvancedSearch />} {...props} />}
                    />
                    <Route
                        exact
                        path="/download"
                        component={(props: any) => <App children={<Download />} {...props} />}
                    />
                    <Route
                        exact
                        path="/contact"
                        component={(props: any) => <App children={<Contact />} {...props} />}
                    />

                    <Route
                        exact
                        path="/fmap"
                        component={(props: any) => (
                            <App {...props}>
                                <FMAP computationServer="memprot" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fmap/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <FMAP computationServer="memprot" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fmap_cgopm"
                        component={(props: any) => (
                            <App {...props}>
                                <FMAP computationServer="cgopm" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fmap_cgopm/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <FMAP computationServer="cgopm" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fmap_local"
                        component={(props: any) => (
                            <App {...props}>
                                <FMAP computationServer="local" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fmap_local/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <FMAP computationServer="local" submitted />
                            </App>
                        )}
                    />

                    <Route
                        exact
                        path="/tmdock"
                        component={(props: any) => (
                            <App {...props}>
                                <TMDOCK computationServer="memprot" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmdock/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <TMDOCK computationServer="memprot" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmdock_cgopm"
                        component={(props: any) => (
                            <App {...props}>
                                <TMDOCK computationServer="cgopm" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmdock_cgopm/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <TMDOCK computationServer="cgopm" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmdock_local"
                        component={(props: any) => (
                            <App {...props}>
                                <TMDOCK computationServer="local" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmdock_local/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <TMDOCK computationServer="local" submitted />
                            </App>
                        )}
                    />

                    <Route
                        exact
                        path="/tmmatch"
                        component={(props: any) => (
                            <App {...props}>
                                <TMMATCH computationServer="memprot" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmmatch/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <TMMATCH computationServer="memprot" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmmatch_cgopm"
                        component={(props: any) => (
                            <App {...props}>
                                <TMMATCH computationServer="cgopm" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmmatch_cgopm/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <TMMATCH computationServer="cgopm" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmmatch_local"
                        component={(props: any) => (
                            <App {...props}>
                                <TMMATCH computationServer="local" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/tmmatch_local/submitted"
                        component={(props: any) => (
                            <App {...props}>
                                <TMMATCH computationServer="local" submitted />
                            </App>
                        )}
                    />

                    <Route
                        exact
                        path="/protein_types"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_types"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/protein_types/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_types"
                                        hasList
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/protein_classes"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_classes"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/protein_classes/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_classes"
                                        hasList
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/protein_superfamilies"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_superfamilies"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/protein_superfamilies/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_superfamilies"
                                        hasList
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/protein_families"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_families"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/protein_families/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_families"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/species"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification category="proteins" ctype="species" hasList />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/species/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification category="proteins" ctype="species" hasTable />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/protein_localizations"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_localizations"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/protein_localizations/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="proteins"
                                        ctype="protein_localizations"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/proteins"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification category="proteins" ctype="proteins" hasTable />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/proteins/:id"
                        component={(props: any) => <App children={<Protein />} {...props} />}
                    />

                    <Route
                        exact
                        path="/complex_classes"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_classes"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/complex_classes/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_classes"
                                        hasList
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/complex_superfamilies"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_superfamilies"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/complex_superfamilies/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_superfamilies"
                                        hasList
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/complex_families"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_families"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/complex_families/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_families"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/complex_structures"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_structures"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/complex_structures/:id"
                        component={(props: any) => <App children={<Complex />} {...props} />}
                    />
                    <Route
                        exact
                        path="/complex_structures/:id/glmol_viewer"
                        component={(props: any) => (
                            <App
                                children={<ProteinViewer ctype="complex_structures" />}
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/complex_localizations"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_localizations"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/complex_localizations/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="complex_structures"
                                        ctype="complex_localizations"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/mutations"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="mutations"
                                        ctype="mutations"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/mutation_types"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="mutations"
                                        ctype="mutation_types"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/mutation_types/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="mutations"
                                        ctype="mutation_types"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/diseases"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification category="mutations" ctype="diseases" hasList />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/diseases/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="mutations"
                                        ctype="diseases"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/pathways"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification category="pathways" ctype="pathways" hasTable />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/pathways/:id"
                        component={(props: any) => <App children={<Pathway />} {...props} />}
                    />

                    <Route
                        exact
                        path="/pathway_subclasses"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="pathways"
                                        ctype="pathway_subclasses"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/pathway_subclasses/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="pathways"
                                        ctype="pathway_subclasses"
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/pathway_classes"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="pathways"
                                        ctype="pathway_classes"
                                        hasList
                                    />
                                }
                                {...props}
                            />
                        )}
                    />
                    <Route
                        exact
                        path="/pathway_classes/:id"
                        component={(props: any) => (
                            <App
                                children={
                                    <Classification
                                        category="pathways"
                                        ctype="pathway_classes"
                                        hasList
                                        hasTable
                                    />
                                }
                                {...props}
                            />
                        )}
                    />

                    <Route
                        exact
                        path="/protein_viewer"
                        component={(props: any) => <App children={<ProteinViewer />} {...props} />}
                    />

                    <Route
                        exact
                        path="/1tmnet"
                        render={(props) => (
                            <App {...props}>
                                <TMNet ctype="proteins" />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/1tmnet/submitted"
                        render={(props) => (
                            <App {...props}>
                                <TMNet ctype="proteins" submitted />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/1tmnet/:ctype(protein_superfamilies|protein_families|protein_localizations)/:id?"
                        render={(props) => (
                            <App {...props}>
                                <TMNet
                                    ctype={props.match.params.ctype}
                                    id={props.match.params.id}
                                />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fold"
                        render={(props) => (
                            <App {...props}>
                                <Fold />
                            </App>
                        )}
                    />
                    <Route
                        exact
                        path="/fold/submitted"
                        render={(props) => (
                            <App {...props}>
                                <Fold submitted />
                            </App>
                        )}
                    />

                    <Route
                        exact
                        path="/proteins.php"
                        render={(props: any) => {
                            let params = qs.parse(props.location.search);
                            let url = "/proteins";
                            if (
                                params.search !== undefined &&
                                params.search !== null &&
                                params.search !== ""
                            ) {
                                url = appendQuery(url, { search: params.search.toString() });
                            }
                            return <Redirect to={url} />;
                        }}
                    />
                    <Route
                        exact
                        path="/protein.php"
                        render={(props: any) => {
                            let params = qs.parse(props.location.search);
                            let url = "/proteins";
                            if (
                                params.search !== undefined &&
                                params.search !== null &&
                                params.search !== ""
                            ) {
                                url = appendQuery(url, { search: params.search.toString() });
                            } else if (
                                params.pdbid !== undefined &&
                                params.pdbid !== null &&
                                params.pdbid !== ""
                            ) {
                                url = appendQuery(url, { search: params.pdbid.toString() });
                            } else {
                                return <Error404 />;
                            }
                            return <Redirect to={url} />;
                        }}
                    />

                    <Route component={Error404} />
                </Switch>
            </Router>
        </Provider>
    );
};

ReactDOM.render(<Main />, document.getElementById("react-container"));

if (module.hot) {
    module.hot.accept();
}

export default Main;
