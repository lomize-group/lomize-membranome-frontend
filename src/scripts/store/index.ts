import { combineReducers } from "redux";
import { searchReducer } from "./search";
import { statsReducer } from "./stats";
import { urlReducer } from "./url";

const reducersMap = {
    currentUrl: urlReducer,
    stats: statsReducer,
    search: searchReducer,
} as const;
type ReducersMapType = typeof reducersMap;

export const rootReducer = combineReducers(reducersMap);
export type RootState = ReturnType<typeof rootReducer>;
