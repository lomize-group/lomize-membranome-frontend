import { SearchActionType, SearchBy, UPDATE_SEARCH_BY } from "./searchTypes";

export function updateSearchBy(searchBy: SearchBy): SearchActionType {
    return {
        type: UPDATE_SEARCH_BY,
        payload: searchBy,
    };
}
