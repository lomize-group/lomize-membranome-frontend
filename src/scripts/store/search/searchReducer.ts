import { SearchActionType, SearchState, UPDATE_SEARCH_BY } from "./searchTypes";

const initialState: SearchState = {
    searchBy: "proteins",
    searchValue: "",
};

export function searchReducer(
    state: SearchState = initialState,
    action: SearchActionType
): SearchState {
    switch (action.type) {
        case UPDATE_SEARCH_BY:
            return {
                ...state,
                searchBy: action.payload,
            };
        default:
            return state;
    }
}
