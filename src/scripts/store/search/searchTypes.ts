export const UPDATE_SEARCH_BY = "UPDATE_SEARCH_BY" as const;
export const UPDATE_SEARCH_VALUE = "UPDATE_SEARCH_VALUE" as const;

export type SearchBy = "proteins" | "complex_structures";

export interface SearchState {
    searchBy: SearchBy;
    searchValue: string;
}

interface UpdateSearchByAction {
    type: typeof UPDATE_SEARCH_BY;
    payload: SearchBy;
}

export type SearchActionType = UpdateSearchByAction;
