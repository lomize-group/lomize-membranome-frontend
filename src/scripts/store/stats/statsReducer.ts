import { LOAD_STATS, StatsActionType, StatsState } from "./statsTypes";

export function statsReducer(
    state: StatsState | null = null,
    action: StatsActionType
): StatsState | null {
    switch (action.type) {
        case LOAD_STATS:
            if (state) {
                return {
                    ...state,
                    ...action.payload,
                };
            }
            return { ...action.payload };

        default:
            return state;
    }
}
