import { StatsType } from "../../config/Classification";

export type StatsState = { [key in StatsType]: any };

export const LOAD_STATS = "LOAD_STATS" as const;

interface LoadStatsAction {
    type: typeof LOAD_STATS;
    payload: StatsState;
}

export type StatsActionType = LoadStatsAction;
