import { UPDATE_CURRENT_URL, UrlActionType, UrlState } from "./urlTypes";

export function updateCurrentUrl<T>(url: UrlState<T>): UrlActionType<T> {
    return {
        type: UPDATE_CURRENT_URL,
        payload: url,
    };
}
