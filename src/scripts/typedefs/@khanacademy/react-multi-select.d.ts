declare module "@khanacademy/react-multi-select" {
    import { Component, ReactNode } from "react";

    export type MultiSelectOption = {
        label: string | number;
        value: string | number;
    };

    type MultiSelectProps = {
        onSelectedChanged: (selected: Array<string>) => void;
        options: Array<MultiSelectOption>;
        selected: Array<string>;
        isLoading: boolean;
        disableSearch: boolean;
        hasSelectAll: boolean;
        valueRenderer: (selected: Array<MultiSelectOption>, options: any) => ReactNode;
    };

    class MultiSelect extends Component<MultiSelectProps> {}

    export default MultiSelect;
}
