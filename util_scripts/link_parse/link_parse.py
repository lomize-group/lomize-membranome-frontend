# reference: https://stackoverflow.com/questions/34610162/extract-all-links-from-a-web-page-using-python

from bs4 import BeautifulSoup, SoupStrainer
import requests

soup = BeautifulSoup(open("Membranome.mhtml"),"html.parser")
count = 0

base_url = "https://membranome.org/proteins?search="
api_url = "http://lomize-group-membranome.herokuapp.com/proteins?search="

for link in soup.find_all('a'):
    if  base_url in link.get('href'):
        #print(link.get('href').replace(base_url,api_url))
        protein_query = requests.get(link.get('href').replace(base_url,api_url))
        if protein_query.json()["objects"]==[]:
            print(link.get('href').split('=')[1])
        #print(count)
        count+=1

print(count)